# Stage 1: Build the application
FROM maven:3.8.6-eclipse-temurin-17-alpine AS build

# Set the working directory
WORKDIR /backend

# Copy the Maven wrapper and POM file
COPY API_SPRING_BOOT/auth/mvnw .
COPY API_SPRING_BOOT/auth/.mvn .mvn
COPY API_SPRING_BOOT/auth/pom.xml .

# Download dependencies
RUN mvn clean install -X

# Copy the rest of the application code
COPY API_SPRING_BOOT/auth/src src

# Build the application
RUN mvn package -DskipTests

# Stage 2: Create the final image
FROM eclipse-temurin:17-jdk-alpine

# Create the .env file with the environment variables
# RUN echo "SPRING_DATASOURCE_URL=${SPRING_DATASOURCE_URL}" > .env && \
#     echo "SPRING_DATASOURCE_USERNAME=${SPRING_DATASOURCE_USERNAME}" >> .env && \
#     echo "SPRING_DATASOURCE_PASSWORD=${SPRING_DATASOURCE_PASSWORD}" >> .env

# Create a volume to hold the temporary files
VOLUME /tmp

# Copy the built JAR file from the build stage
COPY --from=build /backend/target/auth-0.0.1-SNAPSHOT.jar /api.jar

# Set the entry point for the container
ENTRYPOINT ["java", "-jar", "/api.jar"]
