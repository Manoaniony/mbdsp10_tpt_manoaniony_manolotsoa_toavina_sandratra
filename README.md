# mbdsp10_tpt_Manoaniony_Manolotsoa_Toavina_Sandratra



## Plan type d’un rapport de stage ou de Travail Pratique Transversal : TPT
https://docs.google.com/document/d/1dzO5AFD2JIWOnDhKPU2Yw43oaOnalUIL/edit?fbclid=IwY2xjawE6-bdleHRuA2FlbQIxMAABHcBNXSAMDhCn-cctpC08Gfo0upD-nPNyjqQGJgqZ3GszLwVFfLvkVEXhqQ_aem_JPPUuyIzypTvhb8HiwdfqQ

## Documentation API Spring Boot
https://docs.google.com/document/d/1iTWdMo3DNInkGRaMIMuddzPsBFrKm5wCtaLNR4awHcA/edit?usp=sharing

## Lien de téléchargement pour le client lourd
https://gitlab.com/Manoaniony/mbdsp10_tpt_manoaniony_manolotsoa_toavina_sandratra/-/blob/main/executable.zip?ref_type=heads

NB: Decompresser tpt_ios.gz et drag and drop le fichier TPT.app dans le simulateur ios

## Lien de la partie backoffice
https://tpt-admin.onrender.com/

## Lien de la partie frontoffice
https://mbdsp10-tpt-manoaniony-manolotsoa-r7fx.onrender.com/
