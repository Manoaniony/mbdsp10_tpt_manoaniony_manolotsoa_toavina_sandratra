#!/bin/bash

# Navigate to the specific directory
cd "$(pwd)/API_SPRING_BOOT/auth" || { echo "Failed to change directory"; exit 1; }

# Print the current working directory
echo "Current working directory: $(pwd)"

# Run the Spring Boot application using Maven Wrapper
./mvnw spring-boot:run
