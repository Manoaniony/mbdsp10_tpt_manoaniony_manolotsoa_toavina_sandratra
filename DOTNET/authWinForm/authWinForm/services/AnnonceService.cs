﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using authWinForm.model;
using Newtonsoft.Json;

namespace authWinForm.services
{
    public class AnnonceService
    {
        private readonly HttpClient _httpClient;

        private const string baseURL = Constants.baseUrl + "api/annonces/";

        public async Task<Result<List<AnnonceModel>>> getAllAnnonce(User user,string authToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    HttpResponseMessage response = await httpClient.GetAsync(baseURL + "getAll");
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        var listeAnnonce = responseObject.objects.content;
                        List<AnnonceModel> liste = new List<AnnonceModel>();
                        if (listeAnnonce != null && listeAnnonce.Count > 0)
                        {
                            foreach (var annonce in listeAnnonce)
                            {
                                AnnonceModel ann = new AnnonceModel();
                                ann.descs = annonce.descs != null ? (string)annonce.descs : null;
                                ann.id = annonce.id != null ? (long)annonce.id : 0;
                                ann.etat = annonce.etat != null ? (string)annonce.etat : null;
                                ann.photo = annonce.photo != null ? (string)annonce.photo : null;
                                ann.datePublication = annonce.datePublication != null ? (DateTime)annonce.datePublication : DateTime.MinValue;
                                ann.libelle = annonce.libelle != null ? (string)annonce.libelle : null;
                                ann.objets = annonce.objets != null ? ((IEnumerable<dynamic>)annonce.objets).Select(o => new Objet
                                {
                                    id = o.id,
                                    description = o.description,
                                    categorie = o.categorie,
                                    etat = o.etat,
                                    photo = o.photo
                                }).ToList() : new List<Objet>();
                                liste.Add(ann);
                            }
                        }
                        return Result<List<AnnonceModel>>.Success(liste, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<List<AnnonceModel>>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<List<AnnonceModel>>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<List<AnnonceModel>>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }

        public async Task<Result<AnnonceModel>> CreateAnnonce(AnnonceModel annonce,String authToken)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    var jsonData = JsonConvert.SerializeObject(annonce);
                    var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(baseURL + "save", content);
                    if (response.IsSuccessStatusCode)
                    {
                        AnnonceModel ann = new AnnonceModel();
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        var resp = responseObject.objects.content;
                        if(resp != null)
                        {
                            ann.descs = resp.descs;
                            ann.id = resp.id;
                            ann.etat = resp.etat;
                            ann.datePublication = resp.datePublication;
                            ann.photo = resp.photo;
                            ann.objets = resp.objets != null ? ((IEnumerable<dynamic>)resp.objets).Select(o => new Objet
                            {
                                id = o.id,
                                description = o.description,
                                categorie = o.categorie,
                                etat = o.etat,
                                photo = o.photo
                            }).ToList() : new List<Objet>();
                        }
                        return Result<AnnonceModel>.Success(ann, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<AnnonceModel>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<AnnonceModel>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<AnnonceModel>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }

        public async Task<Result<AnnonceModel>> getById(long id,User user, string authToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    HttpResponseMessage response = await httpClient.GetAsync(baseURL + "getById/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        AnnonceModel ann = new AnnonceModel();
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        var resp = responseObject.objects;
                        if (resp != null)
                        {
                            ann.libelle = resp.libelle;
                            ann.descs = resp.descs;
                            ann.id = resp.id;
                            ann.etat = resp.etat;
                            ann.datePublication = resp.datePublication;
                            ann.photo = resp.photo;
                            ann.objets = resp.objets != null ? ((IEnumerable<dynamic>)resp.objets).Select(o => new Objet
                            {
                                id = o.id,
                                description = o.description,
                                categorie = o.categorie,
                                etat = o.etat,
                                photo = o.photo
                            }).ToList() : new List<Objet>();
                        }
                        return Result<AnnonceModel>.Success(ann, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<AnnonceModel>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<AnnonceModel>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<AnnonceModel>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }

        public async Task<Result<bool>> delete(long id, User user, string authToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    HttpResponseMessage response = await httpClient.DeleteAsync(baseURL + "delete/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        return Result<bool>.Success(true, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<bool>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<bool>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<bool>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }
    }


}
