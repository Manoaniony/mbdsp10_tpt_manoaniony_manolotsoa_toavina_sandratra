﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using authWinForm.model;
using Newtonsoft.Json;

namespace authWinForm.services
{
    public class ObjetService
    {
        private readonly HttpClient _httpClient;

        private const string baseURL = Constants.baseUrl + "api/objets/";

        public async Task<Result<List<AnnonceModel>>> getAllAnnonce(User user, string authToken)
        {
            try
            {
                List<AnnonceModel> liste = new List<AnnonceModel>();
                AnnonceService annonceService = new AnnonceService();
                var resp = await annonceService.getAllAnnonce(user, authToken);
                if (resp.IsSuccess)
                {
                    for (int i = 0; i < resp.Data.Count; i++) {
                        AnnonceModel ann = new AnnonceModel();
                        ann.libelle = resp.Data[i].libelle;
                        ann.id = resp.Data[i].id;
                        liste.Add(ann);
                    }
                }
                return Result<List<AnnonceModel>>.Success(liste, authToken);
                    
            }
            catch (HttpRequestException ex)
            {
                return Result<List<AnnonceModel>>.Failure($"Erreur HTTP : {ex.Message}");
            }
            catch (Exception ex)
            {
                return Result<List<AnnonceModel>>.Failure($"Une erreur s'est produite : {ex.Message}");
            }
        }

        public async Task<Result<bool>> CreateObjet(Objet objet, String authToken)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    var jsonData = JsonConvert.SerializeObject(objet);
                    var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(baseURL + "save", content);
                    if (response.IsSuccessStatusCode)
                    {
                        bool retour = false;
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        var resp = responseObject.objects;
                        if (resp != null && responseObject.ok == true)
                        {
                            retour = true;
                        }
                        return Result<bool>.Success(retour, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<bool>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<bool>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<bool>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }

        public async Task<Result<List<Objet>>> getAllObjet(User user, string authToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    HttpResponseMessage response = await httpClient.GetAsync(baseURL + "getAll");
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        var listeObjet = responseObject.objects.content;
                        List<Objet> liste = new List<Objet>();
                        if (listeObjet != null && listeObjet.Count > 0)
                        {
                            
                            foreach (var obj in listeObjet)
                            {
                                MessageBox.Show("ppppppppppppppp");
                                Objet ann = new Objet();
                                ann.description = obj.descs != null ? (string)obj.descs : null;
                                ann.id = obj.id != null ? (long)obj.id : 0;
                                ann.etat = obj.etat != null ? (string)obj.etat : null;
                                ann.photo = obj.photo != null ? (string)obj.photo : null;
                                ann.nom = obj.nom != null ? (string)obj.nom : null;
                                /*ann.annonce = obj.annonce != null ? ((IEnumerable<dynamic>)obj.annonce).Select(o => new AnnonceModel
                                {
                                    id = o.id,
                                    descs = o.descs,
                                    datePublication = o.datePublication,
                                    etat = o.etat,
                                    photo = o.photo,
                                    libelle = o.libelle
                                }).ToList() : new List<AnnonceModel>();*/
                                liste.Add(ann);
                            }
                        }
                        string test = JsonConvert.SerializeObject(liste);
                        MessageBox.Show($"++++++ : {test}");
                        return Result<List<Objet>>.Success(liste, authToken);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<List<Objet>>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<List<Objet>>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<List<Objet>>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }
    }
}
