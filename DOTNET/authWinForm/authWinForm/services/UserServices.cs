﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using authWinForm.model;
using Newtonsoft.Json;

namespace authWinForm.services
{
    public class UserServices
    {
        private readonly HttpClient _httpClient;

        private const string baseURL = Constants.baseUrl + "api/users/";
        private const string urlAuth = Constants.baseUrl + "api/auth/";

        public async Task<Result<User>> LoginUser(LoginRequestDto loginRequest)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    /*string json = JsonConvert.SerializeObject(loginRequest);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");*/
                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(urlAuth + "loginUser", loginRequest);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);

                        long id = responseObject.objects.user.id;
                        string username = responseObject.objects.user.username;
                        string nom = responseObject.objects.user.nom;
                        string tel = responseObject.objects.user.tel;
                        string adresse = responseObject.objects.user.adresse;
                        string email = responseObject.objects.user.email;
                        string mdp = responseObject.objects.user.mdp;
                        string token = responseObject.objects.token;


                        User user = new User(username, nom, tel, adresse, email, mdp);
                        user.id = id;

                        return Result<User>.Success(user, token);
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        return Result<User>.Failure(errorMessage);
                    }
                }
                catch (HttpRequestException ex)
                {
                    return Result<User>.Failure($"Erreur HTTP : {ex.Message}");
                }
                catch (Exception ex)
                {
                    return Result<User>.Failure($"Une erreur s'est produite : {ex.Message}");
                }
            }
        }

        public async Task<bool> CreateUser(User user)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    var jsonData = JsonConvert.SerializeObject(user);
                    var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(baseURL + "save", content);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                        dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(jsonResponse);
                        /*res.Add("objects", responseObject.objects);
                        res.Add("message",(string)responseObject.message);
                        res.Add("ok", (bool)responseObject.ok);
                        res.Add("status",(int)responseObject.status);*/
                        return (bool)responseObject.ok;
                    }
                    else
                    {
                        string errorMessage = await response.Content.ReadAsStringAsync();
                        /*res.Add("objects", null);
                        res.Add("message", errorMessage);
                        res.Add("ok", false);
                        res.Add("status", 404);*/
                        return false;
                    }
                }
                catch (HttpRequestException e)
                {
                    return false;
                }
            }
        }
    }
}
