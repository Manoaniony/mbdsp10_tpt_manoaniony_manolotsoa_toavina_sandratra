﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace authWinForm.model
{
    public class Objet
    {
        public long id { get; set; }
        public string nom { get; set; }
        public string description { get; set; }
        public string categorie { get; set; }
        public string etat { get; set; }
        public string photo { get; set; }

        public User user { get; set; }

        public AnnonceModel annonce { get; set; }
    }
}
