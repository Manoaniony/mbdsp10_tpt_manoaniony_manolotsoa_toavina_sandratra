﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace authWinForm.model
{
    public class AnnonceModel
    {
        public long id { get; set; }
        public string descs { get; set; }
        public string etat { get; set; }
        public DateTime datePublication { get; set; }
        public string photo { get; set; }
        public string libelle { get; set; }
        public User user { get; set; }

        public List<Objet> objets { get; set; }

        public AnnonceModel() { 
            this.datePublication = DateTime.Now;
        }

        public static implicit operator AnnonceModel(List<AnnonceModel> v)
        {
            throw new NotImplementedException();
        }
    }
}
