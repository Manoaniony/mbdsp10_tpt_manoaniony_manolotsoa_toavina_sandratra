﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace authWinForm.model
{
    public class User
    {
        public long id { get; set; }
        public string username { get; set; }
        public string nom { get; set; }
        public string tel { get; set; }
        public string adresse { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public User(string username, string nom, string tel, string adresse, string email, string mdp)
        {
            this.username = username;
            this.nom = nom;
            this.tel = tel;
            this.adresse = adresse;
            this.email = email;
            password = mdp;
        }

        internal class LoginRequestDto : model.LoginRequestDto
        {
            public string username { get; set; }
            public string password { get; set; }
        }
        internal class UserRequestDto : model.UserRequestDto
        {
            public int Id { get; set; }
            public int IdPerson { get; set; }
        }
    }
    public class LoginRequestDto
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class UserRequestDto
    {
        //public int Id { get; set; }
        //public int IdPerson { get; set; }
    }
}
