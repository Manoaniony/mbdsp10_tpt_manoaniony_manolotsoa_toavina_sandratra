﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace authWinForm.model
{
    public class ResponseObject<T>
    {
        public bool ok {  get; private set; }
        public T objects { get; private set; }

        public string message { get; private set; }
        public int status { get; private set; }

    }
}
