﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.services;
using authWinForm.Views.annonce;

namespace authWinForm
{
    public partial class AnnonceItem : UserControl
    {
        private User _user;
        private string _token;
        private long _id;

        public event EventHandler ItemClicked;
        public AnnonceItem(User user, string token, long id)
        {
            InitializeComponent();
            //this.Click += new EventHandler(button1_Click);
            _user = user;
            _token = token;
            _id = id;
        }

        private string _title;
        private string _datePubl;
        private string _etat;
        private Image _img;

        private void title_Enter(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void photo_MouseEnter(object sender, EventArgs e)
        {

        }

        private void AnnonceItem_MouseEnter(object sender, EventArgs e)
        {

        }

        private void AnnonceItem_MouseLeave(object sender, EventArgs e)
        {

        }

        private void AnnonceItem_Load(object sender, EventArgs e)
        {

        }

        [Category("Custom Props")]
        public string Title
        {
            get { return _title; }
            set { _title = value; title.Text = value; }
        }

        [Category("Custom Props")]
        public string DatePubl
        {
            get { return _datePubl; }
            set { _datePubl = value; datePubl.Text = value; }
        }

        [Category("Custom Props")]
        public string Etat
        {
            get { return _etat; }
            set { _etat = value; etats.Text = value; }
        }

        [Category("Custom Props")]
        public Image Img
        {
            get { return _img; }
            set { _img = value; photo.Image = value; }
        }

        private void AnnonceItem_Click(object sender, EventArgs e)
        {
            ItemClicked?.Invoke(this, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                form.Hide();
            }
            DetailsAnnonce det = new DetailsAnnonce(_id, _user, _token);
            det.Show();

        }

        private async void btnSuppr_Click(object sender, EventArgs e)
        {
            AnnonceService service = new AnnonceService();
            var res = await service.delete(_id,_user,_token);
            if (res.IsSuccess)
            {
                MessageBox.Show("Suppression réussie");
                foreach (Form form in Application.OpenForms)
                {
                    form.Hide();
                }
                Annonce annonce = new Annonce(_user,_token);
                annonce.Show();
            }
        }
    }
}
