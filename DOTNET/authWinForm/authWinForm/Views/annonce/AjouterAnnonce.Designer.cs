﻿namespace authWinForm
{
    partial class AjouterAnnonce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            btn_upload = new Button();
            label3 = new Label();
            txtDescs = new RichTextBox();
            label2 = new Label();
            txtLibelle = new TextBox();
            label1 = new Label();
            btn_enreg = new Button();
            btn_annuler = new Button();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btn_upload);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(txtDescs);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(txtLibelle);
            groupBox1.Controls.Add(label1);
            groupBox1.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            groupBox1.Location = new Point(12, 87);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(776, 256);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Ajout Annonce";
            // 
            // btn_upload
            // 
            btn_upload.Location = new Point(155, 195);
            btn_upload.Name = "btn_upload";
            btn_upload.Size = new Size(306, 36);
            btn_upload.TabIndex = 6;
            btn_upload.Text = "Upload";
            btn_upload.UseVisualStyleBackColor = true;
            btn_upload.Click += btn_upload_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(75, 211);
            label3.Name = "label3";
            label3.Size = new Size(48, 20);
            label3.TabIndex = 5;
            label3.Text = "Photo";
            label3.Click += label3_Click;
            // 
            // txtDescs
            // 
            txtDescs.Location = new Point(155, 83);
            txtDescs.Name = "txtDescs";
            txtDescs.Size = new Size(306, 96);
            txtDescs.TabIndex = 4;
            txtDescs.Text = "";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(38, 83);
            label2.Name = "label2";
            label2.Size = new Size(85, 20);
            label2.TabIndex = 2;
            label2.Text = "Description";
            // 
            // txtLibelle
            // 
            txtLibelle.Location = new Point(155, 50);
            txtLibelle.Name = "txtLibelle";
            txtLibelle.Size = new Size(306, 27);
            txtLibelle.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(70, 57);
            label1.Name = "label1";
            label1.Size = new Size(53, 20);
            label1.TabIndex = 0;
            label1.Text = "Libelle";
            // 
            // btn_enreg
            // 
            btn_enreg.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btn_enreg.Location = new Point(167, 349);
            btn_enreg.Name = "btn_enreg";
            btn_enreg.Size = new Size(95, 35);
            btn_enreg.TabIndex = 1;
            btn_enreg.Text = "Enregistrer";
            btn_enreg.UseVisualStyleBackColor = true;
            btn_enreg.Click += btn_enreg_Click;
            // 
            // btn_annuler
            // 
            btn_annuler.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            btn_annuler.Location = new Point(378, 349);
            btn_annuler.Name = "btn_annuler";
            btn_annuler.Size = new Size(95, 35);
            btn_annuler.TabIndex = 2;
            btn_annuler.Text = "Annuler";
            btn_annuler.UseVisualStyleBackColor = true;
            btn_annuler.Click += btn_annuler_Click;
            // 
            // AjouterAnnonce
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btn_annuler);
            Controls.Add(btn_enreg);
            Controls.Add(groupBox1);
            Name = "AjouterAnnonce";
            Text = "AjouterAnnonce";
            Load += AjouterAnnonce_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private Label label1;
        private RichTextBox txtDescs;
        private Label label2;
        private TextBox txtLibelle;
        private Label label3;
        private Button btn_upload;
        private Button btn_enreg;
        private Button btn_annuler;
    }
}