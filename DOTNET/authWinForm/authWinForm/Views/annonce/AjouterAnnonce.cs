﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.services;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace authWinForm
{
    public partial class AjouterAnnonce : Home 
    {
        private string lienPhoto;
        public AjouterAnnonce(User user, string token) : base(user,token)
        {
            InitializeComponent();
        }

        private void AjouterAnnonce_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btn_upload_Click(object sender, EventArgs e)
        {
            string sourceFilePath = "";
            string destinationDirectory = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources");
            string destinationFilePath = "";

            try
            {
                // Étape 1 : Sélectionnez le fichier image avec OpenFileDialog
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.jpg, *.png)|*.jpg;*.png|All files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    sourceFilePath = openFileDialog.FileName;

                    if (string.IsNullOrEmpty(sourceFilePath) || !System.IO.File.Exists(sourceFilePath))
                    {
                        MessageBox.Show("Le fichier source est introuvable ou le chemin est incorrect.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Étape 2 : Créez le dossier si nécessaire
                    if (!System.IO.Directory.Exists(destinationDirectory))
                    {
                        System.IO.Directory.CreateDirectory(destinationDirectory);
                    }

                    // Étape 3 : Définir le chemin complet de destination
                    string fileName = System.IO.Path.GetFileName(sourceFilePath); // Récupère le nom du fichier
                    destinationFilePath = System.IO.Path.Combine(destinationDirectory, fileName);

                    if (string.IsNullOrEmpty(destinationFilePath))
                    {
                        MessageBox.Show("Le chemin de destination est incorrect.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    // Étape 4 : Copier le fichier à l'emplacement spécifié
                    System.IO.File.Copy(sourceFilePath, destinationFilePath, true);

                    this.lienPhoto = destinationFilePath;

                    // Message de confirmation
                    MessageBox.Show($"Image sauvegardée à l'emplacement : {destinationFilePath}", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur lors de l'upload de l'image : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btn_enreg_Click(object sender, EventArgs e)
        {
            string libelle = txtLibelle.Text;
            string descs = txtDescs.Text;
            string photo = this.lienPhoto;

            if (string.IsNullOrEmpty(libelle) || string.IsNullOrEmpty(descs))
            {
                MessageBox.Show("Veuillez renseigner tous les champs");
                return;
            }


            AnnonceModel annonce = new AnnonceModel();
            annonce.descs = descs;
            annonce.photo = photo;
            annonce.libelle = libelle;
            annonce.user = _user;
            AnnonceService service = new AnnonceService();
            var response = await service.CreateAnnonce(annonce, _token);
            if (response.IsSuccess)
            {
                MessageBox.Show("Insertion successful!");
                foreach (Form form in Application.OpenForms)
                {
                    form.Hide();
                }
                Annonce formAnn = new Annonce(_user, _token);
                formAnn.Show();
            }
            else
                MessageBox.Show("Erreur!");
        }

        private void btn_annuler_Click(object sender, EventArgs e)
        {

        }
    }
}
