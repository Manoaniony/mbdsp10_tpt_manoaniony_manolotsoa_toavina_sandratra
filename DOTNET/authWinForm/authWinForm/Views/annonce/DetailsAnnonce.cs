﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.services;
using Newtonsoft.Json;

namespace authWinForm.Views.annonce
{
    public partial class DetailsAnnonce :  Home
    {
        private long _id;
        public DetailsAnnonce(long id,User user,string token) : base(user,token) 
        {
            InitializeComponent();
            _id = id;
            getDetails();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private async void getDetails()
        {
            AnnonceService service = new AnnonceService();
            var annonce = await service.getById(_id,_user,_token);
            if(annonce.IsSuccess){
                libelle.Text = annonce.Data.libelle;
                datePubl.Text = annonce.Data.datePublication.ToString("dd-mm-yyyy");
                etats.Text = annonce.Data.etat;
                txtDescs.Text = annonce.Data.descs;
                imgAnn.Image = Image.FromFile(annonce.Data.photo);
            }
        }
    }
}
