﻿namespace authWinForm
{
    partial class AnnonceItem
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            title = new GroupBox();
            btnSuppr = new Button();
            etats = new Label();
            button1 = new Button();
            panel2 = new Panel();
            datePubl = new Label();
            panel1 = new Panel();
            photo = new PictureBox();
            title.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)photo).BeginInit();
            SuspendLayout();
            // 
            // title
            // 
            title.AutoSize = true;
            title.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            title.Controls.Add(btnSuppr);
            title.Controls.Add(etats);
            title.Controls.Add(button1);
            title.Controls.Add(panel2);
            title.Controls.Add(datePubl);
            title.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            title.Location = new Point(200, 3);
            title.Name = "title";
            title.Size = new Size(498, 196);
            title.TabIndex = 3;
            title.TabStop = false;
            title.Text = "Title";
            // 
            // btnSuppr
            // 
            btnSuppr.Location = new Point(367, 121);
            btnSuppr.Name = "btnSuppr";
            btnSuppr.Size = new Size(115, 36);
            btnSuppr.TabIndex = 7;
            btnSuppr.Text = "Supprimer";
            btnSuppr.UseVisualStyleBackColor = true;
            btnSuppr.Click += btnSuppr_Click;
            // 
            // etats
            // 
            etats.AutoEllipsis = true;
            etats.AutoSize = true;
            etats.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            etats.Location = new Point(6, 89);
            etats.Name = "etats";
            etats.Size = new Size(38, 15);
            etats.TabIndex = 6;
            etats.Text = "label1";
            // 
            // button1
            // 
            button1.Location = new Point(6, 121);
            button1.Name = "button1";
            button1.Size = new Size(96, 36);
            button1.TabIndex = 5;
            button1.Text = "Détails";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // panel2
            // 
            panel2.BackColor = SystemColors.ActiveBorder;
            panel2.Location = new Point(-152, 163);
            panel2.Name = "panel2";
            panel2.Size = new Size(644, 1);
            panel2.TabIndex = 1;
            // 
            // datePubl
            // 
            datePubl.AutoEllipsis = true;
            datePubl.AutoSize = true;
            datePubl.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            datePubl.Location = new Point(6, 41);
            datePubl.Name = "datePubl";
            datePubl.Size = new Size(38, 15);
            datePubl.TabIndex = 0;
            datePubl.Text = "label1";
            // 
            // panel1
            // 
            panel1.AutoSize = true;
            panel1.BackColor = SystemColors.ActiveCaption;
            panel1.Controls.Add(photo);
            panel1.Location = new Point(3, 3);
            panel1.Name = "panel1";
            panel1.Size = new Size(181, 165);
            panel1.TabIndex = 4;
            // 
            // photo
            // 
            photo.Image = Properties.Resources.icons8_windows_10_16;
            photo.Location = new Point(3, 6);
            photo.Name = "photo";
            photo.Size = new Size(173, 155);
            photo.SizeMode = PictureBoxSizeMode.Zoom;
            photo.TabIndex = 5;
            photo.TabStop = false;
            // 
            // AnnonceItem
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(panel1);
            Controls.Add(title);
            Name = "AnnonceItem";
            Size = new Size(707, 168);
            Load += AnnonceItem_Load;
            MouseEnter += AnnonceItem_MouseEnter;
            MouseLeave += AnnonceItem_MouseLeave;
            title.ResumeLayout(false);
            title.PerformLayout();
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)photo).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private GroupBox title;
        private Label datePubl;
        private Panel panel1;
        private Panel panel2;
        private Button button1;
        private PictureBox photo;
        private Button btnSuppr;
        private Label etats;
    }
}
