﻿namespace authWinForm.Views.annonce
{
    partial class DetailsAnnonce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            etats = new Label();
            datePubl = new Label();
            libelle = new Label();
            txtDescs = new RichTextBox();
            panel1 = new Panel();
            imgAnn = new PictureBox();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            groupBox1.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)imgAnn).BeginInit();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Controls.Add(etats);
            groupBox1.Controls.Add(datePubl);
            groupBox1.Controls.Add(libelle);
            groupBox1.Controls.Add(txtDescs);
            groupBox1.Controls.Add(panel1);
            groupBox1.Location = new Point(12, 101);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(776, 573);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Détails annonces";
            // 
            // etats
            // 
            etats.AutoSize = true;
            etats.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            etats.Location = new Point(466, 168);
            etats.Name = "etats";
            etats.Size = new Size(63, 25);
            etats.TabIndex = 4;
            etats.Text = "label2";
            // 
            // datePubl
            // 
            datePubl.AutoSize = true;
            datePubl.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            datePubl.Location = new Point(599, 105);
            datePubl.Name = "datePubl";
            datePubl.Size = new Size(63, 25);
            datePubl.TabIndex = 3;
            datePubl.Text = "label1";
            // 
            // libelle
            // 
            libelle.AutoSize = true;
            libelle.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            libelle.Location = new Point(489, 30);
            libelle.Name = "libelle";
            libelle.Size = new Size(63, 25);
            libelle.TabIndex = 2;
            libelle.Text = "label1";
            // 
            // txtDescs
            // 
            txtDescs.Location = new Point(9, 290);
            txtDescs.Name = "txtDescs";
            txtDescs.ReadOnly = true;
            txtDescs.Size = new Size(761, 159);
            txtDescs.TabIndex = 1;
            txtDescs.Text = "";
            // 
            // panel1
            // 
            panel1.Controls.Add(imgAnn);
            panel1.Location = new Point(6, 22);
            panel1.Name = "panel1";
            panel1.Size = new Size(372, 262);
            panel1.TabIndex = 0;
            // 
            // imgAnn
            // 
            imgAnn.Location = new Point(3, 8);
            imgAnn.Name = "imgAnn";
            imgAnn.Size = new Size(358, 251);
            imgAnn.SizeMode = PictureBoxSizeMode.Zoom;
            imgAnn.TabIndex = 0;
            imgAnn.TabStop = false;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label1.Location = new Point(402, 30);
            label1.Name = "label1";
            label1.Size = new Size(81, 25);
            label1.TabIndex = 5;
            label1.Text = "Libelle : ";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.Location = new Point(402, 105);
            label2.Name = "label2";
            label2.Size = new Size(191, 25);
            label2.TabIndex = 6;
            label2.Text = "Date de publication : ";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label3.Location = new Point(402, 168);
            label3.Name = "label3";
            label3.Size = new Size(58, 25);
            label3.TabIndex = 7;
            label3.Text = "Etat : ";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label4.Location = new Point(402, 231);
            label4.Name = "label4";
            label4.Size = new Size(122, 25);
            label4.TabIndex = 8;
            label4.Text = "Description : ";
            // 
            // DetailsAnnonce
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 773);
            Controls.Add(groupBox1);
            Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            Name = "DetailsAnnonce";
            Text = "DetailsAnnonce";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)imgAnn).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private Panel panel1;
        private RichTextBox txtDescs;
        private PictureBox imgAnn;
        private Label etats;
        private Label datePubl;
        private Label libelle;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
    }
}