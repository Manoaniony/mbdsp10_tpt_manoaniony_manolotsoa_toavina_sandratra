﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.Properties;
using authWinForm.services;
using authWinForm;
using Newtonsoft.Json;

namespace authWinForm
{
    public partial class Annonce : Home
    {

        public Annonce(User user, string token) : base(user,token)
        {
            InitializeComponent();
        }

        private async void AjoutAnnonce_Load(object sender, EventArgs e)
        {
            listeItem();
        }

        private async void listeItem()
        {
            AnnonceService service = new AnnonceService();
            var response = await service.getAllAnnonce(_user, _token);
            if (response.IsSuccess)
            {
                List<AnnonceModel> liste = response.Data;
                for (int i = 0; i < liste.Count; i++)
                {
                    AnnonceItem annonce = new AnnonceItem(_user, _token, liste[i].id);
                    annonce.Title = liste[i].libelle;
                    annonce.Img = Image.FromFile(liste[i].photo);
                    annonce.Etat = liste[i].etat;
                    annonce.DatePubl = liste[i].datePublication.ToString("dd-mm-yyyy");
                    if (flowLayoutPanel1.Controls.Count < 0)
                    {
                        flowLayoutPanel1.Controls.Clear();
                    }
                    else
                        flowLayoutPanel1.Controls.Add(annonce);
                }
            }
        }

        private void btnAjoutAnnonce_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
