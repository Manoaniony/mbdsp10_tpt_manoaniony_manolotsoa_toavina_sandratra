﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.services;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace authWinForm.Views.objet
{
    public partial class AjoutObjet : Home
    {
        private string lienPhoto;
        private long idAnnonce;
        private bool isHandlingSelectionChange = false;
        public AjoutObjet(User user, string token) : base(user,token)
        {
            InitializeComponent();
            Load();
        }

        private async void Load()
        {
            ObjetService service = new ObjetService();
            var resp = await service.getAllAnnonce(_user, _token);
            if (resp.IsSuccess)
            {
                List<AnnonceModel> liste = resp.Data.ToList();
                lstAnnonce.DataSource = liste;
                lstAnnonce.DisplayMember = "libelle";
                lstAnnonce.ValueMember = "id";
            }

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            string sourceFilePath = "";
            string destinationDirectory = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources");
            string destinationFilePath = "";

            try
            {
                // Étape 1 : Sélectionnez le fichier image avec OpenFileDialog
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.jpg, *.png)|*.jpg;*.png|All files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    sourceFilePath = openFileDialog.FileName;

                    if (string.IsNullOrEmpty(sourceFilePath) || !System.IO.File.Exists(sourceFilePath))
                    {
                        MessageBox.Show("Le fichier source est introuvable ou le chemin est incorrect.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Étape 2 : Créez le dossier si nécessaire
                    if (!System.IO.Directory.Exists(destinationDirectory))
                    {
                        System.IO.Directory.CreateDirectory(destinationDirectory);
                    }

                    // Étape 3 : Définir le chemin complet de destination
                    string fileName = System.IO.Path.GetFileName(sourceFilePath); // Récupère le nom du fichier
                    destinationFilePath = System.IO.Path.Combine(destinationDirectory, fileName);

                    if (string.IsNullOrEmpty(destinationFilePath))
                    {
                        MessageBox.Show("Le chemin de destination est incorrect.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    // Étape 4 : Copier le fichier à l'emplacement spécifié
                    System.IO.File.Copy(sourceFilePath, destinationFilePath, true);

                    this.lienPhoto = destinationFilePath;

                    // Message de confirmation
                    MessageBox.Show($"Image sauvegardée à l'emplacement : {destinationFilePath}", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur lors de l'upload de l'image : {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lstAnnonce_SelectedIndexChanged(object sender, EventArgs e)
        {
            //AnnonceModel selectedCategory = lstAnnonce.SelectedItem as AnnonceModel;
            if (lstAnnonce.SelectedValue.GetType() == typeof(Int64))
            {
                this.idAnnonce = (long)lstAnnonce.SelectedValue;
            }
        }

        private async void btnEnrg_Click(object sender, EventArgs e)
        {
            ObjetService service = new ObjetService();
            Objet objet = new Objet();
            objet.photo = this.lienPhoto;
            objet.nom = txtNom.Text;
            objet.description = txtDesc.Text;
            objet.categorie = txtCateg.Text;
            objet.user = _user;
            AnnonceModel annonce = new AnnonceModel();
            annonce.id = this.idAnnonce;
            objet.annonce = annonce;
            var resp = await service.CreateObjet(objet,_token);
            if (resp.IsSuccess)
            {
                if(resp.Data == true)
                {
                    MessageBox.Show("Insertion reussie");
                }
            }
        }
    }
}
