﻿namespace authWinForm.Views.objet
{
    partial class AjoutObjet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            btnUpload = new Button();
            label4 = new Label();
            lstAnnonce = new ComboBox();
            txtDesc = new RichTextBox();
            label3 = new Label();
            txtCateg = new TextBox();
            label2 = new Label();
            txtNom = new TextBox();
            label1 = new Label();
            btnEnrg = new Button();
            btnAnnuler = new Button();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btnUpload);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(lstAnnonce);
            groupBox1.Controls.Add(txtDesc);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(txtCateg);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(txtNom);
            groupBox1.Controls.Add(label1);
            groupBox1.Location = new Point(12, 74);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(586, 347);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Ajout objet";
            // 
            // btnUpload
            // 
            btnUpload.Location = new Point(124, 295);
            btnUpload.Name = "btnUpload";
            btnUpload.Size = new Size(248, 23);
            btnUpload.TabIndex = 8;
            btnUpload.Text = "Télécharger (JPG...)";
            btnUpload.UseVisualStyleBackColor = true;
            btnUpload.Click += btnUpload_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(54, 259);
            label4.Name = "label4";
            label4.Size = new Size(64, 15);
            label4.TabIndex = 7;
            label4.Text = "Annonce : ";
            // 
            // lstAnnonce
            // 
            lstAnnonce.FormattingEnabled = true;
            lstAnnonce.Location = new Point(124, 251);
            lstAnnonce.Name = "lstAnnonce";
            lstAnnonce.Size = new Size(248, 23);
            lstAnnonce.TabIndex = 6;
            lstAnnonce.SelectedIndexChanged += lstAnnonce_SelectedIndexChanged;
            // 
            // txtDesc
            // 
            txtDesc.Location = new Point(124, 131);
            txtDesc.Name = "txtDesc";
            txtDesc.Size = new Size(248, 96);
            txtDesc.TabIndex = 5;
            txtDesc.Text = "";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(42, 131);
            label3.Name = "label3";
            label3.Size = new Size(76, 15);
            label3.TabIndex = 4;
            label3.Text = "Description : ";
            // 
            // txtCateg
            // 
            txtCateg.Location = new Point(124, 81);
            txtCateg.Name = "txtCateg";
            txtCateg.Size = new Size(248, 23);
            txtCateg.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(51, 89);
            label2.Name = "label2";
            label2.Size = new Size(67, 15);
            label2.TabIndex = 2;
            label2.Text = "Catégorie : ";
            // 
            // txtNom
            // 
            txtNom.Location = new Point(124, 39);
            txtNom.Name = "txtNom";
            txtNom.Size = new Size(248, 23);
            txtNom.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(75, 47);
            label1.Name = "label1";
            label1.Size = new Size(43, 15);
            label1.TabIndex = 0;
            label1.Text = "Nom : ";
            // 
            // btnEnrg
            // 
            btnEnrg.Location = new Point(136, 427);
            btnEnrg.Name = "btnEnrg";
            btnEnrg.Size = new Size(75, 23);
            btnEnrg.TabIndex = 1;
            btnEnrg.Text = "Enregistrer";
            btnEnrg.UseVisualStyleBackColor = true;
            btnEnrg.Click += btnEnrg_Click;
            // 
            // btnAnnuler
            // 
            btnAnnuler.Location = new Point(309, 427);
            btnAnnuler.Name = "btnAnnuler";
            btnAnnuler.Size = new Size(75, 23);
            btnAnnuler.TabIndex = 2;
            btnAnnuler.Text = "Annuler";
            btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // AjoutObjet
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            AutoSize = true;
            ClientSize = new Size(621, 500);
            Controls.Add(btnAnnuler);
            Controls.Add(btnEnrg);
            Controls.Add(groupBox1);
            Name = "AjoutObjet";
            Text = "AjoutObjet";
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private RichTextBox txtDesc;
        private Label label3;
        private TextBox txtCateg;
        private Label label2;
        private TextBox txtNom;
        private Label label1;
        private Button btnUpload;
        private Label label4;
        private ComboBox lstAnnonce;
        private Button btnEnrg;
        private Button btnAnnuler;
    }
}