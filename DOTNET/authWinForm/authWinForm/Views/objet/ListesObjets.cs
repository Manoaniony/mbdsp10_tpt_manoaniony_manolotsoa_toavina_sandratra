﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.services;
using Newtonsoft.Json.Linq;

namespace authWinForm.Views.objet
{
    public partial class ListesObjets : Home//Form
    {
        public ListesObjets(User user,string token) : base(user,token)
        {
            InitializeComponent();
            listeItem();
        }

        private async void listeItem()
        {
            ObjetService service = new ObjetService();
            var response = await service.getAllObjet(_user, _token);
            MessageBox.Show("etoooooooo");
            if (response.IsSuccess)
            {
                MessageBox.Show("atoooooooo");
                List<Objet> liste = response.Data;
                for (int i = 0; i < liste.Count; i++)
                {
                    AnnonceItem annonce = new AnnonceItem(_user, _token, liste[i].id);
                    annonce.Title = liste[i].nom;
                    annonce.Img = Image.FromFile(liste[i].photo);
                    annonce.Etat = liste[i].etat;
                    annonce.DatePubl = liste[i].categorie;
                    if (flowLayoutPanel1.Controls.Count < 0)
                    {
                        flowLayoutPanel1.Controls.Clear();
                    }
                    else
                        flowLayoutPanel1.Controls.Add(annonce);
                }
            }
        }
    }

    
}
