using System.Net.Http.Headers;
using System.Text;
using authWinForm.model;
using authWinForm.services;
using Newtonsoft.Json;

namespace authWinForm
{
    public partial class Login : Form
    {
        //private readonly HttpClient _httpClient;
        public Login()
        {
            InitializeComponent();
            //_httpClient = new HttpClient();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txt_username.Text) && string.IsNullOrEmpty(txt_password.Text))
                {
                    MessageBox.Show("Veuillez renseigner tous les champs du login");
                }
                else if (string.IsNullOrEmpty(txt_username.Text))
                {
                    MessageBox.Show("Veuillez renseigner le nom d'utilisateur");
                }
                else if (string.IsNullOrEmpty(txt_password.Text))
                {
                    MessageBox.Show("Veuillez renseigner le mot de passe");
                }
                else
                {
                    var loginRequest = new LoginRequestDto
                    {
                        username = txt_username.Text.ToString(),
                        password = txt_password.Text.ToString()
                    };

                    
                    UserServices userService = new UserServices();

                    var loginResponse = await userService.LoginUser(loginRequest);
                    if (loginResponse.IsSuccess)
                    {
                        long idPerson = loginResponse.Data.id;
                        string token = loginResponse.Token;
                        string test = JsonConvert.SerializeObject(loginResponse.Data);
                        User user = loginResponse.Data;

                        Home home = new Home(user,token);
                        home.Show();
                        this.Hide();
                    }
                    else
                    {
                        //messageErreur = "Erreur : " + loginResponse.ErrorMessage;
                        if (loginResponse.ErrorMessage.Contains("Nom d'utilisateur invalide"))
                        {
                            MessageBox.Show("Nom d'utilisateur ou mot de passe invalide ! Veuillez v�rifier ou contacter l'admin");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
                //messageErreur = "Erreur : " + ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Inscription form1 = new Inscription();
            form1.FormClosed += (s, args) => this.Show();
            // Afficher la nouvelle fen�tre
            form1.Show();
            // Masquer la fen�tre actuelle (MainWindow)
            this.Hide();
        }
    }
}
