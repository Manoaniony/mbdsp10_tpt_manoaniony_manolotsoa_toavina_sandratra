﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using authWinForm.model;
using authWinForm.Views.objet;

namespace authWinForm
{
    public partial class Home : Form
    {
        protected User _user;
        protected string _token;
        private string nom;

        public Home(User user, string token)
        {
            InitializeComponent();
            _user = user;
            _token = token;
            déconnexionToolStripMenuItem.Text = user.nom;
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void ajoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void objetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void objetToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjouterAnnonce ajout = new AjouterAnnonce(_user, _token);
            ajout.Show();
            this.Hide();
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Annonce childForm = new Annonce(_user, _token);
            childForm.Show();
            this.Hide();
        }

        private void déconnexionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            var openForms = Application.OpenForms.Cast<Form>().ToList();
            foreach (Form form in openForms)
            {
                form.Hide();
            }
            login.Show();
        }

        private void ajouterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AjoutObjet ajout = new AjoutObjet(_user, _token);
            ajout.Show();
            this.Hide();
        }

        private void listesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListesObjets objets = new ListesObjets(_user,_token);
            objets.Show();
            this.Hide();
        }
    }
}
