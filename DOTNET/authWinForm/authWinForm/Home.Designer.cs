﻿namespace authWinForm
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            menuStrip1 = new MenuStrip();
            objetToolStripMenuItem = new ToolStripMenuItem();
            objetToolStripMenuItem1 = new ToolStripMenuItem();
            ajouterToolStripMenuItem = new ToolStripMenuItem();
            listeToolStripMenuItem = new ToolStripMenuItem();
            objetToolStripMenuItem2 = new ToolStripMenuItem();
            ajouterToolStripMenuItem1 = new ToolStripMenuItem();
            listesToolStripMenuItem = new ToolStripMenuItem();
            déconnexionToolStripMenuItem = new ToolStripMenuItem();
            déconnexionToolStripMenuItem1 = new ToolStripMenuItem();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { objetToolStripMenuItem, objetToolStripMenuItem1, objetToolStripMenuItem2, déconnexionToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(800, 28);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // objetToolStripMenuItem
            // 
            objetToolStripMenuItem.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            objetToolStripMenuItem.Name = "objetToolStripMenuItem";
            objetToolStripMenuItem.Size = new Size(69, 24);
            objetToolStripMenuItem.Text = "Accueil";
            objetToolStripMenuItem.Click += objetToolStripMenuItem_Click;
            // 
            // objetToolStripMenuItem1
            // 
            objetToolStripMenuItem1.DropDownItems.AddRange(new ToolStripItem[] { ajouterToolStripMenuItem, listeToolStripMenuItem });
            objetToolStripMenuItem1.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            objetToolStripMenuItem1.Name = "objetToolStripMenuItem1";
            objetToolStripMenuItem1.Size = new Size(79, 24);
            objetToolStripMenuItem1.Text = "Annonce";
            objetToolStripMenuItem1.Click += objetToolStripMenuItem1_Click;
            // 
            // ajouterToolStripMenuItem
            // 
            ajouterToolStripMenuItem.Name = "ajouterToolStripMenuItem";
            ajouterToolStripMenuItem.Size = new Size(127, 24);
            ajouterToolStripMenuItem.Text = "Ajouter";
            ajouterToolStripMenuItem.Click += ajouterToolStripMenuItem_Click;
            // 
            // listeToolStripMenuItem
            // 
            listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            listeToolStripMenuItem.Size = new Size(127, 24);
            listeToolStripMenuItem.Text = "Liste";
            listeToolStripMenuItem.Click += listeToolStripMenuItem_Click;
            // 
            // objetToolStripMenuItem2
            // 
            objetToolStripMenuItem2.DropDownItems.AddRange(new ToolStripItem[] { ajouterToolStripMenuItem1, listesToolStripMenuItem });
            objetToolStripMenuItem2.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            objetToolStripMenuItem2.Name = "objetToolStripMenuItem2";
            objetToolStripMenuItem2.Size = new Size(58, 24);
            objetToolStripMenuItem2.Text = "Objet";
            // 
            // ajouterToolStripMenuItem1
            // 
            ajouterToolStripMenuItem1.Name = "ajouterToolStripMenuItem1";
            ajouterToolStripMenuItem1.Size = new Size(180, 24);
            ajouterToolStripMenuItem1.Text = "Ajouter";
            ajouterToolStripMenuItem1.Click += ajouterToolStripMenuItem1_Click;
            // 
            // listesToolStripMenuItem
            // 
            listesToolStripMenuItem.Name = "listesToolStripMenuItem";
            listesToolStripMenuItem.Size = new Size(180, 24);
            listesToolStripMenuItem.Text = "Listes";
            listesToolStripMenuItem.Click += listesToolStripMenuItem_Click;
            // 
            // déconnexionToolStripMenuItem
            // 
            déconnexionToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { déconnexionToolStripMenuItem1 });
            déconnexionToolStripMenuItem.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            déconnexionToolStripMenuItem.Name = "déconnexionToolStripMenuItem";
            déconnexionToolStripMenuItem.Size = new Size(108, 24);
            déconnexionToolStripMenuItem.Text = "Déconnexion";
            // 
            // déconnexionToolStripMenuItem1
            // 
            déconnexionToolStripMenuItem1.Name = "déconnexionToolStripMenuItem1";
            déconnexionToolStripMenuItem1.Size = new Size(165, 24);
            déconnexionToolStripMenuItem1.Text = "Déconnexion";
            déconnexionToolStripMenuItem1.Click += déconnexionToolStripMenuItem1_Click;
            // 
            // Home
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "Home";
            Text = "Home";
            Load += Home_Load;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem objetToolStripMenuItem;
        private ToolStripMenuItem objetToolStripMenuItem1;
        private ToolStripMenuItem objetToolStripMenuItem2;
        private ToolStripMenuItem déconnexionToolStripMenuItem;
        private ToolStripMenuItem ajouterToolStripMenuItem;
        private ToolStripMenuItem listeToolStripMenuItem;
        private ToolStripMenuItem ajouterToolStripMenuItem1;
        private ToolStripMenuItem listesToolStripMenuItem;
        private ToolStripMenuItem déconnexionToolStripMenuItem1;
    }
}