﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using System.Security.Policy;
using authWinForm.model;
using authWinForm.services;
using System.Diagnostics;

namespace authWinForm
{
    public partial class Inscription : Form
    {
        private readonly HttpClient _httpClient;
        public Inscription()
        {
            InitializeComponent();
            _httpClient = new HttpClient();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            string username = txtPseudo.Text;
            string nom = txtNom.Text;
            string tel = txtTel.Text;
            string adresse = txtAdresse.Text;
            string email = txtMail.Text;
            string mdp = txtMdp.Text;
            string verifMdp = txtVerifMdp.Text;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(nom) || string.IsNullOrEmpty(tel) ||
                string.IsNullOrEmpty(adresse) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(mdp) ||
                string.IsNullOrEmpty(verifMdp))
            {
                MessageBox.Show("Veuillez renseigner tous les champs");
                return;
            }

            if (!IsValidEmail(email))
            {
                MessageBox.Show("Veuillez entrer un mail correct");
                return;
            }
            if (!checkMdp(mdp, verifMdp))
            {
                MessageBox.Show("Mot de passe incorrect");
                return;
            }

            User user = new User(username, nom, tel, adresse, email, mdp);
            UserServices userService = new UserServices();
            var inscResponse = await userService.CreateUser(user);

            Debug.WriteLine(".............." + inscResponse);

            if (inscResponse)
            {
                MessageBox.Show("Insertion successful!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Utilisateur ou email déja utilisé");
            }

            /*if (inscResponse != null && inscResponse.ContainsKey("ok") && (bool)inscResponse["ok"])
            {
                MessageBox.Show("Insertion successful!");
                this.Close();
            }
            else
            {
                MessageBox.Show($"Erreur : {(string)inscResponse["message"]}");
            }*/

        }

            

            /*User user = new User(username, nom, tel, adresse, email, mdp);
            string json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            //HttpResponseMessage response = await _httpClient.PostAsync("http://localhost:8036/login", content);
            String Url = Constants.baseUrl + "api/users/save";
            HttpResponseMessage response = await _httpClient.PostAsync(Url, content);
            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Insertion successful!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Erreur");
            }*/


        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool checkMdp(string mdp, string verifMdp)
        {
            try
            {
                return mdp == verifMdp;
            }
            catch
            {
                return false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
