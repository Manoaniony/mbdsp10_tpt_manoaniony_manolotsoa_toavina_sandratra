﻿namespace authWinForm
{
    partial class Inscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            txtTel = new TextBox();
            label7 = new Label();
            txtVerifMdp = new TextBox();
            label6 = new Label();
            txtMdp = new TextBox();
            label5 = new Label();
            txtAdresse = new TextBox();
            label4 = new Label();
            txtMail = new TextBox();
            label3 = new Label();
            txtNom = new TextBox();
            label2 = new Label();
            txtPseudo = new TextBox();
            label1 = new Label();
            button1 = new Button();
            button2 = new Button();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(txtTel);
            groupBox1.Controls.Add(label7);
            groupBox1.Controls.Add(txtVerifMdp);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(txtMdp);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(txtAdresse);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(txtMail);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(txtNom);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(txtPseudo);
            groupBox1.Controls.Add(label1);
            groupBox1.Location = new Point(12, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(776, 330);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Informations utilisateur";
            // 
            // txtTel
            // 
            txtTel.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtTel.Location = new Point(236, 209);
            txtTel.Name = "txtTel";
            txtTel.Size = new Size(322, 29);
            txtTel.TabIndex = 9;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label7.Location = new Point(125, 217);
            label7.Name = "label7";
            label7.Size = new Size(80, 21);
            label7.TabIndex = 8;
            label7.Text = "Téléphone";
            // 
            // txtVerifMdp
            // 
            txtVerifMdp.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtVerifMdp.Location = new Point(236, 279);
            txtVerifMdp.Name = "txtVerifMdp";
            txtVerifMdp.PasswordChar = '*';
            txtVerifMdp.Size = new Size(322, 29);
            txtVerifMdp.TabIndex = 13;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label6.Location = new Point(28, 287);
            label6.Name = "label6";
            label6.Size = new Size(177, 21);
            label6.TabIndex = 12;
            label6.Text = "Confirmer mot de passe";
            // 
            // txtMdp
            // 
            txtMdp.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtMdp.Location = new Point(236, 244);
            txtMdp.Name = "txtMdp";
            txtMdp.PasswordChar = '*';
            txtMdp.Size = new Size(322, 29);
            txtMdp.TabIndex = 11;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label5.Location = new Point(103, 252);
            label5.Name = "label5";
            label5.Size = new Size(102, 21);
            label5.TabIndex = 10;
            label5.Text = "Mot de passe";
            // 
            // txtAdresse
            // 
            txtAdresse.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtAdresse.Location = new Point(236, 174);
            txtAdresse.Name = "txtAdresse";
            txtAdresse.Size = new Size(322, 29);
            txtAdresse.TabIndex = 7;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label4.Location = new Point(140, 182);
            label4.Name = "label4";
            label4.Size = new Size(65, 21);
            label4.TabIndex = 6;
            label4.Text = "Adresse";
            // 
            // txtMail
            // 
            txtMail.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtMail.Location = new Point(236, 139);
            txtMail.Name = "txtMail";
            txtMail.Size = new Size(322, 29);
            txtMail.TabIndex = 5;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label3.Location = new Point(147, 147);
            label3.Name = "label3";
            label3.Size = new Size(54, 21);
            label3.TabIndex = 4;
            label3.Text = "E-mail";
            label3.Click += label3_Click;
            // 
            // txtNom
            // 
            txtNom.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtNom.Location = new Point(236, 104);
            txtNom.Name = "txtNom";
            txtNom.Size = new Size(322, 29);
            txtNom.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.Location = new Point(84, 112);
            label2.Name = "label2";
            label2.Size = new Size(121, 21);
            label2.TabIndex = 2;
            label2.Text = "Nom et Prénom";
            // 
            // txtPseudo
            // 
            txtPseudo.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            txtPseudo.Location = new Point(236, 69);
            txtPseudo.Name = "txtPseudo";
            txtPseudo.Size = new Size(322, 29);
            txtPseudo.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label1.Location = new Point(144, 77);
            label1.Name = "label1";
            label1.Size = new Size(61, 21);
            label1.TabIndex = 0;
            label1.Text = "Pseudo";
            label1.Click += label1_Click;
            // 
            // button1
            // 
            button1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button1.Location = new Point(248, 348);
            button1.Name = "button1";
            button1.Size = new Size(115, 40);
            button1.TabIndex = 14;
            button1.Text = "Enregistrer";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button2
            // 
            button2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button2.Location = new Point(455, 348);
            button2.Name = "button2";
            button2.Size = new Size(115, 40);
            button2.TabIndex = 15;
            button2.Text = "Annuler";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // Inscription
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(groupBox1);
            Name = "Inscription";
            Text = "Form1";
            Load += Form1_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private Label label1;
        private TextBox txtPseudo;
        private TextBox txtMdp;
        private Label label5;
        private TextBox txtAdresse;
        private Label label4;
        private TextBox txtMail;
        private Label label3;
        private TextBox txtNom;
        private Label label2;
        private TextBox txtVerifMdp;
        private Label label6;
        private Button button1;
        private TextBox txtTel;
        private Label label7;
        private Button button2;
    }
}