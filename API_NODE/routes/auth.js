function internalServer(_, res) {
  res.status(500).json({
    dataResponse: null,
    status: 500,
    message: "Internal Server"
  })
}

function forbidden(_, res) {
  res.status(403).json({
    data: null,
    status: 403,
    message: "FORBIDDEN"
  })
}

function unauthorized(_, res) {
  res.status(401).json({
    data: null,
    status: 401,
    message: "UNAUTHORIZED"
  })
}

function ok(_, res) {
  res.status(200).json({
    dataResponse: {
      ok: true
    },
    message: "OK",
    status: 200
  })
}

function mockUsers(_, res) {
  res.status(200).json({
    dataResponse: {
      object: [{
        id: 1,
        userName: "Tilt",
        firstName: "Manolotsoa",
        lastName: "Razafindrakoto"
      }]
    },
    status: 200,
    message: "OK"
  })
}

module.exports = { internalServer, forbidden, unauthorized, ok, mockUsers }