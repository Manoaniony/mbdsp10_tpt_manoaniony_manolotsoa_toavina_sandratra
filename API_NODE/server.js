require('dotenv').config()
const fs = require('fs');
const multer = require("multer");
const upload = multer({ dest: "uploads/" });
const cloudinary = require("./cloudinary");


let express = require('express');
const cors = require('cors');

let app = express();
let bodyParser = require('body-parser');

// Pour les formulaires
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors(
  {
    origin: '*',
    //   allowedHeaders: ['Authorization', 'Content-Type'],
    //   methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTION'],
    //   credentials: true // If you're using cookies for authentication
  }
));

app.post("/api/upload_files", upload.array("files"), uploadFiles);
async function uploadFiles(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  const uploader = async (path) => await cloudinary.uploads(path, 'Images');
  if (req.method === 'POST') {
    const urls = []
    const files = req.files;
    console.log(files);
    for (const file of files) {
      const { path } = file
      const newPath = await uploader(path)
      urls.push(newPath)
      fs.unlinkSync(path)
    }
    res.status(200).json({
      message: 'images uploaded successfully',
      data: urls
    })
  }
  else {
    res.status(405).json({
      err: `${req.method} method not allowed`
    })
  }
}
let { forbidden, internalServer, unauthorized, ok, mockUsers } = require('./routes/auth');

let port = process.env.PORT || 4000;
const prefix = '/api';

app.route(prefix + '/internal-server')
  .get(internalServer)
app.route(prefix + '/forbidden')
  .get(forbidden)
app.route(prefix + '/unauthorized')
  .get(unauthorized)
app.route(prefix + '/ok')
  .get(ok)
app.route(prefix + '/mock_users')
  .post(mockUsers)

// On démarre le serveur
app.listen(port, "0.0.0.0");
console.log('Serveur démarré sur http://localhost:' + port);

module.exports = app;