import { SelectValueType } from "../../types";

export const useAnnoncesHandler = () => {
  const onSearch = (
    e: unknown,
    loadQuery: (params?: {
      page?: number;
      size?: number;
      libelle?: string;
    }) => void
  ) => {
    if (!e) {
      console.log("load all");
      loadQuery();
    } else {
      loadQuery({
        libelle: e as string,
      });
    }
  };

  const onChange = (
    e: { annonce: SelectValueType },
    loadQuery: (params?: {
      page?: number;
      size?: number;
      libelle?: string;
      userId: number;
    }) => void
  ) => {
    if (!e.annonce) {
      loadQuery();
    }
  };

  return { onSearch, onChange };
};
