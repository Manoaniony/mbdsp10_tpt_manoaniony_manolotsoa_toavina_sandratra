import { useQuery } from "@tanstack/react-query";
import {
  DataSuccessPaginateType,
  Objet,
  RequestSuccessType,
} from "../../types";
import { AxiosError } from "axios";
import { PaginateObject } from "../../types/DataSuccessPaginate.types";
import { useState } from "react";
import { Annonce } from "../../types/Annonce.types";
import { getAllAnnonces } from "../../services/annonces/Annonces.services";

export const useAnnonces = (params: { page?: number; size?: number }) => {
  const { page, size } = params;
  const {
    data,
    isLoading,
    error,
    refetch: load,
  } = useQuery<
    RequestSuccessType<Annonce[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<Annonce[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [page || 0, size || 30],
    queryFn: ({ queryKey }) =>
      getAllAnnonces({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });
  const content: Annonce[] | undefined = (
    (
      (data as RequestSuccessType<Annonce[]>)
        ?.data as DataSuccessPaginateType<Annonce>
    )?.objects as PaginateObject<Annonce>
  )?.content;
  return { data, isLoading, content, error, load };
};

export const useQueryAnnonces: () => {
  annonces: Annonce[] | undefined;
  error?: unknown;
  isLoading: boolean;
  loadQuery: (
    params?: { page?: number; size?: number; nom?: string } | undefined
  ) => Promise<void>;
} = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [annonces, setAnnonces] = useState<Annonce[]>();
  const [error, setError] = useState<unknown>();
  const loadQuery = async (params?: { page?: number; size?: number }) => {
    try {
      setIsLoading(true);
      const { status, data } = (await getAllAnnonces(
        params
      )) as RequestSuccessType<Objet>;

      if (status === 200) {
        setAnnonces(
          (
            (data as DataSuccessPaginateType<Annonce>)
              ?.objects as PaginateObject<Annonce>
          )?.content
        );
      } else {
        setError(
          "Une erreur s'est produite lors de la recuperation des objets, veuillez réessayer"
        );
      }
    } catch (error: unknown) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };
  return { annonces, error, isLoading, loadQuery };
};
