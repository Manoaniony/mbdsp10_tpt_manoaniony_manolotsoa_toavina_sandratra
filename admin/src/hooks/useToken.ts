import { Dispatch, SetStateAction, useContext } from "react";
import { TokenContext } from "../context/TokenContext";

export interface TokenContextType {
  token: string | undefined;
  setToken: Dispatch<SetStateAction<string | undefined>> | undefined;
}

export const useToken = (): TokenContextType => {
  const context = useContext<TokenContextType>(TokenContext);
  return context;
};
