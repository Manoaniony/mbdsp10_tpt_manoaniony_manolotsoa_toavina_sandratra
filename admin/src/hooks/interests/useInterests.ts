import { useQuery } from "@tanstack/react-query";
import {
  DataSuccessPaginateType,
  InterestType,
  RequestSuccessType,
} from "../../types";
import { AxiosError } from "axios";
import { PaginateObject } from "../../types/DataSuccessPaginate.types";
import { useState } from "react";
import { getAllInterests } from "../../services/interests/Interests.services";

export const useInterests = (params: { page?: number; size?: number }) => {
  const { page, size } = params;
  const {
    data,
    isLoading,
    error,
    refetch: load,
  } = useQuery<
    RequestSuccessType<InterestType[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<InterestType[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [page || 0, size || 30],
    queryFn: ({ queryKey }) =>
      getAllInterests({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });
  const content: InterestType[] | undefined = (
    (
      (data as RequestSuccessType<InterestType[]>)
        ?.data as DataSuccessPaginateType<InterestType>
    )?.objects as PaginateObject<InterestType>
  )?.content;
  return { data, isLoading, content, error, load };
};

export const useQueryInterests: () => {
  interests: InterestType[] | undefined;
  error?: unknown;
  isLoading: boolean;
  loadQuery: (
    params?: { page?: number; size?: number } | undefined
  ) => Promise<void>;
} = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [interests, setInterests] = useState<InterestType[]>();
  const [error, setError] = useState<unknown>();
  const loadQuery = async (params?: { page?: number; size?: number }) => {
    try {
      setIsLoading(true);
      const { status, data } = (await getAllInterests(
        params
      )) as RequestSuccessType<InterestType>;

      if (status === 200) {
        setInterests(
          (
            (data as DataSuccessPaginateType<InterestType>)
              ?.objects as PaginateObject<InterestType>
          )?.content
        );
      } else {
        setError(
          "Une erreur s'est produite lors de la recuperation des interets, veuillez réessayer"
        );
      }
    } catch (error: unknown) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };
  return { interests, error, isLoading, loadQuery };
};
