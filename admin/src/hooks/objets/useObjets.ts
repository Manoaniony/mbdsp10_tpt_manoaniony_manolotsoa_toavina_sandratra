import { useQuery } from "@tanstack/react-query";
import {
  DataSuccessPaginateType,
  Objet,
  RequestSuccessType,
} from "../../types";
import { AxiosError } from "axios";
import { getAllObjets } from "../../services/objets/Objets.services";
import { PaginateObject } from "../../types/DataSuccessPaginate.types";
import { useState } from "react";

export const useObjets = (params: {
  page?: number;
  size?: number;
  nom?: string;
}) => {
  const { page, size, nom } = params;
  const {
    data,
    isLoading,
    error,
    refetch: load,
  } = useQuery<
    RequestSuccessType<Objet[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<Objet[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [page || 0, size || 30],
    queryFn: ({ queryKey }) =>
      getAllObjets({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
        nom,
      }),
    enabled: true,
  });
  const content: Objet[] | undefined = (
    (
      (data as RequestSuccessType<Objet[]>)
        ?.data as DataSuccessPaginateType<Objet>
    )?.objects as PaginateObject<Objet>
  )?.content;
  return { data, isLoading, content, error, load };
};

export const useQueryObjets: () => {
  objets: Objet[] | undefined;
  error?: unknown;
  isLoading: boolean;
  loadQuery: (
    params?:
      | { page?: number; size?: number; nom?: string; user?: { id?: number } }
      | undefined
  ) => Promise<void>;
} = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [objets, setObjets] = useState<Objet[]>();
  const [error, setError] = useState<unknown>();
  const loadQuery = async (params?: {
    page?: number;
    size?: number;
    nom?: string;
    userId?: number;
  }) => {
    try {
      setIsLoading(true);
      const { status, data } = (await getAllObjets(
        params
      )) as RequestSuccessType<Objet>;

      if (status === 200) {
        setObjets(
          (
            (data as DataSuccessPaginateType<Objet>)
              ?.objects as PaginateObject<Objet>
          )?.content
        );
      } else {
        setError(
          "Une erreur s'est produite lors de la recuperation des objets, veuillez réessayer"
        );
      }
    } catch (error: unknown) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };
  return { objets, error, isLoading, loadQuery };
};
