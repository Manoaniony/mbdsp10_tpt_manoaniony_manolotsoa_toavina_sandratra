import { SelectValueType } from "../../types";

export const useObjetsHandler = () => {
  const onSearch = (
    e: unknown,
    loadQuery: (params?: { page?: number; size?: number; nom?: string }) => void
  ) => {
    if (!e) {
      console.log("load all");
      loadQuery();
    } else {
      loadQuery({
        nom: e as string,
      });
    }
  };

  const onChange = (
    e: { objet: SelectValueType },
    loadQuery: (params?: {
      page?: number;
      size?: number;
      nom?: string;
      user?: { id: number };
    }) => void
  ) => {
    if (!e.objet) {
      loadQuery();
    }
  };

  return { onSearch, onChange };
};
