import { AxiosError } from "axios";
import { RequestSuccessType, User } from "../../types";
import { useQuery } from "@tanstack/react-query";
import { getAllUsers } from "../../services/users/Users.services";
import {
  DataSuccessPaginateType,
  PaginateObject,
} from "../../types/DataSuccessPaginate.types";
import { useState } from "react";

export const useUsers = (params: {
  page?: number;
  size?: number;
  username?: string;
}) => {
  const { page, size, username } = params;
  const {
    data,
    isLoading,
    error,
    refetch: load,
  } = useQuery<
    RequestSuccessType<User[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<User[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [page || 0, size || 30],
    queryFn: ({ queryKey }) =>
      getAllUsers({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
        username,
      }),
    enabled: true,
  });
  const content: User[] | undefined = (
    (
      (data as RequestSuccessType<User[]>)
        ?.data as DataSuccessPaginateType<User>
    )?.objects as PaginateObject<User>
  )?.content;
  return { data, isLoading, content, error, load };
};

export const useQueryUsers: () => {
  users: User[] | undefined;
  error?: unknown;
  isLoading: boolean;
  loadQuery: (
    params?: { page?: number; size?: number; username?: string } | undefined
  ) => Promise<void>;
} = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [users, setUsers] = useState<User[]>();
  const [error, setError] = useState<unknown>();
  const loadQuery = async (params?: {
    page?: number;
    size?: number;
    username?: string;
  }) => {
    try {
      setIsLoading(true);
      const { status, data } = (await getAllUsers(
        params
      )) as RequestSuccessType<User>;

      if (status === 200) {
        setUsers(
          (
            (data as DataSuccessPaginateType<User>)
              ?.objects as PaginateObject<User>
          )?.content
        );
      } else {
        setError(
          "Une erreur s'est produite lors de la recuperation des utilisateurs, veuillez réessayer"
        );
      }
    } catch (error: unknown) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };
  return { users, error, isLoading, loadQuery };
};
