import { SelectValueType } from "../../types";

export const useUsersHandler = () => {
  const onSearch = (
    e: unknown,
    loadQuery: (params?: {
      page?: number;
      size?: number;
      username?: string;
    }) => void
  ) => {
    if (!e) {
      console.log("load all");
      loadQuery();
    } else {
      loadQuery({
        username: e as string,
      });
    }
  };

  const onChange = (
    e: { user: SelectValueType },
    loadQuery: (params?: {
      page?: number;
      size?: number;
      username?: string;
    }) => void,
    loadObjet?: (params?: {
      page?: number;
      size?: number;
      userId?: number;
    }) => void
  ) => {
    if (!e.user) {
      loadQuery();
    }
    if (e.user && loadObjet) {
      loadObjet({
        userId: e?.user?.value as number,
      });
    }
  };

  return { onSearch, onChange };
};
