export const useUserValidator = () => {
  const usernameOrEmailAlreadyExist = (errorMessage?: string) => {
    return new Promise<void>((resolve, reject) => {
      console.log("Error Message ", errorMessage);

      if (!errorMessage) {
        resolve()
        return;
      }
      reject('Email ou nom d\'utilisateur existe déjà'); // Validation failed
    })
  }

  return { usernameOrEmailAlreadyExist }
}