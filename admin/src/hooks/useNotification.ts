import { notification } from "antd";

interface NotificationType {
  title: string,
  description: string,
  showProgress: boolean,
  pauseOnHover: boolean
  type?: 'success' | 'info' | 'warning' | 'error'
}

export const useNotification: () => {
  openNotification: (params?: NotificationType) => void,
  contextHolder: unknown
} = () => {
  const [api, contextHolder] = notification.useNotification();
  const openNotification: ((params?: NotificationType) => void) = (params) => {
    const { description, pauseOnHover, showProgress, title, type } = params as NotificationType
    api[type || "success"]({
      message: title,
      description,
      showProgress,
      pauseOnHover,
    });
  };
  return { openNotification, contextHolder }
}