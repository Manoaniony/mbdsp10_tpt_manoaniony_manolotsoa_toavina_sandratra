import { createBrowserRouter } from "react-router-dom";
import { App } from "./App";
import { LoginPage } from "./pages/LoginPage";
import { InternalServer, NotFound } from "./pages/ErrorPage";
import { Unauthorized } from "./pages/ErrorPage/unauthorized";
import { BadRequest } from "./pages/ErrorPage/bad-request";
import { Forbidden } from "./pages/ErrorPage/forbidden";
import { MainLayout } from "./components";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <LoginPage />,
  },
  {
    path: "admin/",
    element: <MainLayout />,
    children: [
      {
        path: "users/",
        children: [
          {
            path: "",
            lazy: async () => {
              const { ListComponent } = await import("./components/users/list");
              return { Component: ListComponent };
            },
          },
          {
            path: "list",
            lazy: async () => {
              const { ListComponent } = await import("./components/users/list");
              return { Component: ListComponent };
            },
          },
          {
            path: "create",
            lazy: async () => {
              const { CreateComponent } = await import(
                "./components/users/create"
              );
              return { Component: CreateComponent };
            },
          },
          {
            path: "edit/:id",
            lazy: async () => {
              const { UpdateComponent } = await import(
                "./components/users/update"
              );
              return { Component: UpdateComponent };
            },
          },
        ],
      },
      {
        path: "annonces/",
        children: [
          {
            path: "",
            lazy: async () => {
              const { ListAnnoncesComponent } = await import(
                "./components/annonces/list"
              );
              return { Component: ListAnnoncesComponent };
            },
          },
          {
            path: "list",
            lazy: async () => {
              const { ListAnnoncesComponent } = await import(
                "./components/annonces/list"
              );
              return { Component: ListAnnoncesComponent };
            },
          },
          {
            path: "create",
            lazy: async () => {
              const { CreateAnnonceComponent } = await import(
                "./components/annonces/create"
              );
              return { Component: CreateAnnonceComponent };
            },
          },
          {
            path: "edit/:id",
            lazy: async () => {
              const { UpdateComponent } = await import(
                "./components/annonces/update"
              );
              return { Component: UpdateComponent };
            },
          },
        ],
      },
      {
        path: "objets/",
        children: [
          {
            path: "",
            lazy: async () => {
              const { ListComponent: ListObjetsComponent } = await import(
                "./components/objets/list"
              );
              return { Component: ListObjetsComponent };
            },
          },
          {
            path: "list",
            lazy: async () => {
              const { ListComponent: ListObjetsComponent } = await import(
                "./components/objets/list"
              );
              return { Component: ListObjetsComponent };
            },
          },
          {
            path: "create",
            lazy: async () => {
              const { CreateComponent: CreateObjetComponent } = await import(
                "./components/objets/create"
              );
              return { Component: CreateObjetComponent };
            },
          },
          {
            path: "edit/:id",
            lazy: async () => {
              const { UpdateComponent: UpdateObjetComponent } = await import(
                "./components/objets/update"
              );
              return { Component: UpdateObjetComponent };
            },
          },
        ],
      },
      {
        path: "interests/",
        children: [
          {
            path: "",
            lazy: async () => {
              const { ListInterestsComponent: ListInterestsComponent } =
                await import("./components/interests/list");
              return { Component: ListInterestsComponent };
            },
          },
          {
            path: "list",
            lazy: async () => {
              const { ListInterestsComponent: ListInterestsComponent } =
                await import("./components/interests/list");
              return { Component: ListInterestsComponent };
            },
          },
          {
            path: "create",
            lazy: async () => {
              const { CreateInterestsComponent: CreateInterestComponent } =
                await import("./components/interests");
              return { Component: CreateInterestComponent };
            },
          },
          {
            path: "edit/:id",
            lazy: async () => {
              const { UpdateInterestComponent } = await import(
                "./components/interests/update"
              );
              return { Component: UpdateInterestComponent };
            },
          },
        ],
      },
      {
        path: "matchs/",
        children: [
          {
            path: "",
            lazy: async () => {
              const { ListMatchsComponent: ListMatchsComponent } = await import(
                "./components/matchs/list"
              );
              return { Component: ListMatchsComponent };
            },
          },
          {
            path: "list",
            lazy: async () => {
              const { ListMatchsComponent: ListMatchsComponent } = await import(
                "./components/matchs/list"
              );
              return { Component: ListMatchsComponent };
            },
          },
          {
            path: "create",
            lazy: async () => {
              const { CreateMatchsComponent: CreateMatchsComponent } =
                await import("./components/matchs");
              return { Component: CreateMatchsComponent };
            },
          },
          {
            path: "edit/:id",
            lazy: async () => {
              const { UpdateMatchComponent } = await import(
                "./components/matchs/update"
              );
              return { Component: UpdateMatchComponent };
            },
          },
        ],
      },
    ],
  },
  {
    path: "/home",
    element: <App />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  // error-page
  {
    path: "/internal-server",
    element: <InternalServer />,
  },
  {
    path: "/forbidden",
    element: <Forbidden />,
  },
  {
    path: "/bad-request",
    element: <BadRequest />,
  },
  {
    path: "/unauthorized",
    element: <Unauthorized />,
  },
  {
    path: "/not-found",
    element: <NotFound />,
  },
]);
