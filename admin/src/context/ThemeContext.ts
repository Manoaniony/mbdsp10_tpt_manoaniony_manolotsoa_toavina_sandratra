import { createContext } from "react";

export type ThemeCustomType = {
  isDark: boolean | undefined,
  handleToggleClick: (() => void) | undefined
}

// Create the context
export const ThemeContext = createContext<ThemeCustomType>({
  isDark: undefined,
  handleToggleClick: undefined
});