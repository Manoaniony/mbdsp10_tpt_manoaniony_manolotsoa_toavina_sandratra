import { Form } from "antd";
import { createContext } from "react";
import { CustomFormProviderTypes } from "../types/CustomFormProvider.types";

export const FormContext = createContext<CustomFormProviderTypes>({
  Form,
  form: undefined,
  onFinish: () => { },
  onFinishFailed: () => { },
  initialValues: undefined,
});

