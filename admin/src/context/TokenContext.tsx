import { createContext, Dispatch, ReactNode, SetStateAction } from "react";

interface TokenContextType {
  token: string | undefined;
  setToken: Dispatch<SetStateAction<string | undefined>> | undefined;
}

// Create a context with a default value
export const TokenContext = createContext<TokenContextType>({
  token: undefined,
  setToken: undefined
});


export interface TokenProviderProps {
  children: ReactNode;
}