import { createContext, Dispatch, ReactNode, SetStateAction } from "react";

interface ErrorContextType {
  error: string | undefined;
  setError: Dispatch<SetStateAction<string | undefined>> | undefined;
}

// Create a context with a default value
export const ErrorContext = createContext<ErrorContextType>({
  error: undefined,
  setError: undefined
});


export interface ErrorProviderProps {
  children: ReactNode;
}