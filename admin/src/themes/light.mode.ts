import type { ThemeType } from "../types/Themes.types";

export const lightTheme: ThemeType = {
  token: {
    colorPrimary: '#1890ff',
    colorBgContainer: '#ffffff',
    colorText: '#000000'
    // Add other customizations here
  },
};