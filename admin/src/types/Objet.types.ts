import { Annonce } from "./Annonce.types";
import { SelectValueType } from "./SelectValue.types";
import { User } from "./User.types";

export type Objet = {
  id?: number;
  nom?: string;
  description?: string;
  categorie?: string;
  etat?: string;
  photo?: unknown;
  user?: SelectValueType | User;
  annonce?: unknown;
  interet?: unknown;
};

export type CreateObjetType = {
  nom?: string;
  description?: string;
  categorie?: string;
  etat?: string;
  photo?: unknown;
  user?: User | { id?: number };
  annonce?: SelectValueType | Annonce | { id?: number };
  interet?: unknown;
};

export type UpdateObjetType = {
  id?: number;
  nom?: string;
  description?: string;
  categorie?: string;
  etat?: string;
  photo?: unknown;
  user?: User | { id?: number };
  annonce?: SelectValueType | Annonce | { id?: number };
  interet?: unknown;
};
