import { AxiosHeaders } from "axios";
import { DataSuccessType } from "./DataSuccess.types";
import { DataSuccessPaginateType } from "./DataSuccessPaginate.types";

export type RequestSuccessType<T> = {
  config?: unknown;
  data?: DataSuccessType<T> | DataSuccessPaginateType<T> | T;
  headers?: AxiosHeaders;
  request?: XMLHttpRequest;
  status?: number;
  statusText?: string;
};
