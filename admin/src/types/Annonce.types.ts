import { Dayjs } from "dayjs";
import { Objet } from "./Objet.types";
import { SelectValueType } from "./SelectValue.types";
import { User } from "./User.types";

export type Annonce = {
  id?: number;
  libelle?: string;
  descs?: string;
  etat?: string;
  datePublication?: string | Dayjs;
  user?: User | SelectValueType | { id?: number };
  objets?: Objet[] | null | SelectValueType[];
  objet: Objet | SelectValueType | { id?: number };
};

export type CreateAnnonceType = {
  libelle?: string;
  descs?: string;
  etat?: string;
  datePublication?: string | Date | Dayjs;
  user?: User | SelectValueType | { id?: number };
  objets?: Objet[] | null | { id: number }[];
  objet?: Objet | SelectValueType | { id?: number };
};

export type UpdateAnnonceType = {
  id: number;
  libelle?: string;
  descs?: string;
  etat?: string;
  datePublication?: string | Date | Dayjs;
  user?: User | SelectValueType | { id?: number };
  objets?: Objet[] | null;
  objet?: Objet | SelectValueType | { id?: number };
};
