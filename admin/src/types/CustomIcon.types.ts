import { ReactNode } from "react"
import { IconContext } from "react-icons"

export const defaultIconContext: IconContext = {
  color: undefined,
  size: undefined,
  className: undefined,
  style: undefined,
  attr: undefined
}

export type CustomIconType = {
  propsIconContext?: IconContext
  icon: ReactNode
}