export type PaginationType = {
  page: number | undefined,
  size: number | undefined,
  totalPages: number | undefined,
  totalElement: number | undefined
}