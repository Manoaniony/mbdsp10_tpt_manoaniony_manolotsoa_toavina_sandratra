import { AxiosHeaders } from "axios"
import { DataSuccessLogin } from "./DataSuccess.login.types"

export type RequestSuccessLoginType = {
  config?: unknown
  data?: DataSuccessLogin<string>
  headers?: AxiosHeaders
  request?: XMLHttpRequest
  status?: number
  statusText?: string
}