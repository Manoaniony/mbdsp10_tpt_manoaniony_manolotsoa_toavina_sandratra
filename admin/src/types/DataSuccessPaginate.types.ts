export type PaginateObject<T> = {
  content?: T[]
  totalPages?: number
  totalElements?: number
}

export type DataSuccessPaginateType<T> = {
  objects?: PaginateObject<T>
  message?: string
  status?: number
}