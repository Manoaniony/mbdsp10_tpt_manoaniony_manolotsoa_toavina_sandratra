export type DataSuccessType<T> = {
  ok: boolean
  objects?: T | T[]
  message?: string
  status: number
}