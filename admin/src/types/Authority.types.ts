export type Authority = {
  id?: number
  authority: "ROLE_USER" | "ROLE_ADMIN"
}