import { Annonce } from "./Annonce.types";
import { Objet } from "./Objet.types";
import { SelectValueType } from "./SelectValue.types";
import { User } from "./User.types";

export type InterestType = {
  id?: number;
  annonce?: Annonce | null;
  user?: User | null;
  objets?: Objet[] | null;
};

export type CreateInterestType = {
  annonce?: Annonce | SelectValueType | null;
  user?: User | SelectValueType | null;
  objets?: Objet[] | SelectValueType[] | null;
};

export type UpdateInterestType = {
  id?: number;
  annonce?: Annonce | SelectValueType | null;
  user?: User | SelectValueType | null;
  objets?: Objet[] | SelectValueType[] | null;
};
