export type ThemeType = {
  token: {
    colorPrimary?: string,
    colorBgContainer?: string,
    colorText?: string,
    colorSuccess?: string,
    btnSuccessBg?: string,
    btnSuccessColor?: string
  }
}