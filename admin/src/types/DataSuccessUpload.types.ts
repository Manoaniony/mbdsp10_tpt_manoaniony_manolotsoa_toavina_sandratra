type ItemUploadType = {
  url: string;
  id: string;
};

export type DataSuccessUploadType = {
  message: string;
  data: ItemUploadType[];
};
