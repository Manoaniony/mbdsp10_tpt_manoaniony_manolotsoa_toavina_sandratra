import { Dayjs } from "dayjs";
import { Objet } from "./Objet.types";
import { SelectValueType } from "./SelectValue.types";
import { User } from "./User.types";

export type MatchType = {
  id?: number;
  dateMatch?: string | Dayjs;
  user1?: User | SelectValueType | null;
  user2?: User | SelectValueType | null;
  objet1?: Objet[] | SelectValueType[];
  objet2?: Objet[] | SelectValueType[];
  pointdeRecup?: { lat?: number; long?: number };
};

export type CreateMatchType = {
  dateMatch?: string | Dayjs;
  user1?: User | SelectValueType | null;
  user2?: User | SelectValueType | null;
  objet1?: Objet[] | SelectValueType[];
  objet2?: Objet[] | SelectValueType[];
  pointdeRecup?: { lat?: number; long?: number };
};

export type UpdateMatchType = {
  dateMatch?: string | Dayjs;
  user1?: User | SelectValueType | null;
  user2?: User | SelectValueType | null;
  objet1?: Objet[] | SelectValueType[];
  objet2?: Objet[] | SelectValueType[];
  pointdeRecup?: { lat?: number; long?: number };
};
