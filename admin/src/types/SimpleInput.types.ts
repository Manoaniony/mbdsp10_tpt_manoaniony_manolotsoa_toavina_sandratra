import { Form } from "antd";
import { Rule } from "antd/es/form";
import { ReactNode } from "react";

export type SimpleInputTypes<T> = {
  Form: typeof Form;
  name?: never[] | keyof T;
  label?: string;
  hidden?: boolean;
  rules?: Rule[];
  customInput?: ReactNode;
  disabled?: boolean;
  hasFeedback?: boolean;
  valuePropName?: "checked";
  onChange?: (e: never) => void;
  onPressEnter?: () => void;
  maxLength?: number;
  spaceNoWrap?: boolean;
}