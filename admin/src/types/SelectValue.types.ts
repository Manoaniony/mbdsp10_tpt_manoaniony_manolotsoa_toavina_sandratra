export type SelectValueType = {
  label?: string;
  key?: number | string;
  value?: number | string;
}