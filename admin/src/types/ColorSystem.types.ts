export type ColorSystemTypes = {
  success: string;
  info: string;
  warning: string;
  error: string;
}