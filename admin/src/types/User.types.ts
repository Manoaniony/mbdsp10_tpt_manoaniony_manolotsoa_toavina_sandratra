import { Authority } from "./Authority.types"

export type User = {
  id?: number
  authorities?: Authority[]
  username: string
  accountNonExpired?: boolean
  accountNonLocked?: boolean
  credentialsNonExpired?: boolean
  enabled?: boolean
  adresse?: boolean
  tel?: string
  email: string
  nom: string
  pointsDeRecuperation?: unknown[]
}