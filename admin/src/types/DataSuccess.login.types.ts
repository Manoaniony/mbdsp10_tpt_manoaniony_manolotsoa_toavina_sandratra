export type DataSuccessLogin<T> = {
  ok: boolean
  objects: T | T[]
  message?: string
  status?: number
}