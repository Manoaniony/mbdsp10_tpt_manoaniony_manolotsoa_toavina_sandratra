import { AxiosError } from "axios";
import type { Objet, RequestSuccessType } from "../../types";
import { request } from "../../utils";
import { CreateObjetType, UpdateObjetType } from "../../types/Objet.types";

export const getAllObjets: (params?: {
  page?: number;
  size?: number;
  nom?: string;
}) => Promise<RequestSuccessType<Objet[]> | AxiosError> = (params?: {
  page?: number;
  size?: number;
  nom?: string;
}) => {
  return request({
    url: "/api/objets/getAll",
    params,
    method: "get",
  }) as Promise<RequestSuccessType<Objet[]> | AxiosError>;
};

export const deleteObjet: (
  data: Objet
) => Promise<RequestSuccessType<Objet> | AxiosError> = (data: Objet) => {
  return request({
    url: `/api/objets/delete/${data?.id}`,
    method: "delete",
  }) as Promise<RequestSuccessType<Objet> | AxiosError>;
};

export const saveObjet: (
  data: CreateObjetType
) => Promise<RequestSuccessType<Objet> | AxiosError> = (
  data: CreateObjetType
) => {
  return request({
    url: `/api/objets/save`,
    data,
    method: "post",
  }) as Promise<RequestSuccessType<Objet> | AxiosError>;
};

export const getObjet: (params: {
  id: number;
}) => Promise<RequestSuccessType<Objet> | AxiosError> = (params: {
  id: number;
}) => {
  const { id } = params;
  return request({
    url: `/api/objets/getById/${id}`,
    method: "get",
  }) as Promise<RequestSuccessType<Objet> | AxiosError>;
};

export const updateObjet: (
  data: UpdateObjetType
) => Promise<RequestSuccessType<Objet> | AxiosError> = (
  data: UpdateObjetType
) => {
  return request({
    url: `/api/objets/update/${data?.id}`,
    data,
    method: "put",
  }) as Promise<RequestSuccessType<Objet> | AxiosError>;
};
