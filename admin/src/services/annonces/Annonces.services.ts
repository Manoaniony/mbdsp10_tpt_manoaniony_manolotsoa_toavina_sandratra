import { AxiosError } from "axios";
import { RequestSuccessType } from "../../types";
import { Annonce, CreateAnnonceType } from "../../types/Annonce.types";
import { request } from "../../utils";

export const getAllAnnonces: (params?: {
  page?: number;
  size?: number;
}) => Promise<RequestSuccessType<Annonce[]> | AxiosError> = (params?: {
  page?: number;
  size?: number;
}) => {
  return request({
    url: "/api/annonces/getAll",
    params,
    method: "get",
  }) as Promise<RequestSuccessType<Annonce[]> | AxiosError>;
};

export const deleteAnnonce: (
  data: Annonce
) => Promise<RequestSuccessType<Annonce> | AxiosError> = (data: Annonce) => {
  return request({
    url: `/api/annonces/delete/${data?.id}`,
    method: "delete",
  }) as Promise<RequestSuccessType<Annonce> | AxiosError>;
};

export const saveAnnonce: (
  data: CreateAnnonceType
) => Promise<RequestSuccessType<Annonce> | AxiosError> = (
  data: CreateAnnonceType
) => {
  return request({
    url: `/api/annonces/save`,
    data,
    method: "post",
  }) as Promise<RequestSuccessType<Annonce> | AxiosError>;
};
