import { AxiosError } from "axios";
import type { RequestSuccessLoginType, RequestSuccessType, User } from "../../types";
import type { DataSuccessType } from "../../types/DataSuccess.types";
import { request } from "../../utils";

export const getUserMock: () => Promise<DataSuccessType<User> | AxiosError> = () => {
  return request({ url: "/api/mock_users" }) as Promise<DataSuccessType<User>>
}

export const postUserMock: () => Promise<DataSuccessType<User> | AxiosError> = () => {
  return request({ url: "/api/mock_users", method: "post" }) as Promise<DataSuccessType<User>>
}

export const login: (credential: { username: string, password: string }) => Promise<RequestSuccessLoginType | AxiosError> = (credential: { username: string, password: string }) => {
  return request({ url: "/api/auth/login", method: "post", data: credential }) as Promise<RequestSuccessLoginType | AxiosError>
}

export const getAllUsers: (params?: { page?: number, size?: number, username?: string }) => Promise<RequestSuccessType<User[]> | AxiosError> = (params?: { page?: number, size?: number }) => {
  return request({
    url: "/api/users/getAll", params, method: "get"
  }) as Promise<RequestSuccessType<User[]> | AxiosError>
}

export const getUser: (params: { id: number }) => Promise<RequestSuccessType<User> | AxiosError> = (params: { id: number }) => {
  const { id } = params
  return request({
    url: `/api/users/getById/${id}`, method: "get"
  }) as Promise<RequestSuccessType<User> | AxiosError>
}

export const registerUser: (data: User) => Promise<RequestSuccessType<User> | AxiosError> = (data: User) => {
  return request({
    url: "/api/users/save",
    data,
    method: "post"
  }) as Promise<RequestSuccessType<User> | AxiosError>
}

export const updateUser: (data: User) => Promise<RequestSuccessType<User> | AxiosError> = (data: User) => {
  return request({
    url: `/api/users/update/${data?.id}`,
    data,
    method: "put"
  }) as Promise<RequestSuccessType<User> | AxiosError>
}

export const deleteUser: (data: User) => Promise<RequestSuccessType<User> | AxiosError> = (data: User) => {
  return request({
    url: `/api/users/delete/${data?.id}`,
    method: "delete"
  }) as Promise<RequestSuccessType<User> | AxiosError>
}