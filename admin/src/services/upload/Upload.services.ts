import { AxiosError } from "axios";
import { RequestSuccessType } from "../../types";
import { requestBlob } from "../../utils";

export const upload: (data: {
  file: { originFileObj?: unknown };
}) => Promise<RequestSuccessType<unknown> | AxiosError> = (data: {
  file: { originFileObj?: unknown };
}) => {
  const formData = new FormData();

  formData.append("files", data?.file?.originFileObj as Blob);
  return requestBlob({
    url: `/api/upload_files`,
    data: formData,
    method: "post",
  }) as Promise<RequestSuccessType<unknown> | AxiosError>;
};
