import { AxiosError } from "axios";
import {
  CreateInterestType,
  InterestType,
  RequestSuccessType,
} from "../../types";
import { request } from "../../utils";

export const getAllInterests: (params?: {
  page?: number;
  size?: number;
}) => Promise<RequestSuccessType<InterestType[]> | AxiosError> = (params?: {
  page?: number;
  size?: number;
}) => {
  return request({
    url: "/api/interets/getAll",
    params,
    method: "get",
  }) as Promise<RequestSuccessType<InterestType[]> | AxiosError>;
};

export const deleteInterest: (
  data: InterestType
) => Promise<RequestSuccessType<InterestType> | AxiosError> = (
  data: InterestType
) => {
  return request({
    url: `/api/interets/delete/${data?.id}`,
    method: "delete",
  }) as Promise<RequestSuccessType<InterestType> | AxiosError>;
};

export const saveInterest: (
  data: CreateInterestType
) => Promise<RequestSuccessType<InterestType> | AxiosError> = (
  data: CreateInterestType
) => {
  return request({
    url: `/api/interets/save`,
    data,
    method: "post",
  }) as Promise<RequestSuccessType<InterestType> | AxiosError>;
};
