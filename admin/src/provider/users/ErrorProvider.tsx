import React, { useState } from "react";
import { ErrorContext, ErrorProviderProps } from "../../context/errors/ErrorContext";

export const ErrorProvider: React.FC<ErrorProviderProps> = ({ children }) => {
  const [error, setError] = useState<string | undefined>();

  return (
    <ErrorContext.Provider value={{ error, setError }}>
      {children}
    </ErrorContext.Provider>
  );
};
