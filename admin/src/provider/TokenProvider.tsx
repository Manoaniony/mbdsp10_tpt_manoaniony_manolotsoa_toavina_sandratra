import React, { useCallback, useState } from "react";
import { TokenContext, TokenProviderProps } from "../context/TokenContext";

export const TokenProvider: React.FC<TokenProviderProps> = ({ children }) => {
  const [token, setToken] = useState<string | undefined>();

  const getToken = useCallback((): string | undefined => {
    return token || localStorage.getItem("token") || undefined;
  }, [token]);

  return (
    <TokenContext.Provider value={{ token: getToken(), setToken }}>
      {children}
    </TokenContext.Provider>
  );
};
