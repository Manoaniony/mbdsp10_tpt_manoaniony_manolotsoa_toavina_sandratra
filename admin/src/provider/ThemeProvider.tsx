import { PropsWithChildren, useState } from "react";
import { ThemeContext } from "../context/ThemeContext";
import { ConfigProvider } from "antd";
import { darkTheme } from "../themes/dark.mode";
import { lightTheme } from "../themes/light.mode";

export const ThemeProvider = (props: PropsWithChildren) => {
  const { children } = props
  const [isDark, setIsDark] = useState<boolean>(false);
  const handleToggleClick: () => void = () => {
    setIsDark((previous: boolean) => !previous)
  }

  return (
    <ThemeContext.Provider value={{ isDark, handleToggleClick }}>
      <ConfigProvider
        theme={isDark ? darkTheme : lightTheme}
      >
        {children}
      </ConfigProvider>
    </ThemeContext.Provider>
  )
}