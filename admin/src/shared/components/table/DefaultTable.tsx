import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getPaginationRowModel,
  PaginationState,
  useReactTable,
} from "@tanstack/react-table";
import { useEffect, useState } from "react";
import { CiEdit } from "react-icons/ci";
import { MdDeleteOutline } from "react-icons/md";
import { PopUp } from "../../ui/popup";
import { Alert, Button } from "antd";
import { Spinner } from "../../ui/spinner";
import { CustomButtonFilled } from "../../ui/button";
// import { Spinner } from "../../ui/spinner";

type DefaultTableType<T> = {
  columns: ColumnDef<T>[];
  data: T[];
  isCanEdit?: boolean;
  isCanDelete?: boolean;
  isCanShow?: boolean;
  onEdit?: (object?: unknown) => void;
  onDelete?: (
    toggleLoadingDelete: () => void,
    toggleShowModalDelete: () => void,
    object?: unknown
  ) => void;
};

export const DefaultTable = <T,>({
  columns,
  data,
  isCanShow,
  isCanEdit,
  isCanDelete,
  onEdit,
  onDelete,
}: DefaultTableType<T>) => {
  const [pagination, setPagination] = useState<PaginationState>({
    pageIndex: 0,
    pageSize: 10,
  });
  const [isShowModalDelete, setIsShowModalDelete] = useState<boolean>(false);
  const [isLoadingDelete, setIsLoadingDelete] = useState<boolean>(false);
  const [dataToDelete, setDataToDelete] = useState<unknown>();

  const handleShowModalDelete = () => {
    setIsShowModalDelete((prev: boolean) => !prev);
  };

  const onLoadingDeleteHandler = () => {
    setIsLoadingDelete((prev: boolean) => !prev);
  };

  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    onPaginationChange: setPagination,
    //no need to pass pageCount or rowCount with client-side pagination as it is calculated automatically
    state: {
      pagination,
    },
  });

  useEffect(() => {
    if (dataToDelete) {
      console.log("Data To Delete ", dataToDelete);
    }
    return () => {};
  }, [dataToDelete]);
  return (
    <>
      <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id} scope="col" className="px-6 py-3">
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
              {(isCanEdit || isCanDelete || isCanShow) &&
              table.getHeaderGroups()[0]?.headers.length ? (
                <th className="table__header_item table__row-pair">Actions</th>
              ) : (
                ""
              )}
            </tr>
          ))}
        </thead>
        <tbody>
          {!table.getRowModel().rows.length ? (
            <tr>
              <td colSpan={4}>
                <div className="flex justify-center p-[12px]">
                  <span>Aucun donnees disponible</span>
                </div>
              </td>
            </tr>
          ) : (
            table.getRowModel().rows.map((row) => (
              <tr
                className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-gray-800 border-b dark:border-gray-700"
                key={row.id}
              >
                {row.getVisibleCells().map((cell) => (
                  <td key={cell.id} className="px-6 py-4">
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </td>
                ))}
                {(isCanEdit || isCanDelete || isCanShow) &&
                table.getHeaderGroups()[0]?.headers?.length ? (
                  <td className="py-4">
                    <div className="w-full flex gap-[5px] p-2 whitespace-nowrap cursor-pointer">
                      {isCanEdit && (
                        <CiEdit
                          onClick={() => onEdit && onEdit(row?.original)}
                        />
                      )}
                      {isCanDelete && (
                        <MdDeleteOutline
                          onClick={() => {
                            handleShowModalDelete();
                            setDataToDelete(row?.original);
                          }}
                        />
                      )}
                    </div>
                  </td>
                ) : (
                  ""
                )}
              </tr>
            ))
          )}
          ‚
        </tbody>
      </table>

      <PopUp
        centered
        title="Suppression"
        isModalOpen={isShowModalDelete}
        hanldeCloseModal={handleShowModalDelete}
      >
        <>
          <Alert
            message="Voulez-vous vraiment supprimer cette élément ?"
            type="error"
            showIcon
          />
          <div className=" flex gap-[20px] my-[20px] justify-end">
            <Button onClick={handleShowModalDelete}>Annuler</Button>
            <CustomButtonFilled
              onClick={() => {
                onDelete &&
                  onDelete(
                    onLoadingDeleteHandler,
                    handleShowModalDelete,
                    dataToDelete
                  );
              }}
              color="warning"
            >
              {isLoadingDelete ? <Spinner /> : "OK"}
            </CustomButtonFilled>
          </div>
        </>
      </PopUp>
    </>
  );
};
