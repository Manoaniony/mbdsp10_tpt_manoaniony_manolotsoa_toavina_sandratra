import { Typography } from "antd"
import { FC } from "react";
import { FormDebuggerType } from "../../../types/FormDebugger.types";

export const FormDebugger: FC<FormDebuggerType> = ({
  Form,
  form
}) => {
  return (
    <>
      {
        Form && (
          <Form.Item noStyle shouldUpdate>
            {() => (
              <Typography>
                <pre>{JSON.stringify(form?.getFieldsValue(), null, 2)}</pre>
              </Typography>
            )}
          </Form.Item>
        )
      }
    </>
  )
}
