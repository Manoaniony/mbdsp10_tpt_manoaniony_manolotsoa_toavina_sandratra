import { MenuProps } from "antd";
import type { MenuType } from "../../../types";
import { CustomIcon } from "../../ui/icon/CustomIcon";
import { FaCircle, FaExchangeAlt, FaHeart, FaUser } from "react-icons/fa";
import { FaSignsPost } from "react-icons/fa6";

type MenuItem = Required<MenuProps>["items"][number];

export const Menus = ({ isOpen: open }: MenuType) => {
  const items: MenuItem[] = [
    {
      key: "users",
      label: open ? <div className="ml-4">Utilisateurs</div> : null,
      icon: (
        <CustomIcon
          icon={<FaUser />}
          propsIconContext={{ className: "text-gray-950" }}
        />
      ),
      children: [
        {
          key: "users-create",
          label: "Create",
        },
        // {
        //   key: 'users-edit',
        //   label: 'Edit',
        // },
        {
          key: "users-list",
          label: "List",
        },
      ],
    },
    {
      key: "objets",
      label: open ? <div className="ml-4">Objets</div> : null,
      icon: (
        <CustomIcon
          icon={<FaCircle />}
          propsIconContext={{ className: "text-gray-950" }}
        />
      ),
      children: [
        {
          key: "objets-create",
          label: "Create",
        },
        // {
        //   key: 'annonces-edit',
        //   label: 'Edit',
        // },
        {
          key: "objets-list",
          label: "List",
        },
      ],
    },
    {
      key: "annonces",
      label: open ? <div className="ml-4">Annonces</div> : null,
      icon: (
        <CustomIcon
          icon={<FaSignsPost />}
          propsIconContext={{ className: "text-gray-950" }}
        />
      ),
      children: [
        {
          key: "annonces-create",
          label: "Create",
        },
        // {
        //   key: 'annonces-edit',
        //   label: 'Edit',
        // },
        {
          key: "annonces-list",
          label: "List",
        },
      ],
    },
    {
      key: "interests",
      label: open ? <div className="ml-4">Interests</div> : null,
      icon: (
        <CustomIcon
          icon={<FaHeart />}
          propsIconContext={{ className: "text-gray-950" }}
        />
      ),
      children: [
        {
          key: "interests-create",
          label: "Create",
        },
        // {
        //   key: 'annonces-edit',
        //   label: 'Edit',
        // },
        {
          key: "interests-list",
          label: "List",
        },
      ],
    },
    {
      key: "matchs",
      label: open ? <div className="ml-4">Matchs</div> : null,
      icon: (
        <CustomIcon
          icon={<FaExchangeAlt />}
          propsIconContext={{ className: "text-gray-950" }}
        />
      ),
      children: [
        {
          key: "matchs-create",
          label: "Create",
        },
        // {
        //   key: 'annonces-edit',
        //   label: 'Edit',
        // },
        {
          key: "matchs-list",
          label: "List",
        },
      ],
    },
  ];

  return items;
};
