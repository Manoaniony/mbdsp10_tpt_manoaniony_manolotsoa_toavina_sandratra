import { Spin } from 'antd';
import { LoadingOutlined } from "@ant-design/icons";


const antIcon = (
  <LoadingOutlined
    style={{ fontSize: 18, color: "#fff", marginLeft: 20 }}
    spin
  />
);

export const Spinner = () => <Spin indicator={antIcon} />