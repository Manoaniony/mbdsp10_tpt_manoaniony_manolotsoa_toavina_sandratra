import { Button, FormInstance, Upload } from "antd"
import { UploadChangeParam, UploadFile, UploadProps } from "antd/es/upload";
import { FC, useEffect, useState } from "react"
import { FaCloudUploadAlt } from "react-icons/fa";
import { Link } from "../link";

interface InputFile {
  buttonText?: string;
  form?: FormInstance<never> | null;
  url?: string;
  selector?: string;
  isDisabled?: boolean;
  accept?: string;
}

export const SimpleUploadFile: FC<UploadProps & InputFile> = (props) => {
  const { buttonText, form, url, selector, isDisabled, accept } = props;
  const [loading, setLoading] = useState(false);
  const [docUrl, setDocUrl] = useState<string>();
  // const [fileImage, setFileImage] = useState<RcFile | null | undefined>(null);
  const { setFieldValue } = form as FormInstance<never>

  useEffect(() => {
    if (url) {
      setDocUrl(url);
    }
  }, [url])

  const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
    try {
      setLoading(true)
      // getBase64(info.file as RcFile, (url) => {
      //     // setFileImage(info?.file as RcFile)
      //     setLoading(false);
      //     setDocUrl(url);
      // });
      setFieldValue(selector, info.fileList[0]);
      setDocUrl("")
    } catch (error) {
      setDocUrl(undefined);
    } finally {
      setLoading(false);
    }
  };

  const uploadButton = () => {
    return (
      loading ? <>Chargement...</> : <Button disabled={isDisabled} className="w-full flex items-center" icon={<FaCloudUploadAlt />}>{buttonText || "Télécharger"}</Button>
    )
  };

  return (
    <div className="flex-col gap-[10px] items-center">
      <div className="justify-start">
        <Upload
          {...props}
          disabled={isDisabled}
          accept={accept || "*"}
          showUploadList={true}
          beforeUpload={() => false}
          onChange={handleChange}
          multiple={false}
          maxCount={1}
          onRemove={() => setFieldValue(selector, null)}
        >
          {uploadButton()}
        </Upload>
      </div>
      <div>
        {docUrl ? <Link path={docUrl}>{docUrl}</Link> : null}
      </div>
    </div>
  )
}
