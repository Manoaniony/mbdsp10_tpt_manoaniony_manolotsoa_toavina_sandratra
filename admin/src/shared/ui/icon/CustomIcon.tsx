import { IconContext } from "react-icons"
import { CustomIconType, defaultIconContext } from "../../../types/CustomIcon.types"

export const CustomIcon = (props: CustomIconType) => {
  const { icon, propsIconContext } = props;
  return (
    <IconContext.Provider value={propsIconContext || defaultIconContext}>
      {icon}
    </IconContext.Provider>
  )
}