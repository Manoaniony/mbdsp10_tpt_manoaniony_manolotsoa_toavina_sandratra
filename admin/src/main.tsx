import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {
  RouterProvider,
} from "react-router-dom";
import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'
import { router } from './router';
import { ThemeProvider } from './provider/ThemeProvider';
import { TokenProvider } from './provider/TokenProvider';

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <TokenProvider>
        <ThemeProvider>
          <RouterProvider router={router} />
        </ThemeProvider>
      </TokenProvider>
    </QueryClientProvider>
  </React.StrictMode>,
)
