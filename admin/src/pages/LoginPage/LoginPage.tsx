import { Button, Card, Form, Input } from "antd"
import Title from "antd/es/typography/Title"
import { FormProvider } from "../../provider/FormProvider";
import { SimpleInput } from "../../shared/ui/simpleInput";
import { ChangeEvent, useContext, useEffect, } from "react";
import { useMutation } from "@tanstack/react-query";
import { login } from "../../services/users/Users.services";
import { RequestSuccessLoginType } from "../../types";
import { AxiosError } from "axios";
import { useNavigate } from "react-router-dom";
import { TokenContext } from "../../context/TokenContext";

export const LoginPage = () => {
  const [form] = Form.useForm();
  const { Password } = Input
  const navigate = useNavigate();
  const { setToken } = useContext(TokenContext);

  // Mutation
  const { data, isSuccess, isPending, mutate } = useMutation<RequestSuccessLoginType | AxiosError, Error, {
    username: string;
    password: string;
  }, unknown>({
    mutationFn: login,
  });

  useEffect(() => {
    if (isSuccess && (data as RequestSuccessLoginType)?.data?.objects && setToken) {
      setToken(() => {
        const token = (data as RequestSuccessLoginType)?.data?.objects as string
        localStorage.setItem("token", token)
        return token
      })
      navigate("/admin/users")
    } else {
      navigate("/login")
    }
    return () => {

    };
  }, [data, navigate, isSuccess, setToken]);

  if (isPending) {
    return <>Chargement...</>
  }


  const handleLogin: ((values: unknown) => void) = (values: unknown) => {
    const { username, password } = values as never
    mutate({ username, password })
  }
  return (
    <div className="login__container">
      <div className="flex justify-center items-center">
        <Card className="w-[600px]">
          <div className="login__content-card">
            <div className="login__content-card-item ">
              <div className="login__form">
                <div className="login__form-header">
                  <Title className="text-center">Se connecter</Title>
                </div>
                <div>
                  <FormProvider
                    Form={Form}
                    form={form}
                    onFinish={(values: unknown) => {
                      handleLogin(values)
                    }}
                    onFinishFailed={(errorInfo) => {
                      console.log("ERROR INFO ", errorInfo);
                    }}
                    debug={import.meta.env.DEV}
                    initialValues={undefined}>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="username"
                        label="Nom d'utilisateur"
                        rules={[
                          {
                            required: true, message: 'Le champ Nom d\'utilisateur est réquis.'
                          }
                        ]}
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="password"
                        label="Mot de passe"
                        rules={[
                          {
                            required: true, message: 'Le champ Mot de passe est réquis.'
                          },
                        ]}
                        customInput={
                          <Password
                            onChange={(e: ChangeEvent<HTMLInputElement>) => console.log((e?.target?.value))}
                          />
                        }
                      />
                    </div>
                    <div className="flex flex-col justify-center">
                      <Button
                        type="primary"
                        htmlType="submit"
                      >
                        Se connecter
                      </Button>
                    </div>
                  </FormProvider>

                </div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  )

}