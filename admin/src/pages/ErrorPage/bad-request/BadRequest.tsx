import { CustomError } from "../../../shared/components/error";

export const BadRequest = () => {
  return (
    <CustomError status={400} message="Votre requête ne peut pas être traiter" />
  )
}