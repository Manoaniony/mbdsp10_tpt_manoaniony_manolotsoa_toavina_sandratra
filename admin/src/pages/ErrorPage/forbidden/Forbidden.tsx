import { CustomError } from "../../../shared/components/error";

export const Forbidden = () => {
  return (
    <CustomError status={403} message="Vous n'avez les permissions necessaires pour accéder cette page." />
  )
}