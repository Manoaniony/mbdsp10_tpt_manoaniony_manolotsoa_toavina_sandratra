import { CustomError } from "../../../shared/components/error";

export const NotFound = () => {
  return (
    <>
      <CustomError status={404} message="Cette page n'existe pas" />
    </>
  )
}