import axios, { AxiosError } from "axios";

const client = axios.create({
  baseURL: import.meta.env.URL_API_BACKEND_NODE || "http://localhost:4000",
});

export const requestBlob = ({ ...options }) => {
  //   if (!options.url.includes("/api/auth/login")) {
  //     client.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem(
  //       "token"
  //     )}`;
  //   }
  console.log("PASS HERE");

  //   client.defaults.headers.common["Content-Type"] = "multipart/form-data";
  //   console.log("headers: ", client.defaults.headers);

  const onSuccess = (response: unknown) => response;
  const onError = (error: unknown) => {
    // optionnally catch errors and add additionnal logging here
    console.log("optionnally catch errors and add additionnal logging here");
    console.log(
      "Redirect to right way ",
      (error as AxiosError)?.response?.status
    );
    // switch ((error as AxiosError)?.response?.status) {
    //   case 500:
    //     window.location.href = "/internal-server"; // Adjust this URL as needed
    //     return;
    //   case 403:
    //     window.location.href = "/forbidden"; // Adjust this URL as needed
    //     return;
    //   case 400:
    //     window.location.href = "/bad-request"; // Adjust this URL as needed
    //     return;
    //   case 401:
    //     window.location.href = "/unauthorized"; // Adjust this URL as needed
    //     return;

    //   default:
    //     break;
    // }
    return error;
  };
  return client(options).then(onSuccess).catch(onError);
};
