import { InterestType } from "../types";

export const interestsMock: () => InterestType[] = () =>
  [
    {
      id: 1,
      annonce: { id: 1, libelle: "Annonce 1" },
      objets: [
        {
          id: 1,
          nom: "Nom1",
        },
        {
          id: 1,
          nom: "Objet2",
        },
      ],
      user: {
        id: 1,
        username: "user1",
      },
    },
    {
      id: 2,
      annonce: { id: 2, libelle: "Annonce 2" },
      objets: [
        {
          id: 1,
          nom: "Objet1",
        },
        {
          id: 1,
          nom: "Objet2",
        },
      ],
      user: {
        id: 2,
        username: "user2",
      },
    },
  ] as InterestType[];
