import { MatchType } from "../types/Match.types";

export const matchsMock: () => MatchType[] = () =>
  [
    {
      id: 1,
      dateMatch: "2024-08-26",
      objet1: [
        {
          id: 1,
          nom: "Nom1",
        },
        {
          id: 1,
          nom: "Objet2",
        },
      ],
      objet2: [
        {
          id: 3,
          nom: "Objet3",
        },
        {
          id: 4,
          nom: "Objet4",
        },
      ],
      user1: {
        id: 1,
        username: "user1",
      },
      user2: {
        id: 2,
        username: "user2",
      },
    },
    {
      id: 2,
      dateMatch: "2024-08-26",
      objet1: [
        {
          id: 3,
          nom: "Nom3",
        },
        {
          id: 4,
          nom: "Objet4",
        },
      ],
      objet2: [
        {
          id: 5,
          nom: "Objet5",
        },
        {
          id: 6,
          nom: "Objet6",
        },
      ],
      user1: {
        id: 3,
        username: "user3",
      },
      user2: {
        id: 4,
        username: "user4",
      },
    },
  ] as MatchType[];
