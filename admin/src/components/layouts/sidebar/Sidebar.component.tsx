import { useEffect, useMemo, useState } from "react";
import { Menu, MenuProps } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
import { Menus } from "../../../shared/components/menus";
import {
  MAIN_MENU,
  MAIN_MENU_METHODS,
} from "../../../constants/menus_constants";

export const Sidebar = () => {
  const [open, setOpen] = useState(true);
  const navigate = useNavigate();

  const MENU_METHODS: string[] = useMemo(() => {
    return MAIN_MENU_METHODS;
  }, []);

  const MENU_KEYS: string[] = useMemo(() => {
    return MAIN_MENU;
  }, []);

  const [keys, setKeys] = useState<{
    opened: undefined | string[];
    selected: undefined | string[];
  }>();

  const { pathname } = useLocation();

  useEffect(() => {
    if (pathname) {
      setKeys(() => {
        const mainMenu: string = MENU_KEYS.find((menu: string) =>
          pathname.includes(menu)
        ) as string;
        const methodMenu: string = MENU_METHODS.find((menu: string) =>
          pathname.includes(menu)
        ) as string;
        const selected: string[] = [];
        const opened: string[] = [];
        if (mainMenu) {
          opened.push(mainMenu);
          selected.push(mainMenu);
          selected.push(`${mainMenu}-${methodMenu || "list"}`);
        }
        return {
          opened: selected.length > 0 ? opened : undefined,
          selected: selected.length > 0 ? selected : undefined,
        };
      });
    }
    return () => {};
  }, [MENU_KEYS, MENU_METHODS, pathname]);

  const convertPath = (path: string) => {
    return path
      .replace(/\/(\w+)-create$/, "/create")
      .replace(/\/(\w+)-edit$/, "/edit")
      .replace(/\/(\w+)-list$/, "/list");
  };

  const onClick: MenuProps["onClick"] = (e) => {
    const wrongPath = `/admin/${e.keyPath.reverse().join("/")}`;
    const rightPath = convertPath(wrongPath);
    navigate(rightPath);
  };

  return (
    <div
      className={` ${
        open ? "w-72" : "w-30"
      } bg-[#e5e5e5] h-full pt-8 relative duration-300`}
    >
      <div className="flex justify-center">
        <img
          src={"/control.png"}
          className={`absolute cursor-pointer -right-3 top-9 w-7 border-dark-purple
           border-2 rounded-full  ${!open && "rotate-180"}`}
          onClick={() => setOpen(!open)}
        />
        <div
          className={`flex gap-x-4 items-center ${!open && "justify-center"}`}
        >
          <img
            src="/vite.svg"
            className={`cursor-pointer duration-500 ${
              open && "rotate-[360deg]"
            }`}
          />
          {open && (
            <h1
              className={`text-gray-950 origin-left font-medium text-xl duration-200 ${
                !open && "scale-0"
              }`}
            >
              Tpt
            </h1>
          )}
        </div>
      </div>
      {keys && (
        <>
          <Menu
            className="bg-[#e5e5e5]"
            onClick={onClick}
            defaultSelectedKeys={keys?.selected}
            selectedKeys={keys?.selected}
            defaultOpenKeys={keys?.opened}
            mode="inline"
            items={Menus({ isOpen: open })}
            inlineCollapsed={!open}
          />
        </>
      )}
    </div>
  );
};
