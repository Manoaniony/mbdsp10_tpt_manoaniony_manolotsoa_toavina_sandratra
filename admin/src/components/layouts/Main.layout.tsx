import { Outlet, useNavigate } from "react-router-dom";
import { Sidebar } from "./sidebar";
import { CustomButtonFilled } from "../../shared/ui/button";
import { useCallback, useEffect } from "react";
import { useToken } from "../../hooks/useToken";

export const MainLayout = () => {
  const navigate = useNavigate();
  const { token } = useToken();

  const redirectToLogin = useCallback(() => {
    navigate("/login");
  }, [navigate]);

  useEffect(() => {
    if (!token) {
      redirectToLogin();
    }
    return () => {};
  }, [token, redirectToLogin]);

  const onDisconnect = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <>
      <div className="flex h-full">
        <div>
          <Sidebar />
        </div>
        <div className="flex flex-col w-full min-h-screen h-full">
          <div className="ml-auto m-4">
            <CustomButtonFilled onClick={onDisconnect} color="warning">
              Déconnexion
            </CustomButtonFilled>
          </div>
          <Outlet />
        </div>
      </div>
    </>
  );
};
