import { useNavigate } from "react-router-dom";
import { useNotification } from "../../../hooks/useNotification";
import Title from "antd/es/typography/Title";
import { FormComponent as FormObjet } from "../forms";
import { saveObjet } from "../../../services/objets/Objets.services";
import {
  DataSuccessType,
  DataSuccessUploadType,
  Objet,
  RequestSuccessType,
  SelectValueType,
} from "../../../types";
import { CreateObjetType } from "../../../types/Objet.types";
import { upload } from "../../../services/upload/Upload.services";
import { AxiosError } from "axios";

export const CreateComponent = () => {
  const navigate = useNavigate();

  const handleSuccessNavigation: () => void = () => {
    navigate("/admin/objets");
  };
  // Notification
  const { contextHolder, openNotification } = useNotification();

  const handleSubmit = (
    finalData: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => {
    let objet: CreateObjetType = {
      ...(finalData as Objet),
      user: {
        id: ((finalData as Objet)?.user as SelectValueType)?.value as number,
      },
      annonce: undefined,
      interet: undefined,
    };
    onLoadingHandler && onLoadingHandler();
    if (objet?.photo) {
      upload({
        file: objet?.photo as { originFileObj: unknown },
      })
        .then(
          (
            response: AxiosError<unknown, any> | RequestSuccessType<unknown>
          ) => {
            const { url } = (
              (response as RequestSuccessType<DataSuccessUploadType>)
                ?.data as DataSuccessUploadType
            )?.data[0];
            // update the photo url
            objet = {
              ...objet,
              photo: url,
              annonce: objet?.annonce
                ? { id: (objet?.annonce as SelectValueType)?.value as number }
                : undefined,
            };
            saveObjet(objet)
              .then((response: RequestSuccessType<Objet>) => {
                if (
                  response.status === 200 &&
                  (response.data as DataSuccessType<Objet>)?.ok
                ) {
                  openNotification({
                    type: "success",
                    title: "Création d'objet",
                    description: "Objet créé avec succès",
                    pauseOnHover: true,
                    showProgress: true,
                  });
                  setTimeout(() => {
                    onLoadingHandler && onLoadingHandler();
                    handleSuccessNavigation();
                    handleReset && handleReset();
                  }, 2000);
                } else {
                  openNotification({
                    type: "error",
                    title: "Création d'objet",
                    description: "Objet non créé",
                    pauseOnHover: true,
                    showProgress: true,
                  });
                }
              })
              .catch((error: unknown) => {
                console.log("if they are error ", error);
                openNotification({
                  type: "error",
                  title: "Erreur",
                  description: JSON.stringify(error),
                  pauseOnHover: true,
                  showProgress: true,
                });
              });
          }
        )
        .catch((error: unknown) => {
          throw error;
        });
    } else {
      saveObjet(objet)
        .then((response: RequestSuccessType<Objet>) => {
          if (
            response.status === 200 &&
            (response.data as DataSuccessType<Objet>)?.ok
          ) {
            openNotification({
              type: "success",
              title: "Création d'objet",
              description: "Objet créé avec succès",
              pauseOnHover: true,
              showProgress: true,
            });
            setTimeout(() => {
              onLoadingHandler && onLoadingHandler();
              handleSuccessNavigation();
              handleReset && handleReset();
            }, 2000);
          } else {
            openNotification({
              type: "error",
              title: "Création d'objet",
              description: "Objet non créé",
              pauseOnHover: true,
              showProgress: true,
            });
          }
        })
        .catch((error: unknown) => {
          console.log("if they are error ", error);
          openNotification({
            type: "error",
            title: "Erreur",
            description: JSON.stringify(error),
            pauseOnHover: true,
            showProgress: true,
          });
        });
    }
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Création d'objet</Title>
        <div>
          <FormObjet
            handleSubmit={(e, handleReset, onLoadingHandler) => {
              handleSubmit(e, handleReset, onLoadingHandler);
            }}
          />
        </div>
      </div>
    </>
  );
};
