import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { Objet, User } from "../../../types";

export const ObjetsColumns: () => ColumnDef<Objet>[] = () => {
  const columnHelper = createColumnHelper<Objet>();
  const columns: ColumnDef<Objet, never>[] = [
    columnHelper.accessor("nom", {
      header: () => <span>Nom</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("description", {
      header: () => <span>Description</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("categorie", {
      header: () => <span>Categorie</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("etat", {
      header: () => <span>Etat</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("user", {
      header: () => <span>Utilisateur</span>,
      cell: (props) => (props.getValue() as User)?.username,
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
  ];
  return columns as ColumnDef<Objet>[];
};
