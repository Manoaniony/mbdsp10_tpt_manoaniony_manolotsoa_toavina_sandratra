import { useMutation, useQuery } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  DataSuccessType,
  DataSuccessUploadType,
  Objet,
  RequestSuccessType,
  SelectValueType,
  User,
} from "../../../types";
import { AxiosError } from "axios";
import {
  getObjet,
  updateObjet,
} from "../../../services/objets/Objets.services";
import { UpdateObjetType } from "../../../types/Objet.types";
import { useNotification } from "../../../hooks/useNotification";
import Title from "antd/es/typography/Title";
import { FormObjetComponent } from "../..";
import { upload } from "../../../services/upload/Upload.services";
import { Annonce } from "../../../types/Annonce.types";

export const UpdateComponent = () => {
  const { id } = useParams();
  const [selected, setSelected] = useState<unknown>();

  const { data: response, error: errorRetrieveQuery } = useQuery<
    RequestSuccessType<Objet> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<Objet> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [Number(id)],
    queryFn: ({ queryKey }) =>
      getObjet({ id: queryKey[0] as unknown as number }),
    enabled: true,
  });

  // mutation
  const { mutateAsync: updateObjetAsync } = useMutation<
    RequestSuccessType<Objet> | AxiosError,
    Error,
    UpdateObjetType,
    unknown
  >({
    mutationFn: updateObjet,
  });

  const handleSuccessUpdate = () => {
    location.reload();
  };

  useEffect(() => {
    if (
      ((response as RequestSuccessType<Objet>)?.data as DataSuccessType<Objet>)
        ?.objects as Objet
    ) {
      const objetToEdit = (
        (response as RequestSuccessType<Objet>)?.data as DataSuccessType<Objet>
      )?.objects as Objet;
      setSelected({
        ...objetToEdit,
        user: objetToEdit?.user
          ? {
              value: (objetToEdit?.user as User)?.id,
              label: (objetToEdit?.user as User)?.username,
              key: (objetToEdit?.user as User)?.id,
            }
          : undefined,
        annonce: objetToEdit?.annonce
          ? {
              value: (objetToEdit?.annonce as Annonce)?.id,
              label: (objetToEdit?.annonce as Annonce)?.libelle,
              key: (objetToEdit?.annonce as Annonce)?.id,
            }
          : undefined,
      });
    }
    return () => {};
  }, [response]);

  useEffect(() => {
    if (selected) {
      console.log("selected ", selected);
    }
  }, [selected]);

  // Notification
  const { contextHolder, openNotification } = useNotification();

  if (errorRetrieveQuery) {
    return <>Un erreur est survenu</>;
  }

  const handleSubmit = async (
    finalData: unknown,
    _: undefined,
    onLoadingHandler?: () => void
  ) => {
    let dataToUpdate: UpdateObjetType = {
      ...(finalData as Objet),
      user: (finalData as Objet)?.user
        ? {
            id: ((finalData as Objet)?.user as SelectValueType)
              ?.value as number,
          }
        : undefined,
      annonce: (finalData as Objet)?.annonce
        ? {
            id: ((finalData as Objet)?.annonce as SelectValueType)
              ?.value as number,
          }
        : undefined,
    };
    // loading
    onLoadingHandler && onLoadingHandler();
    if (dataToUpdate?.photo) {
      upload({
        file: dataToUpdate?.photo as { originFileObj: unknown },
      })
        .then(
          (
            response: AxiosError<unknown, any> | RequestSuccessType<unknown>
          ) => {
            const { url } = (
              (response as RequestSuccessType<DataSuccessUploadType>)
                ?.data as DataSuccessUploadType
            )?.data[0];
            // update the photo url
            dataToUpdate = {
              ...dataToUpdate,
              photo: url,
            };
            updateObjetAsync(dataToUpdate as UpdateObjetType)
              .then((response: RequestSuccessType<Objet>) => {
                console.log("FinalData ", finalData as Objet);
                if (
                  response.status === 200 &&
                  (response.data as DataSuccessType<Objet>)?.ok
                ) {
                  openNotification({
                    type: "success",
                    title: "Modification d'objet",
                    description: "Objet modifié avec succès",
                    pauseOnHover: true,
                    showProgress: true,
                  });
                  setTimeout(() => {
                    onLoadingHandler && onLoadingHandler();
                    handleSuccessUpdate();
                  }, 2000);
                } else {
                  openNotification({
                    type: "error",
                    title: "Modification d'objet",
                    description: "Objet non modifié",
                    pauseOnHover: true,
                    showProgress: true,
                  });
                  onLoadingHandler && onLoadingHandler();
                }
              })
              .catch((error: unknown) => {
                console.log("if they are error ", error);
                openNotification({
                  type: "error",
                  title: "Erreur",
                  description: JSON.stringify(error),
                  pauseOnHover: true,
                  showProgress: true,
                });
              });
          }
        )
        .catch((error: unknown) => {
          throw error;
        });
    } else {
      dataToUpdate = {
        ...dataToUpdate,
        photo: (selected as Objet)?.photo,
      };
      updateObjetAsync(dataToUpdate as UpdateObjetType)
        .then((response: RequestSuccessType<Objet>) => {
          if (
            response.status === 200 &&
            (response.data as DataSuccessType<Objet>)?.ok
          ) {
            openNotification({
              type: "success",
              title: "Modification d'objet",
              description: "Objet modifié avec succès",
              pauseOnHover: true,
              showProgress: true,
            });
            setTimeout(() => {
              onLoadingHandler && onLoadingHandler();
            }, 2000);
          } else {
            openNotification({
              type: "error",
              title: "Modification d'objet",
              description: "Objet non modifié",
              pauseOnHover: true,
              showProgress: true,
            });
            onLoadingHandler && onLoadingHandler();
          }
        })
        .catch((error: unknown) => {
          console.log("if they are error ", error);
          openNotification({
            type: "error",
            title: "Erreur",
            description: JSON.stringify(error),
            pauseOnHover: true,
            showProgress: true,
          });
        });
    }
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Edition d'un objet</Title>
        <div>
          <FormObjetComponent
            selected={selected}
            handleSubmit={(e, _, onLoadingHandler) => {
              handleSubmit(e, undefined, onLoadingHandler);
            }}
          />
        </div>
      </div>
    </>
  );
};
