import { Button, Form, FormInstance, Input, Select } from "antd";
import { FormProvider } from "../../../provider/FormProvider";
import { FC, useCallback, useEffect, useState } from "react";
import { SimpleInput } from "../../../shared/ui/simpleInput";
import { Objet, SelectValueType, User } from "../../../types";
import { Spinner } from "../../../shared/ui/spinner";
import { useQueryUsers } from "../../../hooks/users/useUsers";
import { useUsersHandler } from "../../../hooks/users/useUsersHandler";
import { SimpleUploadFile } from "../../../shared/ui/simpleUploadFile";
import { useQueryAnnonces } from "../../../hooks/annonces/useAnnonces";
import { useAnnoncesHandler } from "../../../hooks/annonces/useAnnoncesHandler";
import { Annonce } from "../../../types/Annonce.types";

interface IForm {
  handleSubmit: (
    e: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => void;
  selected?: unknown;
}

export const FormComponent: FC<IForm> = ({ handleSubmit, selected }) => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { resetFields, setFieldsValue, setFieldValue } = form as FormInstance;
  const emptyArray: unknown[] = [];
  const [userSelected, setUserSelected] = useState<SelectValueType>();

  const { TextArea } = Input;

  // for user searching
  const {
    annonces,
    isLoading: isLoadingAnnonce,
    loadQuery: loadQueryAnc,
    error: errorQueryAnnonces,
  } = useQueryAnnonces();
  const { onSearch: onSearchAnnonce, onChange: onChangeAnnonce } =
    useAnnoncesHandler();

  const {
    users,
    isLoading: isLoadingUser,
    loadQuery,
    error: errorQueryUsers,
  } = useQueryUsers();
  const { onSearch: onSearchUser, onChange: onChangeUser } = useUsersHandler();

  const handleReset = () => {
    resetFields();
  };
  const onLoadingHandler = () => {
    setIsLoading((prev: boolean) => !prev);
  };
  // Wrap loadQueryUser with useCallback
  const loadQueryUser = useCallback(
    (params?: { page?: number; size?: number; username?: string }) => {
      loadQuery(params); // You can pass arguments here if needed
    },
    [loadQuery]
  ); // Dependencies should include anything that loadQueryUser depends on

  // Wrap loadQueryUser with useCallback
  const loadQueryAnnonce = useCallback(
    (params?: {
      page?: number;
      size?: number;
      libelle?: string;
      userId?: number;
    }) => {
      loadQueryAnc(params); // You can pass arguments here if needed
    },
    [loadQueryAnc]
  ); // Dependencies should include anything that loadQueryUser depends on

  const [isFirstLoad, setIsFirstLoad] = useState(true);

  useEffect(() => {
    if (isFirstLoad) {
      loadQueryUser();
      setIsFirstLoad(false); // Mark as loaded to prevent further calls
    }
  }, [isFirstLoad, loadQueryUser, loadQueryAnnonce, selected]);

  useEffect(() => {
    if (selected) {
      console.log("SELECTED IN USEEFFECT ", selected);

      setFieldsValue({ ...selected, photo: undefined });
      loadQueryAnnonce({
        userId: ((selected as Annonce)?.user as unknown as SelectValueType)
          ?.value as number,
      });
      setUserSelected(
        (selected as Annonce)?.user as unknown as SelectValueType
      );
    }
  }, [selected, setFieldsValue]);

  if (errorQueryUsers || errorQueryAnnonces) {
    return <>Un erreur est survenu</>;
  }

  return (
    <>
      <FormProvider
        Form={Form}
        form={form}
        onFinish={(values: unknown) => {
          handleSubmit(values, handleReset, onLoadingHandler);
        }}
        onFinishFailed={(errorInfo) => {
          console.log("ERROR INFO ", errorInfo);
        }}
        debug={import.meta.env.DEV}
        initialValues={undefined}
      >
        <SimpleInput<Objet> Form={Form} name="id" hidden />
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="nom"
            label="Nom"
            rules={[
              {
                required: true,
                message: "Le champ Nom est réquis.",
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="description"
            label="Description"
            rules={[
              {
                required: false,
                message: "Le champ Description est réquis.",
              },
            ]}
            customInput={<TextArea className="w-full h-full" />}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="categorie"
            label="Catégorie"
            rules={[
              {
                required: true,
                message: "Le champ Catégorie est réquis.",
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="etat"
            label="Etat"
            rules={[
              {
                required: true,
                message: "Le champ Etat est réquis.",
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="photo"
            label="Photo"
            rules={[
              {
                required: false,
                message: "Le champ photo est réquis.",
              },
            ]}
            customInput={
              <SimpleUploadFile
                buttonText="Télécharger image"
                selector="photo"
                // isDisabled={isDetail}
                url={(selected as Objet)?.photo as string}
                form={form as unknown as FormInstance<never>}
                accept="image/*"
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="user"
            label="Utilisateur"
            rules={[
              {
                required: false,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingUser}
                showSearch
                onSearch={(e) => onSearchUser(e as string, loadQueryUser)}
                filterOption={false}
                onChange={(e) => {
                  if (e) {
                    setUserSelected(e);
                  }
                  // if user is unselected
                  if (!e) {
                    setUserSelected(undefined);
                    setFieldValue("annonce", undefined);
                  }
                  onChangeUser({ user: e }, loadQueryUser, loadQueryAnnonce);
                }}
                labelInValue
                options={users?.map((user: User) => ({
                  label: user?.username,
                  value: user?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<Objet>
            Form={Form}
            name="annonce"
            label="Annonce"
            rules={[
              {
                required: false,
                message: "Le champ Annonce est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingAnnonce}
                showSearch
                onSearch={(e) => onSearchAnnonce(e as string, loadQueryAnnonce)}
                filterOption={false}
                onChange={(e) =>
                  onChangeAnnonce({ annonce: e }, loadQueryAnnonce)
                }
                labelInValue
                options={
                  !userSelected
                    ? (emptyArray as never[])
                    : annonces?.map((annonce: Annonce) => ({
                        label: annonce?.libelle,
                        value: annonce?.id,
                      }))
                }
              />
            }
          />
        </div>
        <div className="flex flex-col justify-center">
          <Button type="primary" htmlType="submit">
            {isLoading ? <Spinner /> : <>Sauvegarder</>}
          </Button>
        </div>
      </FormProvider>
    </>
  );
};
