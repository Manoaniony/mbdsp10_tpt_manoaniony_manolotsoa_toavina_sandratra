import { useEffect, useState } from "react";
import { DataSuccessPaginateType, DataSuccessType, Objet, PaginationType, RequestSuccessType } from "../../../types";
import { ObjetsColumns as useObjetsColumns } from "../columns"
import { useNavigate } from "react-router-dom";
import { deleteObjet, getAllObjets } from "../../../services/objets/Objets.services";
import { AxiosError } from "axios";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useNotification } from "../../../hooks/useNotification";
import { PaginateObject } from "../../../types/DataSuccessPaginate.types";
import { DefaultTable as BasicTable } from "../../../shared/components/table";
import { Pagination } from "antd";


export const ListComponent = () => {

  const [data, setData] = useState<Objet[]>();
  const columns = useObjetsColumns();
  const navigate = useNavigate();

  const [pagination, setPagination] = useState<{
    page: number | undefined;
    size: number | undefined;
    totalPages: number | undefined;
    totalElement: number | undefined;
  }>({
    page: 0,
    size: 10,
    totalPages: undefined,
    totalElement: undefined,
  });

  const { data: response,
    error,
    refetch
  } = useQuery<
    RequestSuccessType<Objet[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<Objet[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [pagination?.page || 0, pagination?.size || 10],
    queryFn: ({ queryKey }) =>
      getAllObjets({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });


  // Notification
  const { contextHolder, openNotification } = useNotification()

  // mutation
  const { mutateAsync: deleteObjetAsync } = useMutation<RequestSuccessType<Objet> | AxiosError, Error, Objet, unknown>({
    mutationFn: deleteObjet,
  })

  useEffect(() => {
    if (response) {
      setData(
        (
          (
            (response as RequestSuccessType<Objet>)
              ?.data as DataSuccessPaginateType<Objet>
          ).objects as PaginateObject<Objet>
        )?.content
      );
      setPagination((previous: PaginationType) => {
        return {
          ...previous,
          totalPages: (
            (
              (response as RequestSuccessType<Objet>)
                ?.data as DataSuccessPaginateType<Objet>
            ).objects as PaginateObject<Objet>
          )?.totalPages,
          totalElement: (
            (
              (response as RequestSuccessType<Objet>)
                ?.data as DataSuccessPaginateType<Objet>
            ).objects as PaginateObject<Objet>
          )?.totalElements,
        };
      });
    }
    return () => { };
  }, [response]);

  if (error) {
    return <>Un erreur est survenu</>;
  }

  const onEdit = (row?: unknown) => {
    navigate(`/admin/objets/edit/${(row as Objet)?.id}`);
  };

  const onDelete = (
    onLoadingDeleteHandler: () => void,
    toggleShowModal: () => void,
    row?: unknown
  ) => {
    onLoadingDeleteHandler()
    console.log("row To DELETE", row);
    deleteObjetAsync(row as Objet).then((response: RequestSuccessType<Objet>) => {
      if (response.status === 200 && (response.data as DataSuccessType<Objet>)?.ok) {
        openNotification({
          type: "success",
          title: "Suppression d'objet",
          description: "objet supprimé avec succès",
          pauseOnHover: true,
          showProgress: true
        })
        onLoadingDeleteHandler()
        toggleShowModal()
      }
      console.log("Response on Delete ", response);
      refetch()
    })
  }

  return (
    <>
      {contextHolder}
      <div className='m-4'>
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <BasicTable
            columns={columns}
            data={data || []}
            isCanEdit
            isCanDelete
            onDelete={onDelete}
            onEdit={onEdit}
          />
        </div>
        {(pagination.totalPages || 1) > 1 ? (
          <div className="mt-4 flex justify-end">
            <Pagination
              onChange={(e) => {
                setPagination((previous: PaginationType) => ({
                  ...previous,
                  page: e - 1,
                }));
              }}
              defaultCurrent={1}
              total={pagination?.totalElement}
            />
          </div>
        ) : null}
      </div>
    </>
  )
}