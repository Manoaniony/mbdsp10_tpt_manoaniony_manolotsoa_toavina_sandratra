export { CreateInterestsComponent } from "./create";
export { FormInterestsComponent } from "./forms";
export { ListInterestsComponent } from "./list";
export { UpdateInterestComponent } from "./update";
