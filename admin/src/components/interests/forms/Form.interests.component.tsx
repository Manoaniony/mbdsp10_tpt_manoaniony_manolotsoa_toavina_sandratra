import { Button, Form, FormInstance, Select } from "antd";
import { FormProvider } from "../../../provider/FormProvider";
import { FC, useCallback, useEffect, useState } from "react";
import { Annonce } from "../../../types/Annonce.types";
import { SimpleInput } from "../../../shared/ui/simpleInput";
import { useQueryUsers } from "../../../hooks/users/useUsers";
import { useUsersHandler } from "../../../hooks/users/useUsersHandler";
import { InterestType, Objet, SelectValueType, User } from "../../../types";
import { Spinner } from "../../../shared/ui/spinner";
import { useQueryAnnonces } from "../../../hooks/annonces/useAnnonces";
import { useQueryObjets } from "../../../hooks/objets/useObjets";
// import { useQueryObjets } from "../../../hooks/objets/useObjets";
// import { useObjetsHandler } from "../../../hooks/objets/useObjetsHandler";

interface IForm {
  handleSubmit: (
    e: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => void;
  selected?: unknown;
}

export const FormInterestsComponent: FC<IForm> = ({
  handleSubmit,
  selected,
}) => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [userSelected, setUserSelected] = useState<SelectValueType>();

  const { resetFields, setFieldsValue, setFieldValue } = form as FormInstance;

  const handleReset = () => {
    resetFields();
  };
  const onLoadingHandler = () => {
    setIsLoading((prev: boolean) => !prev);
  };

  // for user searching
  const {
    users,
    isLoading: isLoadingUser,
    loadQuery,
    error: errorQueryUsers,
  } = useQueryUsers();

  const {
    annonces,
    isLoading: isLoadingAnnonce,
    loadQuery: loadQueryAnnonce,
    error: errorQueryAnnonces,
  } = useQueryAnnonces();

  const {
    objets,
    isLoading: isLoadingObjet,
    loadQuery: loadQueryObj,
    error: errorQueryObjet,
  } = useQueryObjets();

  // Wrap loadQueryUser with useCallback
  const loadQueryUser = useCallback(
    (params?: { page: number; size: number; username?: string }) => {
      loadQuery(params); // You can pass arguments here if needed
    },
    [loadQuery]
  );

  // Wrap loadQueryUser with useCallback
  const loadQueryObjet = useCallback(
    (params?: { page: number; size: number }) => {
      loadQueryObj(params); // You can pass arguments here if needed
    },
    [loadQueryObj]
  );

  const { onSearch: onSearchUser, onChange: onChangeUser } = useUsersHandler();

  const [isFirstLoad, setIsFirstLoad] = useState(true);

  // for user searching
  // const {
  //   objets,
  //   isLoading: isLoadingObjet,
  //   loadQuery: loadQueryObj,
  //   error: errorQueryObjets,
  // } = useQueryObjets();
  // const { onSearch: onSearchObjet, onChange: onChangeObjet } =
  //   useObjetsHandler();

  // Wrap loadQueryObj with useCallback
  // const loadQueryObjet = useCallback(
  //   (params?: {
  //     page?: number;
  //     size?: number;
  //     nom?: string;
  //     userId?: number;
  //   }) => {
  //     loadQueryObj(params); // You can pass arguments here if needed
  //   },
  //   [loadQueryObj]
  // ); // Dependencies should include anything that loadQueryUser depends on

  useEffect(() => {
    if (isFirstLoad) {
      loadQueryUser();
      loadQueryAnnonce();
      loadQueryObjet();
      setIsFirstLoad(false); // Mark as loaded to prevent further calls
    }
  }, [isFirstLoad, loadQueryUser, userSelected]);

  useEffect(() => {
    if (selected) {
      setFieldsValue({ ...selected });
    }
  }, [selected, setFieldsValue]);

  if (errorQueryUsers || errorQueryAnnonces || errorQueryObjet) {
    return (
      <>Un erreur est survenu lors du chargement des utilisateurs et objets</>
    );
  }

  return (
    <>
      <FormProvider
        Form={Form}
        form={form}
        onFinish={(values: unknown) => {
          handleSubmit(values, handleReset, onLoadingHandler);
        }}
        onFinishFailed={(errorInfo) => {
          console.log("ERROR INFO ", errorInfo);
        }}
        debug={import.meta.env.DEV}
        initialValues={undefined}
      >
        <SimpleInput<InterestType> Form={Form} name="id" hidden />
        <div className="w-full">
          <SimpleInput<InterestType>
            Form={Form}
            name="annonce"
            label="Annonce"
            rules={[
              {
                required: true,
                message: "Le champ Annonce est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingAnnonce}
                showSearch
                onSearch={(e) => console.log(e)}
                filterOption={false}
                onChange={(e) => {
                  if (e) {
                    setUserSelected(e);
                  }
                  // if user is unselected
                  if (!e) {
                    setUserSelected(undefined);
                    setFieldValue("objet", undefined);
                  }
                  onChangeUser({ user: e }, loadQuery);
                }}
                labelInValue
                options={annonces?.map((annonce: Annonce) => ({
                  label: annonce?.libelle,
                  value: annonce?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<InterestType>
            Form={Form}
            name="user"
            label="Utilisateur"
            rules={[
              {
                required: true,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingUser}
                showSearch
                onSearch={(e) => onSearchUser(e as string, loadQuery)}
                filterOption={false}
                onChange={(e) => {
                  if (e) {
                    setUserSelected(e);
                  }
                  // if user is unselected
                  if (!e) {
                    setUserSelected(undefined);
                    setFieldValue("objet", undefined);
                  }
                  onChangeUser({ user: e }, loadQuery);
                }}
                labelInValue
                options={users?.map((user: User) => ({
                  label: user?.username,
                  value: user?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<InterestType>
            Form={Form}
            name="objets"
            label="Objet"
            rules={[
              {
                required: true,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingObjet}
                showSearch
                mode="multiple"
                onSearch={(e) => console.log(e)}
                filterOption={false}
                onChange={(e) => {
                  if (e) {
                    setUserSelected(e);
                  }
                  // if user is unselected
                  if (!e) {
                    setUserSelected(undefined);
                    setFieldValue("objet", undefined);
                  }
                  onChangeUser({ user: e }, loadQuery);
                }}
                labelInValue
                options={objets?.map((objet: Objet) => ({
                  label: objet?.nom,
                  value: objet?.id,
                }))}
              />
            }
          />
        </div>

        <div className="flex flex-col justify-center">
          <Button type="primary" htmlType="submit">
            {isLoading ? <Spinner /> : <>Sauvegarder</>}
          </Button>
        </div>
      </FormProvider>
    </>
  );
};
