import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { InterestType, Objet } from "../../../types";

export const useInterestsColumns: () => ColumnDef<InterestType>[] = () => {
  const columnHelper = createColumnHelper<InterestType>();
  const columns: ColumnDef<InterestType, never>[] = [
    columnHelper.accessor("annonce.libelle", {
      header: () => <span>Libelle</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("user.username", {
      header: () => <span>Utilisateur</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("objets", {
      header: () => <span>Liste d'objets a echanger</span>,
      cell: (props) =>
        (props.getValue() as Objet[])?.map((objet: Objet) => (
          <div>{objet?.nom}</div>
        )),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
  ];
  return columns as ColumnDef<InterestType>[];
};
