import Title from "antd/es/typography/Title";
import { useNotification } from "../../../hooks/useNotification";
import { FormInterestsComponent } from "../forms";
import { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import { interestsMock } from "../../../mocks/interests.mock";
import { InterestType, Objet } from "../../../types";

export const UpdateInterestComponent = () => {
  const { id } = useParams();
  const [selected, setSelected] = useState<unknown>();
  const data = useMemo(() => interestsMock(), [interestsMock]);
  useEffect(() => {
    if (data) {
      const dataToUpdated = data.find(
        (interest: InterestType) => interest?.id === Number(id)
      );
      setSelected({
        ...dataToUpdated,
        annonce: dataToUpdated?.annonce
          ? {
              label: dataToUpdated?.annonce?.libelle,
              value: dataToUpdated?.annonce?.id,
            }
          : undefined,
        user: dataToUpdated?.user
          ? {
              label: dataToUpdated?.user?.username,
              value: dataToUpdated?.user?.id,
            }
          : undefined,
        objets: dataToUpdated?.objets?.length
          ? dataToUpdated?.objets?.map((objet: Objet) => ({
              label: objet?.nom,
              value: objet?.id,
              key: objet?.id,
            }))
          : undefined,
      });
      // setSelected(() => {
      //   const dataToUpdated = data.find(
      //     (interest: InterestType) => interest?.id === Number(id)
      //   );
      //   console.log("dataToUpdated ", dataToUpdated);
      // return {
      //   ...dataToUpdated,
      //   annonce: dataToUpdated?.annonce
      //     ? {
      //         label: dataToUpdated?.annonce?.libelle,
      //         value: dataToUpdated?.annonce?.id,
      //       }
      //     : undefined,
      //   user: dataToUpdated?.user
      //     ? {
      //         label: dataToUpdated?.user?.username,
      //         value: dataToUpdated?.user?.id,
      //       }
      //     : undefined,
      //   objet: dataToUpdated?.objets?.length
      //     ? dataToUpdated?.objets?.map((objet: Objet) => ({
      //         label: objet?.nom,
      //         value: objet?.id,
      //         key: objet?.id,
      //       }))
      //     : undefined,
      // };
      // });
    }
  }, [data]);

  // Notification
  const { contextHolder, openNotification } = useNotification();

  const handleSubmit = async (
    finalData: unknown,
    _: undefined,
    onLoadingHandler?: () => void
  ) => {
    console.log("finalData ", finalData);
    onLoadingHandler && onLoadingHandler();
    openNotification({
      type: "success",
      title: "Modification d'interet",
      description: "Interet modifié avec succès",
      pauseOnHover: true,
      showProgress: true,
    });
    setTimeout(() => {
      onLoadingHandler && onLoadingHandler();
    }, 2000);
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Edition d'un interet</Title>
        <div>
          <FormInterestsComponent
            selected={selected}
            handleSubmit={(e, _, onLoadingHandler) => {
              handleSubmit(e, undefined, onLoadingHandler);
            }}
          />
        </div>
      </div>
    </>
  );
};
