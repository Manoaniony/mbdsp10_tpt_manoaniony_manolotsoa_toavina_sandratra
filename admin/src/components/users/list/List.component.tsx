import { UsersColumns as useUserColumns } from "../columns";
import { DefaultTable as BasicTable } from "../../../shared/components/table";
import { Pagination } from 'antd';
import { useMutation, useQuery } from "@tanstack/react-query";
import { deleteUser, getAllUsers } from "../../../services/users/Users.services";
import { useEffect, useState } from "react";
import { DataSuccessPaginateType, DataSuccessType, PaginationType, RequestSuccessType, User } from "../../../types";
import { AxiosError } from "axios";
import { PaginateObject } from "../../../types/DataSuccessPaginate.types";
import { useNavigate } from "react-router-dom";
import { useNotification } from "../../../hooks/useNotification";

export const ListComponent = () => {
  const [data, setData] = useState<User[]>();
  const columns = useUserColumns();
  const navigate = useNavigate();

  const [pagination, setPagination] = useState<{
    page: number | undefined;
    size: number | undefined;
    totalPages: number | undefined;
    totalElement: number | undefined;
  }>({
    page: 0,
    size: 10,
    totalPages: undefined,
    totalElement: undefined,
  });

  const { data: response,
    error,
    refetch
  } = useQuery<
    RequestSuccessType<User[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<User[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [pagination?.page || 0, pagination?.size || 10],
    queryFn: ({ queryKey }) =>
      getAllUsers({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });

  // Notification
  const { contextHolder, openNotification } = useNotification()

  // mutation
  const { mutateAsync: deleteUserAsync } = useMutation<RequestSuccessType<User> | AxiosError, Error, User, unknown>({
    mutationFn: deleteUser,
  })

  useEffect(() => {
    if (response) {
      setData(
        (
          (
            (response as RequestSuccessType<User>)
              ?.data as DataSuccessPaginateType<User>
          ).objects as PaginateObject<User>
        )?.content
      );
      setPagination((previous: PaginationType) => {
        return {
          ...previous,
          totalPages: (
            (
              (response as RequestSuccessType<User>)
                ?.data as DataSuccessPaginateType<User>
            ).objects as PaginateObject<User>
          )?.totalPages,
          totalElement: (
            (
              (response as RequestSuccessType<User>)
                ?.data as DataSuccessPaginateType<User>
            ).objects as PaginateObject<User>
          )?.totalElements,
        };
      });
    }
    return () => { };
  }, [response]);

  if (error) {
    return <>Un erreur est survenu</>;
  }

  const onEdit = (row?: unknown) => {
    navigate(`/admin/users/edit/${(row as User)?.id}`);
  };

  const onDelete = (
    onLoadingDeleteHandler: () => void,
    toggleShowModal: () => void,
    row?: unknown
  ) => {
    onLoadingDeleteHandler()
    console.log("row To DELETE", row);
    deleteUserAsync(row as User).then((response: RequestSuccessType<User>) => {
      if (response.status === 200 && (response.data as DataSuccessType<User>)?.ok) {
        openNotification({
          type: "success",
          title: "Suppression d'utilisateur",
          description: "utilisateur supprimé avec succès",
          pauseOnHover: true,
          showProgress: true
        })
        onLoadingDeleteHandler()
        toggleShowModal()
      }
      console.log("Response on Delete ", response);
      refetch()
    })
  }

  return (
    <>
      {contextHolder}
      <div className='m-4'>
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <BasicTable
            columns={columns}
            data={data || []}
            isCanEdit
            isCanDelete
            onDelete={onDelete}
            onEdit={onEdit}
          />
        </div>
        {(pagination.totalPages || 1) > 1 ? (
          <div className="mt-4 flex justify-end">
            <Pagination
              onChange={(e) => {
                setPagination((previous: PaginationType) => ({
                  ...previous,
                  page: e - 1,
                }));
              }}
              defaultCurrent={1}
              total={pagination?.totalElement}
            />
          </div>
        ) : null}
      </div>
    </>
  )
}
