import { createColumnHelper } from "@tanstack/react-table"
import { User } from "../../../types"

export const UsersColumns = () => {
  const columnHelper = createColumnHelper<User>()

  const columns = [
    columnHelper.accessor('id', {
      cell: info => info.getValue(),
      footer: info => info.column.id,
    }),
    columnHelper.accessor('email', {
      cell: info => info.getValue(),
      footer: info => info.column.id,
    }),
    columnHelper.accessor('username', {
      cell: info => info.getValue(),
      footer: info => info.column.id,
    }),
    columnHelper.accessor('nom', {
      cell: info => info.getValue(),
      footer: info => info.column.id,
    }),
    columnHelper.accessor('tel', {
      cell: info => info.getValue(),
      footer: info => info.column.id,
    }),
  ]

  return columns
}