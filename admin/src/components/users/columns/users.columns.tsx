import { ColumnDef, createColumnHelper } from "@tanstack/react-table"
import { User } from "../../../types"

export const UsersColumns: () => ColumnDef<User>[] = () => {
  const columnHelper = createColumnHelper<User>()
  const columns: ColumnDef<User, never>[] = [
    columnHelper.accessor("email", {
      header: () => <span>Email</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("username", {
      header: () => <span>Nom d'utilisateur</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("nom", {
      header: () => <span>Nom</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    })
  ]
  return columns as ColumnDef<User>[]
}