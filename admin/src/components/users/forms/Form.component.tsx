import { Button, Form, FormInstance, Input } from "antd";
import { FormProvider } from "../../../provider/FormProvider";
import { ChangeEvent, FC, useContext, useEffect, useMemo, useState } from "react";
import { SimpleInput } from "../../../shared/ui/simpleInput";
import { Spinner } from "../../../shared/ui/spinner";
import { ErrorContext } from "../../../context/errors/ErrorContext";
import { useLocation } from "react-router-dom";
import { User } from "../../../types";

interface IFormUsers {
  handleSubmit: (e: unknown, handleReset?: () => void, onLoadingHandler?: () => void) => void,
  selected?: unknown
}

export const FormComponent: FC<IFormUsers> = ({ handleSubmit, selected }) => {
  const [form] = Form.useForm();
  const { error } = useContext(ErrorContext);
  const { Password } = Input
  const [newPassword, setNewPassword] = useState<string>();
  const [, setConfirmPassword] = useState<string>();
  const { pathname } = useLocation();

  const isUserEdit = useMemo(() => pathname.includes("/admin/users/edit"), [pathname])

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { resetFields, setFieldsValue, setFields } = form as FormInstance;

  const handleReset = () => {
    resetFields();
  };
  const onLoadingHandler = () => {
    setIsLoading((prev: boolean) => (!prev))
  }

  useEffect(() => {
    if (selected) {
      setFieldsValue(selected)
    }
  }, [selected, setFieldsValue])

  useEffect(() => {
    if (error) {
      setFields([
        {
          name: 'username',
          errors: [error],
        },
        {
          name: 'email',
          errors: [error],
        },
      ]);
    }
    return () => {

    };
  }, [error, setFields]);

  return (
    <>
      <FormProvider
        Form={Form}
        form={form}
        onFinish={(values: unknown) => {
          handleSubmit(values, handleReset, onLoadingHandler)
        }}
        onFinishFailed={(errorInfo) => {
          console.log("ERROR INFO ", errorInfo);
        }}
        debug={import.meta.env.DEV}
        initialValues={undefined}>
        <SimpleInput<User>
          Form={Form}
          name="id"
          hidden
        />
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="email"
            label="Email"
            rules={[
              {
                required: true, message: 'Le champ Email est réquis.'
              },
              {
                type: 'email',
                message: 'Veuillez entrer un adresse Email valide',
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="nom"
            label="Nom"
            rules={[
              {
                required: true, message: 'Le champ Nom est réquis.'
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="username"
            label="Nom d'utilisateur"
            rules={[
              {
                required: true, message: 'Le champ Nom d\'utilisateur est réquis.'
              }
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="adresse"
            label="Adresse"
          />
        </div>
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="tel"
            label="Téléphone"
          />
        </div>
        <div className="w-full">
          <SimpleInput
            Form={Form}
            name="password"
            label="Mot de passe"
            rules={[
              {
                required: true, message: 'Le champ Mot de passe est réquis.'
              },
            ]}
            customInput={
              <Password
                onChange={(e: ChangeEvent<HTMLInputElement>) => setNewPassword(e?.target?.value)}
              />
            }
          />
        </div>
        {
          isUserEdit ?
            null :
            <>
              <div className="w-full">
                <SimpleInput
                  Form={Form}
                  name="confirmPassword"
                  label="Confirmation Mot de passe"
                  // dependencies={["newPassword"]}
                  rules={[
                    {
                      required: true, message: 'Le champ Confirmation Mot de passe est réquis.'
                    },
                    {
                      validator: (_rule, value) => {
                        return new Promise<void>((resolve, reject) => {
                          if (value && newPassword === value) {
                            resolve();
                            return;
                          }
                          reject("Error")
                        })
                      },
                      message: "Les 2 champs doivent etre identiques"
                    }
                  ]}
                  customInput={
                    <Password
                      onChange={(e: ChangeEvent<HTMLInputElement>) => setConfirmPassword(e?.target?.value)}
                      placeholder="Confirmer votre mot de passe" />
                  }
                />
              </div>
            </>
        }
        <div className="flex flex-col justify-center">
          <Button
            type="primary"
            htmlType="submit"
          >
            {isLoading ? <Spinner /> : <>Sauvegarder</>}
          </Button>
        </div>
      </FormProvider>
    </>
  )
}