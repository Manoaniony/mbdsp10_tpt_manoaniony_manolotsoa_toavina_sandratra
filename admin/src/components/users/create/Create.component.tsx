import { FormComponent as FormUser } from "../forms";
import { useNotification } from "../../../hooks/useNotification";
import Title from "antd/es/typography/Title";
import { registerUser } from "../../../services/users/Users.services";
import { useMutation } from "@tanstack/react-query";
import { DataSuccessType, RequestSuccessType, User } from "../../../types";
import { AxiosError } from "axios";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { ErrorContext } from "../../../context/errors/ErrorContext";

export const CreateComponent = () => {
  // useNavigate
  const navigate = useNavigate();
  const handleSuccessNavigation: () => void = () => {
    navigate("/admin/users");
  };
  // Notification
  const { contextHolder, openNotification } = useNotification();

  // mutation
  const { mutateAsync: saveUser } = useMutation<
    RequestSuccessType<User> | AxiosError,
    Error,
    User,
    unknown
  >({
    mutationFn: registerUser,
  });

  const [error, setError] = useState<string | undefined>();

  const handleSubmit = async (
    finalData: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => {
    setError(undefined);
    saveUser(finalData as User)
      .then((response: RequestSuccessType<User>) => {
        // loading
        onLoadingHandler && onLoadingHandler();
        if (
          response.status === 200 &&
          (response.data as DataSuccessType<User>)?.ok
        ) {
          openNotification({
            type: "success",
            title: "Création d'utilisateur",
            description: "Utilisateur créé avec succès",
            pauseOnHover: true,
            showProgress: true,
          });
          setTimeout(() => {
            onLoadingHandler && onLoadingHandler();
            handleSuccessNavigation();
            handleReset && handleReset();
          }, 2000);
        } else {
          openNotification({
            type: "error",
            title: "Création d'utilisateur",
            description: "Utilisateur non créé",
            pauseOnHover: true,
            showProgress: true,
          });
          setError((response.data as DataSuccessType<User>)?.message);
          onLoadingHandler && onLoadingHandler();
        }
      })
      .catch((error: unknown) => {
        console.log("if they are error ", error);
        openNotification({
          type: "error",
          title: "Erreur",
          description: JSON.stringify(error),
          pauseOnHover: true,
          showProgress: true,
        });
      });
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Création d'utilisateur</Title>
        <div>
          <ErrorContext.Provider value={{ error, setError }}>
            <FormUser
              handleSubmit={(e, handleReset, onLoadingHandler) => {
                handleSubmit(e, handleReset, onLoadingHandler);
              }}
            />
          </ErrorContext.Provider>
        </div>
      </div>
    </>
  );
};
