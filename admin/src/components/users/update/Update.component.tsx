import { useMutation, useQuery } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { DataSuccessType, RequestSuccessType, User } from "../../../types";
import { AxiosError } from "axios";
import { getUser, updateUser } from "../../../services/users/Users.services";
import { useNotification } from "../../../hooks/useNotification";
import Title from "antd/es/typography/Title";
import { ErrorContext } from "../../../context/errors/ErrorContext";
import { FormComponent as FormUser } from "../forms";

export const UpdateComponent = () => {
  const { id } = useParams();
  const [selected, setSelected] = useState<unknown>();

  const { data: response, error: errorRetrieveQuery } = useQuery<
    RequestSuccessType<User> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<User> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [Number(id)],
    queryFn: ({ queryKey }) =>
      getUser({ id: queryKey[0] as unknown as number }),
    enabled: true,
  });

  // mutation
  const { mutateAsync: updateUserAsync } = useMutation<
    RequestSuccessType<User> | AxiosError,
    Error,
    User,
    unknown
  >({
    mutationFn: updateUser,
  });

  // Notification
  const { contextHolder, openNotification } = useNotification();

  const [error, setError] = useState<string | undefined>();

  useEffect(() => {
    if (
      ((response as RequestSuccessType<User>)?.data as DataSuccessType<User>)
        ?.objects as User
    ) {
      setSelected({
        ...((
          (response as RequestSuccessType<User>)?.data as DataSuccessType<User>
        )?.objects as User),
        password: undefined,
      });
    }
    return () => {};
  }, [response]);

  if (error || errorRetrieveQuery) {
    return <>Un erreur est survenu</>;
  }

  const handleSubmit = async (
    finalData: unknown,
    _: undefined,
    onLoadingHandler?: () => void
  ) => {
    setError(undefined);
    updateUserAsync(finalData as User)
      .then((response: RequestSuccessType<User>) => {
        console.log("FinalData ", finalData as User);

        // loading
        onLoadingHandler && onLoadingHandler();
        if (
          response.status === 200 &&
          (response.data as DataSuccessType<User>)?.ok
        ) {
          openNotification({
            type: "success",
            title: "Modification d'utilisateur",
            description: "Utilisateur modifié avec succès",
            pauseOnHover: true,
            showProgress: true,
          });
          setTimeout(() => {
            onLoadingHandler && onLoadingHandler();
          }, 2000);
        } else {
          openNotification({
            type: "error",
            title: "Modification d'utilisateur",
            description: "Utilisateur non modifié",
            pauseOnHover: true,
            showProgress: true,
          });
          setError((response?.data as DataSuccessType<User>)?.message);
          onLoadingHandler && onLoadingHandler();
        }
      })
      .catch((error: unknown) => {
        console.log("if they are error ", error);
        openNotification({
          type: "error",
          title: "Erreur",
          description: JSON.stringify(error),
          pauseOnHover: true,
          showProgress: true,
        });
      });
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Edition d'un utilisateur</Title>
        <div>
          <ErrorContext.Provider value={{ error, setError }}>
            <FormUser
              selected={selected}
              handleSubmit={(e, _, onLoadingHandler) => {
                handleSubmit(e, undefined, onLoadingHandler);
              }}
            />
          </ErrorContext.Provider>
        </div>
      </div>
    </>
  );
};
