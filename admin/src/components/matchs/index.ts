export { CreateMatchsComponent } from "./create";
export { FormMatchsComponent } from "./forms";
export { ListMatchsComponent } from "./list";
export { UpdateMatchComponent } from "./update";
