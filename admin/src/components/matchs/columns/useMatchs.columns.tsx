import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { Objet } from "../../../types";
import { MatchType } from "../../../types/Match.types";
import dayjs from "dayjs";

export const useMatchsColumns: () => ColumnDef<MatchType>[] = () => {
  const columnHelper = createColumnHelper<MatchType>();
  const columns: ColumnDef<MatchType, never>[] = [
    columnHelper.accessor("dateMatch", {
      header: () => <span>Date Match</span>,
      cell: (props) =>
        props.getValue() ? dayjs(props.getValue()).format("YYYY-MM-DD") : null,
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("user1.username", {
      header: () => <span>Utilisateur 1</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("user2.username", {
      header: () => <span>Utilisateur 2</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("objet1", {
      header: () => <span>Objet 1</span>,
      cell: (props) =>
        (props.getValue() as Objet[])?.map((objet: Objet) => (
          <div>{objet?.nom}</div>
        )),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("objet2", {
      header: () => <span>Objet 2</span>,
      cell: (props) =>
        (props.getValue() as Objet[])?.map((objet: Objet) => (
          <div>{objet?.nom}</div>
        )),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
  ];
  return columns as ColumnDef<MatchType>[];
};
