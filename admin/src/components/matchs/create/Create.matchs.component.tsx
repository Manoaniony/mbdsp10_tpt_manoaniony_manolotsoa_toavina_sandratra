import { useNavigate } from "react-router-dom";
import { useNotification } from "../../../hooks/useNotification";
import Title from "antd/es/typography/Title";
import { FormMatchsComponent } from "../forms";
// import { Annonce, CreateAnnonceType } from "../../../types/Annonce.types";
// import {
//   DataSuccessType,
//   RequestSuccessType,
//   SelectValueType,
// } from "../../../types";
// import { saveAnnonce } from "../../../services/annonces/Annonces.services";

export const CreateMatchsComponent = () => {
  const navigate = useNavigate();

  const handleSuccessNavigation: () => void = () => {
    navigate("/admin/matchs");
  };
  // Notification
  const { contextHolder, openNotification } = useNotification();

  const handleSubmit = (
    finalData: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => {
    console.log(finalData);
    openNotification({
      type: "success",
      title: "Création de match",
      description: "Match créé avec succès",
      pauseOnHover: true,
      showProgress: true,
    });
    onLoadingHandler && onLoadingHandler();

    setTimeout(() => {
      handleSuccessNavigation();
      handleReset && handleReset();
    }, 2000);
    // let dataToCreate: CreateAnnonceType = {
    //   ...(finalData as Annonce),
    //   user: (finalData as Annonce)?.user
    //     ? {
    //         id: ((finalData as Annonce)?.user as SelectValueType)
    //           ?.value as number,
    //       }
    //     : undefined,
    //   objets: ((finalData as Annonce)?.objets as SelectValueType[])?.length
    //     ? (finalData as Annonce)?.objets?.map((objet: SelectValueType) => ({
    //         id: objet?.value as number,
    //       }))
    //     : undefined,
    // };

    // saveAnnonce(dataToCreate)
    //   .then((response: RequestSuccessType<Annonce>) => {
    //     if (
    //       response.status === 200 &&
    //       (response.data as DataSuccessType<Annonce>)?.ok
    //     ) {
    // openNotification({
    //   type: "success",
    //   title: "Création d'interet",
    //   description: "Interet créé avec succès",
    //   pauseOnHover: true,
    //   showProgress: true,
    // });
    // setTimeout(() => {
    //   onLoadingHandler && onLoadingHandler();
    //   handleSuccessNavigation();
    //   handleReset && handleReset();
    // }, 2000);
    //     } else {
    //       openNotification({
    //         type: "error",
    //         title: "Création d'interet",
    //         description: "Interet non créé",
    //         pauseOnHover: true,
    //         showProgress: true,
    //       });
    //     }
    //   })
    //   .catch((error: unknown) => {
    //     console.log("if they are error ", error);
    //     openNotification({
    //       type: "error",
    //       title: "Erreur",
    //       description: JSON.stringify(error),
    //       pauseOnHover: true,
    //       showProgress: true,
    //     });
    //   });
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Création d'un match</Title>
        <div>
          <FormMatchsComponent
            handleSubmit={(e, handleReset, onLoadingHandler) => {
              handleSubmit(e, handleReset, onLoadingHandler);
            }}
          />
        </div>
      </div>
    </>
  );
};
