import {
  // useEffect,
  useState,
} from "react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { AxiosError } from "axios";
import {
  // DataSuccessPaginateType,
  DataSuccessType,
  InterestType,
  PaginationType,
  RequestSuccessType,
} from "../../../types";
import { useNotification } from "../../../hooks/useNotification";
import { DefaultTable as BasicTable } from "../../../shared/components/table";
import { Pagination } from "antd";
import { useMatchsColumns } from "../columns";
import { useNavigate } from "react-router-dom";
import {
  deleteInterest,
  getAllInterests,
} from "../../../services/interests/Interests.services";
// import { PaginateObject } from "../../../types/DataSuccessPaginate.types";
import { matchsMock } from "../../../mocks/matchs.mock";
import { MatchType } from "../../../types/Match.types";

export const ListMatchsComponent = () => {
  // const [data, setData] = useState<InterestType[]>();
  const columns = useMatchsColumns();
  const navigate = useNavigate();
  const matchs = matchsMock();

  // Notification
  const { contextHolder, openNotification } = useNotification();

  const [pagination, setPagination] = useState<{
    page: number | undefined;
    size: number | undefined;
    totalPages: number | undefined;
    totalElement: number | undefined;
  }>({
    page: 0,
    size: 10,
    totalPages: undefined,
    totalElement: undefined,
  });

  const {
    // data: response,
    error,
    refetch,
  } = useQuery<
    RequestSuccessType<InterestType[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<InterestType[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [pagination?.page || 0, pagination?.size || 10],
    queryFn: ({ queryKey }) =>
      getAllInterests({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });

  // mutation
  const { mutateAsync: deleteInterestAsync } = useMutation<
    RequestSuccessType<InterestType> | AxiosError,
    Error,
    InterestType,
    unknown
  >({
    mutationFn: deleteInterest,
  });

  // useEffect(() => {
  //   if (response) {
  //     console.log("response ", response);

  //     setData(
  //       (
  //         (
  //           (response as RequestSuccessType<InterestType>)
  //             ?.data as DataSuccessPaginateType<InterestType>
  //         ).objects as PaginateObject<InterestType>
  //       )?.content
  //     );
  //     setPagination((previous: PaginationType) => {
  //       return {
  //         ...previous,
  //         totalPages: (
  //           (
  //             (response as RequestSuccessType<InterestType>)
  //               ?.data as DataSuccessPaginateType<InterestType>
  //           ).objects as PaginateObject<InterestType>
  //         )?.totalPages,
  //         totalElement: (
  //           (
  //             (response as RequestSuccessType<InterestType>)
  //               ?.data as DataSuccessPaginateType<InterestType>
  //           ).objects as PaginateObject<InterestType>
  //         )?.totalElements,
  //       };
  //     });
  //   }
  //   return () => {};
  // }, [response]);

  if (error) {
    return <>Un erreur est survenu</>;
  }

  const onEdit = (row?: unknown) => {
    navigate(`/admin/matchs/edit/${(row as MatchType)?.id}`);
  };

  const onDelete = (
    onLoadingDeleteHandler: () => void,
    toggleShowModal: () => void,
    row?: unknown
  ) => {
    onLoadingDeleteHandler();
    console.log("row To DELETE", row);
    deleteInterestAsync(row as InterestType).then(
      (response: RequestSuccessType<InterestType>) => {
        if (
          response.status === 200 &&
          (response.data as DataSuccessType<InterestType>)?.ok
        ) {
          openNotification({
            type: "success",
            title: "Suppression d'objet",
            description: "objet supprimé avec succès",
            pauseOnHover: true,
            showProgress: true,
          });
          onLoadingDeleteHandler();
          toggleShowModal();
        }
        console.log("Response on Delete ", response);
        refetch();
      }
    );
  };

  return (
    <>
      {contextHolder}
      <div className="m-4">
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <BasicTable
            columns={columns}
            data={matchs || []}
            isCanEdit
            isCanDelete
            onDelete={onDelete}
            onEdit={onEdit}
          />
        </div>
        {(pagination.totalPages || 1) > 1 ? (
          <div className="mt-4 flex justify-end">
            <Pagination
              onChange={(e) => {
                setPagination((previous: PaginationType) => ({
                  ...previous,
                  page: e - 1,
                }));
              }}
              defaultCurrent={1}
              total={pagination?.totalElement}
            />
          </div>
        ) : null}
      </div>
    </>
  );
};
