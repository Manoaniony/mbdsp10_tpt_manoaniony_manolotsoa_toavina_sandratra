import { Button, Form, FormInstance, Select } from "antd";
import { FormProvider } from "../../../provider/FormProvider";
import { FC, useCallback, useEffect, useState } from "react";
import { SimpleInput } from "../../../shared/ui/simpleInput";
import { useQueryUsers } from "../../../hooks/users/useUsers";
import { useUsersHandler } from "../../../hooks/users/useUsersHandler";
import { Objet, SelectValueType, User } from "../../../types";
import { Spinner } from "../../../shared/ui/spinner";
import { useQueryObjets } from "../../../hooks/objets/useObjets";
import { MatchType } from "../../../types/Match.types";
// import { useQueryObjets } from "../../../hooks/objets/useObjets";
// import { useObjetsHandler } from "../../../hooks/objets/useObjetsHandler";

interface IForm {
  handleSubmit: (
    e: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => void;
  selected?: unknown;
}

export const FormMatchsComponent: FC<IForm> = ({ handleSubmit, selected }) => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [userSelected] = useState<SelectValueType>();

  const { resetFields, setFieldsValue } = form as FormInstance;

  const handleReset = () => {
    resetFields();
  };
  const onLoadingHandler = () => {
    setIsLoading((prev: boolean) => !prev);
  };

  // for user searching
  const {
    users: users1,
    isLoading: isLoadingUser1,
    loadQuery: loadQueryUserFirst,
    error: errorQueryUsers1,
  } = useQueryUsers();

  // for user searching
  const {
    users: users2,
    isLoading: isLoadingUser2,
    loadQuery: loadQueryUserSecond,
    error: errorQueryUsers2,
  } = useQueryUsers();

  const {
    objets: objets1,
    isLoading: isLoadingObjet1,
    loadQuery: loadQueryObjFirst,
    error: errorQueryObjet1,
  } = useQueryObjets();

  const {
    objets: objets2,
    isLoading: isLoadingObjet2,
    loadQuery: loadQueryObjSecond,
    error: errorQueryObjet2,
  } = useQueryObjets();

  // Wrap loadQueryUser with useCallback
  const loadQueryUser1 = useCallback(
    (params?: { page?: number; size?: number; username?: string }) => {
      loadQueryUserFirst(params); // You can pass arguments here if needed
    },
    [loadQueryUserFirst]
  );

  const loadQueryUser2 = useCallback(
    (params?: { page?: number; size?: number; username?: string }) => {
      loadQueryUserSecond(params); // You can pass arguments here if needed
    },
    [loadQueryUserFirst]
  );

  // Wrap loadQueryUser with useCallback
  const loadQueryObjet1 = useCallback(
    (params?: { page: number; size: number }) => {
      loadQueryObjFirst(params); // You can pass arguments here if needed
    },
    [loadQueryObjFirst]
  );

  // Wrap loadQueryUser with useCallback
  const loadQueryObjet2 = useCallback(
    (params?: { page: number; size: number }) => {
      loadQueryObjSecond(params); // You can pass arguments here if needed
    },
    [loadQueryObjSecond]
  );

  const { onSearch: onSearchUser } = useUsersHandler();

  const [isFirstLoad, setIsFirstLoad] = useState(true);

  useEffect(() => {
    if (isFirstLoad) {
      loadQueryObjet1();
      loadQueryObjet2();
      loadQueryUser1();
      loadQueryUser2();
      setIsFirstLoad(false); // Mark as loaded to prevent further calls
    }
  }, [
    isFirstLoad,
    loadQueryUser1,
    loadQueryUser2,
    loadQueryObjet1,
    loadQueryObjet2,
    userSelected,
  ]);

  useEffect(() => {
    if (selected) {
      setFieldsValue({ ...selected });
    }
  }, [selected, setFieldsValue]);

  if (
    errorQueryUsers1 ||
    errorQueryUsers2 ||
    errorQueryObjet1 ||
    errorQueryObjet2
  ) {
    return (
      <>Un erreur est survenu lors du chargement des utilisateurs et objets</>
    );
  }

  return (
    <>
      <FormProvider
        Form={Form}
        form={form}
        onFinish={(values: unknown) => {
          handleSubmit(values, handleReset, onLoadingHandler);
        }}
        onFinishFailed={(errorInfo) => {
          console.log("ERROR INFO ", errorInfo);
        }}
        debug={import.meta.env.DEV}
        initialValues={undefined}
      >
        <SimpleInput<MatchType> Form={Form} name="id" hidden />
        <div className="w-full">
          <SimpleInput<MatchType>
            Form={Form}
            name="user1"
            label="Utilisateur 1"
            rules={[
              {
                required: true,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingUser1}
                showSearch
                onSearch={(e) => onSearchUser(e as string, loadQueryUser1)}
                filterOption={false}
                onChange={() => {
                  // if (e) {
                  //   setUserSelected(e);
                  // }
                  // // if user is unselected
                  // if (!e) {
                  //   setUserSelected(undefined);
                  //   setFieldValue("objet", undefined);
                  // }
                  // onChangeUser({ user: e }, loadQueryUser1);
                }}
                labelInValue
                options={users1?.map((user: User) => ({
                  label: user?.username,
                  value: user?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<MatchType>
            Form={Form}
            name="user2"
            label="Utilisateur 2"
            rules={[
              {
                required: true,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingUser2}
                showSearch
                onSearch={(e) => onSearchUser(e as string, loadQueryUser2)}
                filterOption={false}
                onChange={() => {
                  // if (e) {
                  //   setUserSelected(e);
                  // }
                  // // if user is unselected
                  // if (!e) {
                  //   setUserSelected(undefined);
                  //   setFieldValue("objet", undefined);
                  // }
                  // onChangeUser({ user: e }, loadQueryUser2);
                }}
                labelInValue
                options={users2?.map((user: User) => ({
                  label: user?.username,
                  value: user?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<MatchType>
            Form={Form}
            name="objet1"
            label="Objet 1"
            rules={[
              {
                required: true,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingObjet1}
                showSearch
                mode="multiple"
                onSearch={(e) => console.log(e)}
                filterOption={false}
                onChange={() => {
                  // if (e) {
                  //   setUserSelected(e);
                  // }
                  // // if user is unselected
                  // if (!e) {
                  //   setUserSelected(undefined);
                  //   setFieldValue("objet", undefined);
                  // }
                  // onChangeUser({ user: e }, loadQueryUser1);
                }}
                labelInValue
                options={objets1?.map((objet: Objet) => ({
                  label: objet?.nom,
                  value: objet?.id,
                }))}
              />
            }
          />
        </div>
        <div className="w-full">
          <SimpleInput<MatchType>
            Form={Form}
            name="objet2"
            label="Objet 2"
            rules={[
              {
                required: true,
                message: "Le champ objet 2 est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingObjet2}
                showSearch
                mode="multiple"
                onSearch={(e) => console.log(e)}
                filterOption={false}
                onChange={() => {
                  // if (e) {
                  //   setUserSelected(e);
                  // }
                  // if user is unselected
                  // if (!e) {
                  //   setUserSelected(undefined);
                  //   setFieldValue("objet", undefined);
                  // }
                  // onChangeUser({ user: e }, loadQueryUser1);
                }}
                labelInValue
                options={objets2?.map((objet: Objet) => ({
                  label: objet?.nom,
                  value: objet?.id,
                }))}
              />
            }
          />
        </div>

        <div className="flex flex-col justify-center">
          <Button type="primary" htmlType="submit">
            {isLoading ? <Spinner /> : <>Sauvegarder</>}
          </Button>
        </div>
      </FormProvider>
    </>
  );
};
