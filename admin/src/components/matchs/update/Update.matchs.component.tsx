import Title from "antd/es/typography/Title";
import { useNotification } from "../../../hooks/useNotification";
import { FormMatchsComponent } from "../forms";
import { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import { matchsMock } from "../../../mocks/matchs.mock";
import { Objet, User } from "../../../types";
import { MatchType } from "../../../types/Match.types";

export const UpdateMatchComponent = () => {
  const { id } = useParams();
  const [selected, setSelected] = useState<unknown>();
  const data = useMemo(() => matchsMock(), [matchsMock]);
  useEffect(() => {
    if (data) {
      const dataToUpdated = data.find(
        (match: MatchType) => match?.id === Number(id)
      );
      setSelected({
        ...dataToUpdated,
        user1: dataToUpdated?.user1
          ? {
              label: (dataToUpdated?.user1 as User)?.username,
              value: (dataToUpdated?.user1 as User)?.id,
            }
          : undefined,
        user2: dataToUpdated?.user2
          ? {
              label: (dataToUpdated?.user2 as User)?.username,
              value: (dataToUpdated?.user2 as User)?.id,
            }
          : undefined,
        objet1: dataToUpdated?.objet1?.length
          ? dataToUpdated?.objet1?.map((objet: Objet) => ({
              label: objet?.nom,
              value: objet?.id,
              key: objet?.id,
            }))
          : undefined,
        objet2: dataToUpdated?.objet2?.length
          ? dataToUpdated?.objet2?.map((objet: Objet) => ({
              label: objet?.nom,
              value: objet?.id,
              key: objet?.id,
            }))
          : undefined,
      });
      // setSelected(() => {
      //   const dataToUpdated = data.find(
      //     (interest: InterestType) => interest?.id === Number(id)
      //   );
      //   console.log("dataToUpdated ", dataToUpdated);
      // return {
      //   ...dataToUpdated,
      //   annonce: dataToUpdated?.annonce
      //     ? {
      //         label: dataToUpdated?.annonce?.libelle,
      //         value: dataToUpdated?.annonce?.id,
      //       }
      //     : undefined,
      //   user: dataToUpdated?.user
      //     ? {
      //         label: dataToUpdated?.user?.username,
      //         value: dataToUpdated?.user?.id,
      //       }
      //     : undefined,
      //   objet: dataToUpdated?.objets?.length
      //     ? dataToUpdated?.objets?.map((objet: Objet) => ({
      //         label: objet?.nom,
      //         value: objet?.id,
      //         key: objet?.id,
      //       }))
      //     : undefined,
      // };
      // });
    }
  }, [data]);

  // Notification
  const { contextHolder, openNotification } = useNotification();

  const handleSubmit = async (
    finalData: unknown,
    _: undefined,
    onLoadingHandler?: () => void
  ) => {
    console.log("finalData ", finalData);
    onLoadingHandler && onLoadingHandler();
    openNotification({
      type: "success",
      title: "Modification d'un match",
      description: "Match modifié avec succès",
      pauseOnHover: true,
      showProgress: true,
    });
    setTimeout(() => {
      onLoadingHandler && onLoadingHandler();
    }, 2000);
  };

  return (
    <>
      {contextHolder}
      <div className="flex flex-col p-4">
        <Title className="text-left">Edition d'un match</Title>
        <div>
          <FormMatchsComponent
            selected={selected}
            handleSubmit={(e, _, onLoadingHandler) => {
              handleSubmit(e, undefined, onLoadingHandler);
            }}
          />
        </div>
      </div>
    </>
  );
};
