export { MainLayout } from "./layouts"
export {
  CreateComponent,
  FormComponent,
  ListComponent,
  UpdateComponent
} from "./users"
export {
  CreateComponent as CreateObjetComponent,
  FormComponent as FormObjetComponent,
  ListComponent as ListObjetComponent,
  UpdateComponent as UpdateObjetComponent
} from "./objets"