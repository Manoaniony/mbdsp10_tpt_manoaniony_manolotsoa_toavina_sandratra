import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { Annonce } from "../../../types/Annonce.types";
import dayjs from "dayjs";
import { User } from "../../../types";

export const useAnnoncesColumns: () => ColumnDef<Annonce>[] = () => {
  const columnHelper = createColumnHelper<Annonce>();
  const columns: ColumnDef<Annonce, never>[] = [
    columnHelper.accessor("descs", {
      header: () => <span>Description</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("etat", {
      header: () => <span>Etat</span>,
      cell: (props) => props.getValue(),
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("datePublication", {
      header: () => <span>Date de publication</span>,
      cell: (props) =>
        props.getValue() ? dayjs(props.getValue()).format("YYYY-MM-DD") : null,
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
    columnHelper.accessor("user", {
      header: () => <span>Utilisateur</span>,
      cell: (props) => (props.getValue() as User)?.username,
      enableColumnFilter: false,
      enableSorting: true,
      enableHiding: true,
    }),
  ];
  return columns as ColumnDef<Annonce>[];
};
