import { useEffect, useState } from "react";
import { useAnnoncesColumns } from "../columns";
import { Annonce } from "../../../types/Annonce.types";
import { useNavigate } from "react-router-dom";
import { useMutation, useQuery } from "@tanstack/react-query";
import { AxiosError } from "axios";
import {
  DataSuccessPaginateType,
  DataSuccessType,
  PaginationType,
  RequestSuccessType,
} from "../../../types";
import {
  deleteAnnonce,
  getAllAnnonces,
} from "../../../services/annonces/Annonces.services";
import { PaginateObject } from "../../../types/DataSuccessPaginate.types";
import { useNotification } from "../../../hooks/useNotification";
import { DefaultTable as BasicTable } from "../../../shared/components/table";
import { Pagination } from "antd";

export const ListAnnoncesComponent = () => {
  const [data, setData] = useState<Annonce[]>();
  const columns = useAnnoncesColumns();
  const navigate = useNavigate();

  // Notification
  const { contextHolder, openNotification } = useNotification();

  const [pagination, setPagination] = useState<{
    page: number | undefined;
    size: number | undefined;
    totalPages: number | undefined;
    totalElement: number | undefined;
  }>({
    page: 0,
    size: 10,
    totalPages: undefined,
    totalElement: undefined,
  });

  const {
    data: response,
    error,
    refetch,
  } = useQuery<
    RequestSuccessType<Annonce[]> | AxiosError<unknown, never>,
    Error,
    RequestSuccessType<Annonce[]> | AxiosError<unknown, never>,
    number[]
  >({
    queryKey: [pagination?.page || 0, pagination?.size || 10],
    queryFn: ({ queryKey }) =>
      getAllAnnonces({
        page: queryKey[0] as unknown as number,
        size: queryKey[1] as unknown as number,
      }),
    enabled: true,
  });

  // mutation
  const { mutateAsync: deleteAnnonceAsync } = useMutation<
    RequestSuccessType<Annonce> | AxiosError,
    Error,
    Annonce,
    unknown
  >({
    mutationFn: deleteAnnonce,
  });

  useEffect(() => {
    if (response) {
      setData(
        (
          (
            (response as RequestSuccessType<Annonce>)
              ?.data as DataSuccessPaginateType<Annonce>
          ).objects as PaginateObject<Annonce>
        )?.content
      );
      setPagination((previous: PaginationType) => {
        return {
          ...previous,
          totalPages: (
            (
              (response as RequestSuccessType<Annonce>)
                ?.data as DataSuccessPaginateType<Annonce>
            ).objects as PaginateObject<Annonce>
          )?.totalPages,
          totalElement: (
            (
              (response as RequestSuccessType<Annonce>)
                ?.data as DataSuccessPaginateType<Annonce>
            ).objects as PaginateObject<Annonce>
          )?.totalElements,
        };
      });
    }
    return () => {};
  }, [response]);

  if (error) {
    return <>Un erreur est survenu</>;
  }

  const onEdit = (row?: unknown) => {
    navigate(`/admin/annonces/edit/${(row as Annonce)?.id}`);
  };

  const onDelete = (
    onLoadingDeleteHandler: () => void,
    toggleShowModal: () => void,
    row?: unknown
  ) => {
    onLoadingDeleteHandler();
    console.log("row To DELETE", row);
    deleteAnnonceAsync(row as Annonce).then(
      (response: RequestSuccessType<Annonce>) => {
        if (
          response.status === 200 &&
          (response.data as DataSuccessType<Annonce>)?.ok
        ) {
          openNotification({
            type: "success",
            title: "Suppression d'objet",
            description: "objet supprimé avec succès",
            pauseOnHover: true,
            showProgress: true,
          });
          onLoadingDeleteHandler();
          toggleShowModal();
        }
        console.log("Response on Delete ", response);
        refetch();
      }
    );
  };

  return (
    <>
      {contextHolder}
      <div className="m-4">
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <BasicTable
            columns={columns}
            data={data || []}
            isCanEdit
            isCanDelete
            onDelete={onDelete}
            onEdit={onEdit}
          />
        </div>
        {(pagination.totalPages || 1) > 1 ? (
          <div className="mt-4 flex justify-end">
            <Pagination
              onChange={(e) => {
                setPagination((previous: PaginationType) => ({
                  ...previous,
                  page: e - 1,
                }));
              }}
              defaultCurrent={1}
              total={pagination?.totalElement}
            />
          </div>
        ) : null}
      </div>
    </>
  );
};
