import { Button, Form, FormInstance, Input, Select } from "antd";
import { FormProvider } from "../../../provider/FormProvider";
import { FC, useCallback, useEffect, useState } from "react";
import { Annonce } from "../../../types/Annonce.types";
import { SimpleInput } from "../../../shared/ui/simpleInput";
import { useQueryUsers } from "../../../hooks/users/useUsers";
import { useUsersHandler } from "../../../hooks/users/useUsersHandler";
import { SelectValueType, User } from "../../../types";
import { Spinner } from "../../../shared/ui/spinner";
// import { useQueryObjets } from "../../../hooks/objets/useObjets";
// import { useObjetsHandler } from "../../../hooks/objets/useObjetsHandler";
import moment from "moment";
import { SimpleDatePicker } from "../../../shared/ui/simpleDatePicker";

interface IForm {
  handleSubmit: (
    e: unknown,
    handleReset?: () => void,
    onLoadingHandler?: () => void
  ) => void;
  selected?: unknown;
}

export const FormAnnoncesComponent: FC<IForm> = ({
  handleSubmit,
  selected,
}) => {
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [userSelected, setUserSelected] = useState<SelectValueType>();

  const { resetFields, setFieldsValue, setFieldValue } = form as FormInstance;
  const { TextArea } = Input;

  const handleReset = () => {
    resetFields();
  };
  const onLoadingHandler = () => {
    setIsLoading((prev: boolean) => !prev);
  };

  // for user searching
  const {
    users,
    isLoading: isLoadingUser,
    loadQuery,
    error: errorQueryUsers,
  } = useQueryUsers();
  const { onSearch: onSearchUser, onChange: onChangeUser } = useUsersHandler();

  // Wrap loadQueryUser with useCallback
  const loadQueryUser = useCallback(
    (params?: { page: number; size: number; username?: string }) => {
      loadQuery(params); // You can pass arguments here if needed
    },
    [loadQuery]
  ); // Dependencies should include anything that loadQueryUser depends on

  const [isFirstLoad, setIsFirstLoad] = useState(true);

  // for user searching
  // const {
  //   objets,
  //   isLoading: isLoadingObjet,
  //   loadQuery: loadQueryObj,
  //   error: errorQueryObjets,
  // } = useQueryObjets();
  // const { onSearch: onSearchObjet, onChange: onChangeObjet } =
  //   useObjetsHandler();

  // Wrap loadQueryObj with useCallback
  // const loadQueryObjet = useCallback(
  //   (params?: {
  //     page?: number;
  //     size?: number;
  //     nom?: string;
  //     userId?: number;
  //   }) => {
  //     loadQueryObj(params); // You can pass arguments here if needed
  //   },
  //   [loadQueryObj]
  // ); // Dependencies should include anything that loadQueryUser depends on

  useEffect(() => {
    if (isFirstLoad) {
      loadQueryUser();
      // loadQueryObjet();
      setIsFirstLoad(false); // Mark as loaded to prevent further calls
    }
  }, [isFirstLoad, loadQueryUser, userSelected]);

  useEffect(() => {
    if (selected) {
      setFieldsValue({ ...selected });
    }
  }, [selected, setFieldsValue]);

  if (errorQueryUsers) {
    return (
      <>Un erreur est survenu lors du chargement des utilisateurs et objets</>
    );
  }

  return (
    <>
      <FormProvider
        Form={Form}
        form={form}
        onFinish={(values: unknown) => {
          handleSubmit(values, handleReset, onLoadingHandler);
        }}
        onFinishFailed={(errorInfo) => {
          console.log("ERROR INFO ", errorInfo);
        }}
        debug={import.meta.env.DEV}
        initialValues={undefined}
      >
        <SimpleInput<Annonce> Form={Form} name="id" hidden />
        <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="libelle"
            label="Libelle"
            rules={[
              {
                required: true,
                message: "Le champ libelle est réquis.",
              },
            ]}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="descs"
            label="Description"
            rules={[
              {
                required: true,
                message: "Le champ Description est réquis.",
              },
            ]}
            customInput={<TextArea className="w-full h-full" />}
          />
        </div>
        <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="etat"
            label="Etat"
            rules={[
              {
                required: true,
                message: "Le champ Etat est réquis.",
              },
            ]}
          />
        </div>

        <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="user"
            label="Utilisateur"
            rules={[
              {
                required: false,
                message: "Le champ Utilisateur est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                allowClear
                loading={isLoadingUser}
                showSearch
                onSearch={(e) => onSearchUser(e as string, loadQuery)}
                filterOption={false}
                onChange={(e) => {
                  if (e) {
                    setUserSelected(e);
                  }
                  // if user is unselected
                  if (!e) {
                    setUserSelected(undefined);
                    setFieldValue("objet", undefined);
                  }
                  onChangeUser({ user: e }, loadQuery);
                }}
                labelInValue
                options={users?.map((user: User) => ({
                  label: user?.username,
                  value: user?.id,
                }))}
              />
            }
          />
        </div>

        {/* <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="objets"
            label="Objet"
            rules={[
              {
                required: true,
                message: "Le champ Objet est réquis.",
              },
            ]}
            customInput={
              <Select
                className="w-full"
                mode="multiple"
                allowClear
                loading={isLoadingObjet}
                showSearch
                onSearch={(e) => onSearchObjet(e as string, loadQueryObjet)}
                filterOption={false}
                onChange={(e) => onChangeObjet({ objet: e }, loadQueryObjet)}
                labelInValue
                options={
                  !userSelected
                    ? (emptyArray as never[])
                    : objets?.map((objet: Objet) => ({
                        label: objet?.nom,
                        value: objet?.id,
                      }))
                }
              />
            }
          />
        </div> */}

        <div className="w-full">
          <SimpleInput<Annonce>
            Form={Form}
            name="datePublication"
            label="Date de publication"
            rules={[
              {
                required: true,
                message: "Le champ Date de publication est réquis.",
              },
            ]}
            customInput={
              <SimpleDatePicker
                // disabled={true}
                className="w-full"
                disabledDate={(current) => {
                  let customDate = moment().format("YYYY-MM-DD");
                  return current && current < moment(customDate, "YYYY-MM-DD");
                }}
              />
            }
          />
        </div>

        <div className="flex flex-col justify-center">
          <Button type="primary" htmlType="submit">
            {isLoading ? <Spinner /> : <>Sauvegarder</>}
          </Button>
        </div>
      </FormProvider>
    </>
  );
};
