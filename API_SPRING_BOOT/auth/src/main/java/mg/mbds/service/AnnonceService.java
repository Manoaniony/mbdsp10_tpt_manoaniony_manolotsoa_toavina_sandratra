package mg.mbds.service;


import java.sql.Date;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mg.mbds.model.Annonce;

public interface AnnonceService extends CRUDService<Annonce> {
	Annonce updateAnnonces(Annonce annonces);
	
	//Page<Annonce> getAll(String descs, String etat, Long userId, Pageable pageable);
	
	Map<String,Object> getAll(String descs, String etat, Long userId,Date datePublication,String libelle, Pageable pageable);
}
