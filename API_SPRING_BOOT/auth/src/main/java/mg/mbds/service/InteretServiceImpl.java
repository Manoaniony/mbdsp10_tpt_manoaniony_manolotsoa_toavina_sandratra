package mg.mbds.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import mg.mbds.model.Annonce;
import mg.mbds.model.Interet;
import mg.mbds.model.Objet;
import mg.mbds.repository.InteretRepository;
import mg.mbds.repository.ObjetRepository;
import mg.mbds.specification.AnnonceSpecification;
import mg.mbds.specification.InteretSpecification;

@Service
public class InteretServiceImpl implements InteretService {
	
	@Autowired
	InteretRepository repository;
	
	@Autowired
	ObjetRepository objetRepository;

	@Override
	public Interet save(Interet entity) {
		return repository.save(entity);
	}

	@Override
	public Interet getById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	/*@Override
	public Page<Interet> getAll(int page) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void delete(Interet entity) {
		// TODO Auto-generated method stub
		repository.delete(entity);
	}

	@Override
	public int nombrepage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Interet saveIdUserObject(Interet interet) {
		Interet inte = this.save(interet);
		if(interet.getObjets().size() > 0){
			for(int i=0;i<interet.getObjets().size();i++) {
				Objet objet = objetRepository.getById(interet.getObjets().get(i).getId());
				if(objet != null) {
					objet.setInteret(inte);
					objetRepository.save(objet);
				}
			}
		}
		return inte;
	}

	@Override
	public Map<String, Object> getAll(Long userId, Long annonceId, Pageable pageable) {
		Map<String, Object> listePage = new HashMap<String, Object>();
		Specification<Interet> specification = InteretSpecification.filterByAttributes(userId,annonceId);
        Page<Interet> liste = repository.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	listePage.put("content", liste.getContent());
			listePage.put("totalElements", liste.getTotalElements());
			listePage.put("totalPages", liste.getTotalPages());
		}
		return listePage;
	}

}
