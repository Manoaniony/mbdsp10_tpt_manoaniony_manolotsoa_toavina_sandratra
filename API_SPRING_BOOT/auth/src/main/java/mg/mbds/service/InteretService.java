package mg.mbds.service;

import java.sql.Date;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import mg.mbds.model.Interet;
import mg.mbds.model.Objet;

public interface InteretService extends CRUDService<Interet> {
	
	Interet saveIdUserObject(Interet interet);
	
	Map<String,Object> getAll(Long userId,Long annonceId,Pageable pageable);

}
