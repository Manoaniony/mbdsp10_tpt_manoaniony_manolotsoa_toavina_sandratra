package mg.mbds.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import mg.mbds.model.Annonce;
import mg.mbds.model.Objet;
import mg.mbds.model.User;
import mg.mbds.repository.AnnonceRepository;
import mg.mbds.repository.ObjetRepository;
import mg.mbds.repository.UserRepository;
import mg.mbds.service.util.AppConstants;
import mg.mbds.specification.AnnonceSpecification;
import mg.mbds.specification.ObjetSpecification;

@Service
public class ObjetServiceImpl implements ObjetService {
	
	@Autowired
	ObjetRepository repository;
	
	@Autowired
	AnnonceRepository annonceRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	public Objet save(Objet entity) {
		if(entity.getAnnonce() != null) {
			Annonce annonce = annonceRepository.findById(entity.getAnnonce().getId()).get();
			entity.setAnnonce(annonce);
		}
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public Objet getById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	/*@Override
	public Page<Objet> getAll(int page) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void delete(Objet entity) {
		repository.delete(entity);
	}

	@Override
	public int nombrepage() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public Map<String, Object> getAll(String nom, String description ,String  categorie ,String etat , Long userId, Pageable pageable) {
		
		Map<String, Object> listePage = new HashMap<String, Object>();
		Specification<Objet> specification = ObjetSpecification.filterByAttributes(nom,description,categorie, etat , userId);
        Page<Objet> liste = repository.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	listePage.put("content", liste.getContent());
        	listePage.put("totalElements", liste.getTotalElements());
        	listePage.put("totalPages", liste.getTotalPages());
		}
		return listePage;
	}

	@Override
	public List<Objet> getByUser(Long idUser) {
		List<Objet> res = new ArrayList<>();
		User user = userRepository.findById(idUser).get();
		if(user != null) {
			List<Objet> liste = repository.findByUser(user);
			if(liste.size() > 0) {
				res = liste;
			}
		}
		return res;
	}

	/*@Override
	public List<Objet> getByUser(Long idUser) {
		List<Objet> res = new ArrayList<>();
		User user = userRepository.findById(idUser).get();
		if(user != null) {
			List<Annonce> listeAnnonce = annonceRepository.findByUser(user);
			if(listeAnnonce.size() > 0) {
				for (Annonce annonce : listeAnnonce) {
					List<Objet> listeObjet = repository.findByAnnonceAndEtat(annonce, AppConstants.ETAT_DISPO);
					if(listeObjet.size() > 0) {
						for (Objet obj : listeObjet) {
							res.add(obj);
						}
					}
				}
			}
		}
		return res;
	}*/
	
	
	
	

}
