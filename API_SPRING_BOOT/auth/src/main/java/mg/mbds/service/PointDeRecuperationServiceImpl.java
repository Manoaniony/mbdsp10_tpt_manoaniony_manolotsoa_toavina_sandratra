package mg.mbds.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import mg.mbds.model.PointDeRecuperation;
import mg.mbds.model.User;
import mg.mbds.repository.PointDeRecuperationRepository;
import mg.mbds.specification.PointDeRecuperationSpecification;
import mg.mbds.specification.UserSpecification;


@Service
public class PointDeRecuperationServiceImpl implements PointDeRecuperationService {
	
	public static int max = 50;
	@Autowired
	PointDeRecuperationRepository pointDeRecuperationRepository;

	@Override
	public PointDeRecuperation save(PointDeRecuperation entity) {
		return pointDeRecuperationRepository.save(entity);
	}

	@Override
	public PointDeRecuperation getById(Long id) {
		return pointDeRecuperationRepository.findById(id).get();
	}

	@Override
	public void delete(PointDeRecuperation entity) {
		pointDeRecuperationRepository.delete(entity);
	}

	@Override
	public int nombrepage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Object> getAll(String adresse, Float latitude, Float longitude, Pageable pageable) {
		Map<String, Object> listePage = new HashMap<String, Object>();
		Specification<PointDeRecuperation> specification = PointDeRecuperationSpecification.filterByAttributes(adresse,latitude, longitude);
        Page<PointDeRecuperation> liste = pointDeRecuperationRepository.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	listePage.put("content", liste.getContent());
        	listePage.put("totalElements", liste.getTotalElements());
        	listePage.put("totalPages", liste.getTotalPages());
		}
		return listePage;
	}

}
