package mg.mbds.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import mg.mbds.model.Annonce;
import mg.mbds.model.Match;
import mg.mbds.model.Objet;
import mg.mbds.repository.MatchRepository;
import mg.mbds.repository.ObjetRepository;
import mg.mbds.specification.AnnonceSpecification;
import mg.mbds.specification.MatchSpecification;


@Service
public class MatchServiceImpl implements MatchService {
	
	@Autowired
	MatchRepository repository;
	
	@Autowired
	ObjetRepository objetRepository;

	@Override
	public Match save(Match entity) {
		// TODO Auto-generated method stub
		return repository.save(entity);
	}

	@Override
	public Match getById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	/*@Override
	public Page<Match> getAll(int page) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void delete(Match entity) {
		// TODO Auto-generated method stub
		repository.delete(entity);
	}

	@Override
	public int nombrepage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Object> getAll(Date dateMatch, Pageable pageable) {
		Map<String, Object> listeMatchPage = new HashMap<String, Object>();
		Specification<Match> specification = MatchSpecification.filterByAttributes(dateMatch);
        Page<Match> liste = repository.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	listeMatchPage.put("content", liste.getContent());
        	listeMatchPage.put("totalElements", liste.getTotalElements());
        	listeMatchPage.put("totalPages", liste.getTotalPages());
		}
		return listeMatchPage;
	}

	@Override
	public List<Match> getByObjet(Long idObjet) {
		Objet objet = objetRepository.findById(idObjet).get();
		if(objet != null) {
			List<Match> liste = repository.findByObjet1OrObjet2(objet);
			if(liste.size() > 0) {
				return liste;
			}
		}
		return new ArrayList<>();
	}

}
