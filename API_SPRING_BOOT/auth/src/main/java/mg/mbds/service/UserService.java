package mg.mbds.service;

import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import mg.mbds.model.User;

import java.util.List;
import java.util.Map;

public interface UserService extends UserDetailsService {
    String signIn(User user);
    User save(User user);
    List<User> findAll();
    User findById(Long id);
    //User addPointDeRecupToUser(Long idUser,Long idPointRecup);
    void delete(User user);
    Map<String,Object> getAll(String username,String adresse,String tel,String email,String nom,Pageable pageable);
    User getByToken(String token);
}
