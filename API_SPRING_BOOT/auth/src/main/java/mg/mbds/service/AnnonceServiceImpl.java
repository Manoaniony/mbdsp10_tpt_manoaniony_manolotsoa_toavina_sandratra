package mg.mbds.service;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import mg.mbds.model.Annonce;
import mg.mbds.repository.AnnonceRepository;
import mg.mbds.specification.AnnonceSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class AnnonceServiceImpl implements AnnonceService {
	
	private static final Logger logger = LoggerFactory.getLogger(AnnonceServiceImpl.class);

	@Autowired
	AnnonceRepository annonceRepository;
	
	
	@Override
	public Annonce save(Annonce entity) {
		return annonceRepository.save(entity);
	}

	@Override
	public Annonce getById(Long id) {
		return annonceRepository.findById(id).get();
	}


	@Override
	public void delete(Annonce entity) {
		annonceRepository.delete(entity);
	}

	@Override
	public int nombrepage() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	/*@Override
	public Annonce updateAnnonces(Annonce annonce) {
        return annonceRepository.findById(annonce.getId())
                .map(an -> {
                	an.setDescs(annonce.getDescs());
                	an.setEtat(annonce.getEtat());
                	an.setDatePublication(annonce.getDatePublication());
                	an.setPhoto(annonce.getPhoto());
                    return annonceRepository.save(an);
                })
                .orElseGet(() -> {
                	annonce.setId(annonce.getId());
                    return annonceRepository.save(annonce);
                });
    }*/

	@Override
	public Map<String, Object> getAll(String descs, String etat, Long userId,Date datePublication,String libelle, Pageable pageable) {
		
        logger.info("Filter parameters - descs: {}, etat: {}, userId: {}", descs, etat, userId);
		
		Map<String, Object> listeAnnoncePage = new HashMap<String, Object>();
		Specification<Annonce> specification = AnnonceSpecification.filterByAttributes(descs, etat, userId,datePublication,libelle);
        Page<Annonce> liste = annonceRepository.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	logger.info("Result size: {}", liste.getTotalElements());
			listeAnnoncePage.put("content", liste.getContent());
			listeAnnoncePage.put("totalElements", liste.getTotalElements());
			listeAnnoncePage.put("totalPages", liste.getTotalPages());
		}
		return listeAnnoncePage;
	}

	@Override
	public Annonce updateAnnonces(Annonce annonces) {
		return annonceRepository.save(annonces);
	}

	/*@Override
	public Page<Annonce> getAll(String descs, String etat, Long userId, Pageable pageable) {
		/*Specification<Annonce> spec = Specification
                .where(AnnonceSpecification.withDescs(descs))
                .and(AnnonceSpecification.withEtat(etat))
                .and(AnnonceSpecification.withUserId(userId));
        return annonceRepository.findAll(spec, pageable);
		Specification<Annonce> specification = AnnonceSpecification.filterByAttributes(descs, etat, userId);
        return annonceRepository.findAll(specification, pageable);
	}*/

}
