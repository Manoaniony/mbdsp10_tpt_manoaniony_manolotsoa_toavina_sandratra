package mg.mbds.service;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mg.mbds.model.PointDeRecuperation;

public interface PointDeRecuperationService extends CRUDService<PointDeRecuperation> {
	Map<String,Object> getAll(String adresse , Float latitude , Float longitude , Pageable pageable);
}

