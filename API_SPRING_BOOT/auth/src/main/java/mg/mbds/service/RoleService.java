package mg.mbds.service;

import java.util.Collection;

import mg.mbds.model.Role;

public interface RoleService {
    Role save(Role role);

    void save(Collection<Role> roles);

    Role findByAuthority(String authority);
}
