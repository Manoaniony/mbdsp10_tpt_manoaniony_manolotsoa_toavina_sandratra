package mg.mbds.service;

import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

import mg.mbds.model.Objet;

public interface ObjetService extends CRUDService<Objet> {
	Map<String,Object> getAll(String nom, String description ,String  categorie ,String etat , Long userId, Pageable pageable);
	
	List<Objet> getByUser(Long idUser);
}
