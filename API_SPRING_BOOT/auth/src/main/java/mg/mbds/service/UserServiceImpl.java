package mg.mbds.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import mg.mbds.model.PointDeRecuperation;
import mg.mbds.model.User;
import mg.mbds.repository.PointDeRecuperationRepository;
import mg.mbds.repository.UserRepository;
import mg.mbds.service.util.JwtUtil;
import mg.mbds.specification.UserSpecification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired private UserRepository userDao;
    @Autowired private RoleService roleService;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private AuthenticationManager authenticationManager;
    @Autowired private JwtUtil jwtUtil;
    @Autowired private PointDeRecuperationRepository pointDeRecuperationRepository;

    @Override
    public String signIn(User user) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    user.getUsername(), user.getPassword()
            ));
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("bad creditiel for username " + user.getUsername());
        }
        User loadUserByUsername = loadUserByUsername(user.getUsername());
        String token = jwtUtil.generateToken(loadUserByUsername);
        return token;
    }

    @Override
    public User save(User user) {
    	if(user.getUpdate()) {
    		User verif = userDao.findById(user.getId()).get();
    		if(verif != null) {
    			if(user.getAuthorities() == null) {
    				user.setAuthorities(verif.getAuthorities());
    			}
    		}
    		userDao.save(user);
    	}else {
    		User loadedUser = userDao.findByUsername(user.getUsername());
            User checkMail = userDao.findByEmail(user.getEmail());
            if (loadedUser != null || checkMail != null)
                return null;
            else {
        		user.setPassword(passwordEncoder.encode(user.getPassword()));
                roleService.save(user.getAuthorities());
                userDao.save(user);
            }
    	}
        return user;
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);
        if (user == null || user.getId() == null) {
            throw new UsernameNotFoundException("user " + username + " not founded");
        } else {
            return user;
        }
    }

	@Override
	public User findById(Long id) {
		return userDao.findById(id).get();
	}

	/*@Override
	public User addPointDeRecupToUser(Long idUser, Long idPointRecup) {
		Optional<User> userOptional = userDao.findById(idUser);
        Optional<PointDeRecuperation> pointDeRecuperationOptional = pointDeRecuperationRepository.findById(idPointRecup);

        if (userOptional.isPresent() && pointDeRecuperationOptional.isPresent()) {
            User user = userOptional.get();
            PointDeRecuperation pointDeRecuperation = pointDeRecuperationOptional.get();
            user.getPointsDeRecuperation().add(pointDeRecuperation);
            return userDao.save(user);
        } else {
            throw new RuntimeException("User or Point of Recovery not found");
        }
	}*/

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		userDao.delete(user);
	}

	@Override
	public Map<String, Object> getAll(String username, String adresse, String tel, String email, String nom,
			Pageable pageable) {
		Map<String, Object> listePage = new HashMap<String, Object>();
		Specification<User> specification = UserSpecification.filterByAttributes(username,adresse,tel, email,nom);
        Page<User> liste = userDao.findAll(specification, pageable);
        if(liste.getNumberOfElements() > 0 && !liste.isEmpty() ) {
        	listePage.put("content", liste.getContent());
        	listePage.put("totalElements", liste.getTotalElements());
        	listePage.put("totalPages", liste.getTotalPages());
		}
		return listePage;
	}

	@Override
	public User getByToken(String token) {
		String username = jwtUtil.getUsernameFromToken(token);
		User user = loadUserByUsername(username);
		if(user != null) {
			return user;
		}
		return null;
	}
}
