package mg.mbds.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import mg.mbds.model.Match;
import mg.mbds.model.Objet;

public interface MatchService extends CRUDService<Match> {
	
	Map<String,Object> getAll(Date dateMatch, Pageable pageable);
	List<Match> getByObjet(Long idObjet);
}
