package mg.mbds.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;

public interface CRUDService<E> {
	E save(E entity);

	E getById(Long id);

	void delete(E entity);
	
	int nombrepage();
}
