package mg.mbds.model;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "annonce")
public class Annonce {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_seq")
    @SequenceGenerator(name = "annonce_seq", sequenceName = "annonce_id_seq", allocationSize = 1)
    private Long id;
	private String descs;
	private String etat;
	private Date datePublication;
	private String photo;
	private String libelle;
	
	@ManyToOne()
    @JoinColumn(name = "user_id")
	private User user;
	
	@OneToMany(mappedBy = "annonce")
	//@OneToMany(mappedBy = "annonce", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    //@JsonManagedReference
	@JsonIgnore
	private List<Objet> objets;
	
	
	public List<Objet> getObjets() {
		return objets;
	}


	public void setObjets(List<Objet> objets) {
		this.objets = objets;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}
	

	public String getPhoto() {
		return photo;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescs() {
		return descs;
	}

	public void setDescs(String descs) {
		this.descs = descs;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public Annonce(Long id, String descs, String photo, String etat, User user , String libelle) {
		super();
		this.id = id;
		this.descs = descs;
		this.photo = photo;
		this.etat = etat;
		this.user = user;
		this.libelle = libelle;
	}

	public Annonce() {
		super();
		datePublication = new Date(System.currentTimeMillis());
	}
	
	

}
