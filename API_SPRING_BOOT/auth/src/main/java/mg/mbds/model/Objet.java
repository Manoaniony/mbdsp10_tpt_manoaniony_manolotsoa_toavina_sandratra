package mg.mbds.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import mg.mbds.service.util.AppConstants;

@Entity
public class Objet {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "objet_seq")
    @SequenceGenerator(name = "objet_seq", sequenceName = "objet_id_seq", allocationSize = 1)
    private Long id;
	private String nom;
	private String description;
	private String categorie;
	private String etat;
	private String photo;
	
	@ManyToOne
    @JoinColumn(name = "annonce_id")
	//@JsonBackReference
	private Annonce annonce;
	
	@ManyToOne
    @JoinColumn(name = "interet_id")
	@JsonIgnore
    private Interet interet;
	
	@ManyToOne
    @JoinColumn(name = "user_id")
	//@JsonBackReference(value = "user-objet")
	private User user;
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Annonce getAnnonce() {
		return annonce;
	}

	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}

	public Interet getInteret() {
		return interet;
	}

	public void setInteret(Interet interet) {
		this.interet = interet;
	}

	public Objet() {
		super();
		this.etat = AppConstants.ETAT_DISPO;
	}
	
	
}
