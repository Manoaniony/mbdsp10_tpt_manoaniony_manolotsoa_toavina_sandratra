package mg.mbds.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class User implements UserDetails {
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_id_seq", allocationSize = 1)
    private Long id;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_role",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Collection<Role> authorities;
    private String password;
    private String username;
    private boolean accountNonExpired=true;
    private boolean accountNonLocked=true;
    private boolean credentialsNonExpired=true;
    private boolean enabled=true;
    private String adresse;
    private String tel;
    private String email;
    private String nom;
    @Transient
    private Boolean update;
    
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<Annonce> annonces;
    
    /*@ManyToMany
    @JoinTable(
      name = "user_pRecup", 
      joinColumns = @JoinColumn(name = "user_id"), 
      inverseJoinColumns = @JoinColumn(name = "point_recuperation_id"))
    private List<PointDeRecuperation> pointsDeRecuperation;*/
    
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<Interet> interets;
    
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    //@JsonManagedReference(value = "user-objet")
	private List<Objet> objets;
    
    
    
    public List<Objet> getObjets() {
		return objets;
	}


	public void setObjets(List<Objet> objets) {
		this.objets = objets;
	}


	public User() {
    	this.update = false;
    }
    

    public Boolean getUpdate() {
		return update;
	}


	public void setUpdate(Boolean update) {
		this.update = update;
	}


	public List<Interet> getInterets() {
		return interets;
	}


	public void setInterets(List<Interet> interets) {
		this.interets = interets;
	}


	/*public List<PointDeRecuperation> getPointsDeRecuperation() {
		return pointsDeRecuperation;
	}

	public void setPointsDeRecuperation(List<PointDeRecuperation> pointsDeRecuperation) {
		this.pointsDeRecuperation = pointsDeRecuperation;
	}*/

	public List<Annonce> getAnnonces() {
		return annonces;
	}

	public void setAnnonces(List<Annonce> annonces) {
		this.annonces = annonces;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

    
    

    public User(String password, String username, String adresse, String tel, String email, String nom) {
		super();
		this.password = password;
		this.username = username;
		this.adresse = adresse;
		this.tel = tel;
		this.email = email;
		this.nom = nom;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Collection<Role> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Role> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
