package mg.mbds.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PointDeRecuperation {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "point_de_recuperation_seq")
    @SequenceGenerator(name = "point_de_recuperation_seq", sequenceName = "point_de_recuperation_id_seq", allocationSize = 1)
    private Long id;
	private String adresse;
	private Float latitude;
	private Float longitude;
	
	/*@ManyToMany(mappedBy = "pointsDeRecuperation")
	@JsonIgnore
    private List<User> users;*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	/*public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}*/
	
	
}
