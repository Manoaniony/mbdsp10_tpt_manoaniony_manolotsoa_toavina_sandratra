package mg.mbds.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Match {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq")
    @SequenceGenerator(name = "match_seq", sequenceName = "match_id_seq", allocationSize = 1)
	private Long id;
	
	private Date dateMatch;
	
	/*@ManyToOne
    @JoinColumn(name = "user1_id")
    private User utilisateur1;
	
	@ManyToOne
    @JoinColumn(name = "user2_id")
    private User utilisateur2;*/
	
	@ManyToOne
    @JoinColumn(name = "objet1_id")
	private Objet objet1;
	
	@ManyToOne
    @JoinColumn(name = "objet2_id")
	private Objet objet2;
	
	private Float latitude;
	private Float longitude;
	

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Objet getObjet1() {
		return objet1;
	}

	public void setObjet1(Objet objet1) {
		this.objet1 = objet1;
	}

	public Objet getObjet2() {
		return objet2;
	}

	public void setObjet2(Objet objet2) {
		this.objet2 = objet2;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateMatch() {
		return dateMatch;
	}

	public void setDateMatch(Date dateMatch) {
		this.dateMatch = dateMatch;
	}

	/*public User getUtilisateur1() {
		return utilisateur1;
	}

	public void setUtilisateur1(User utilisateur1) {
		this.utilisateur1 = utilisateur1;
	}

	public User getUtilisateur2() {
		return utilisateur2;
	}

	public void setUtilisateur2(User utilisateur2) {
		this.utilisateur2 = utilisateur2;
	}*/

	public Match() {
		super();
		this.dateMatch = new Date();
	}
	
	
}
