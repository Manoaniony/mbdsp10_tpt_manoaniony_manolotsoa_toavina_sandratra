package mg.mbds.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Interet {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interet_seq")
    @SequenceGenerator(name = "interet_seq", sequenceName = "interet_id_seq", allocationSize = 1)
	private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "interet")
    private List<Objet> objets;
    
    @ManyToOne
    @JoinColumn(name = "annonce_id")
    private Annonce annonce;
    
    

	public Annonce getAnnonce() {
		return annonce;
	}

	public void setAnnonce(Annonce annonce) {
		this.annonce = annonce;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Objet> getObjets() {
		return objets;
	}

	public void setObjets(List<Objet> objets) {
		this.objets = objets;
	}

	public Interet() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}
