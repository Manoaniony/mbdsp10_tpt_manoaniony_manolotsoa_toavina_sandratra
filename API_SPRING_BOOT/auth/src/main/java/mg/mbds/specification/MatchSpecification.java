package mg.mbds.specification;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import mg.mbds.model.Match;

public class MatchSpecification {
	private static final Logger logger = LoggerFactory.getLogger(AnnonceSpecification.class);

    public static Specification<Match> filterByAttributes(Date dateMatch) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (dateMatch != null) {
                logger.info("Adding filter for datePublication: {}", dateMatch);
                predicates.add(criteriaBuilder.equal(root.get("dateMatch"), dateMatch));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
