package mg.mbds.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import mg.mbds.model.PointDeRecuperation;
import mg.mbds.model.User;

public class PointDeRecuperationSpecification {
	private static final Logger logger = LoggerFactory.getLogger(PointDeRecuperationSpecification.class);

    public static Specification<PointDeRecuperation> filterByAttributes(String adresse , Float latitude , Float longitude ) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (adresse != null && !adresse.isEmpty()) {
                predicates.add(criteriaBuilder.like(root.get("adresse"), "%" + adresse + "%"));
            }
            if (latitude != null && latitude != 0) {
            	 predicates.add(criteriaBuilder.equal(root.get("latitude"), latitude));
            }
            if (longitude != null && longitude != 0) {
           	 predicates.add(criteriaBuilder.equal(root.get("longitude"), longitude));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

}
