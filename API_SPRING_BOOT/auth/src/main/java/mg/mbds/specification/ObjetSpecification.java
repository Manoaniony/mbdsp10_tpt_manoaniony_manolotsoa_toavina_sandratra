package mg.mbds.specification;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import mg.mbds.model.Annonce;
import mg.mbds.model.Objet;

public class ObjetSpecification {
	private static final Logger logger = LoggerFactory.getLogger(ObjetSpecification.class);

    public static Specification<Objet> filterByAttributes(String nom, String description ,String  categorie ,String etat , Long userId) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (nom != null && !nom.isEmpty()) {
                predicates.add(criteriaBuilder.like(root.get("nom"), "%" + nom + "%"));
            }
            if (etat != null && !etat.isEmpty()) {
                predicates.add(criteriaBuilder.equal(root.get("etat"), etat));
            }
            if (description != null && !description.isEmpty()) {
            	predicates.add(criteriaBuilder.like(root.get("description"), "%" + description + "%"));
            }
            if (categorie != null && !categorie.isEmpty()) {
                predicates.add(criteriaBuilder.equal(root.get("categorie"), categorie));
            }
            if (userId != null) {
                predicates.add(criteriaBuilder.equal(root.get("user").get("id"), userId));
            }
            
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
