package mg.mbds.specification;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import mg.mbds.model.Annonce;

public class AnnonceSpecification {
	/*public static Specification<Annonce> withDescs(String descs) {
        return (root, query, criteriaBuilder) ->
                descs == null ? null : criteriaBuilder.like(root.get("descs"), "%" + descs + "%");
    }

    public static Specification<Annonce> withEtat(String etat) {
        return (root, query, criteriaBuilder) ->
                etat == null ? null : criteriaBuilder.equal(root.get("etat"), etat);
    }

    public static Specification<Annonce> withUserId(Long userId) {
        return (root, query, criteriaBuilder) ->
                userId == null ? null : criteriaBuilder.equal(root.get("user").get("id"), userId);
    }*/
	private static final Logger logger = LoggerFactory.getLogger(AnnonceSpecification.class);

    public static Specification<Annonce> filterByAttributes(String descs, String etat, Long userId ,Date datePublication,String libelle) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (descs != null && !descs.isEmpty()) {
                logger.info("Adding filter for descs: {}", descs);
                predicates.add(criteriaBuilder.like(root.get("descs"), "%" + descs + "%"));
            }
            if (libelle != null && !libelle.isEmpty()) {
                logger.info("Adding filter for descs: {}", libelle);
                predicates.add(criteriaBuilder.like(root.get("libelle"), "%" + libelle + "%"));
            }
            if (etat != null && !etat.isEmpty()) {
                logger.info("Adding filter for etat: {}", etat);
                predicates.add(criteriaBuilder.equal(root.get("etat"), etat));
            }
            if (userId != null) {
                logger.info("Adding filter for userId: {}", userId);
                predicates.add(criteriaBuilder.equal(root.get("user").get("id"), userId));
            }
            if (datePublication != null) {
                logger.info("Adding filter for datePublication: {}", datePublication);
                predicates.add(criteriaBuilder.equal(root.get("datePublication"), datePublication));
            }
            
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
