package mg.mbds.specification;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import mg.mbds.model.Annonce;
import mg.mbds.model.Interet;

public class InteretSpecification {
	private static final Logger logger = LoggerFactory.getLogger(InteretSpecification.class);

    public static Specification<Interet> filterByAttributes(Long userId ,Long annonceId) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (userId != null) {
                logger.info("Adding filter for userId: {}", userId);
                predicates.add(criteriaBuilder.equal(root.get("user").get("id"), userId));
            }
            if (annonceId != null) {
                predicates.add(criteriaBuilder.equal(root.get("annonce").get("id"), userId));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
