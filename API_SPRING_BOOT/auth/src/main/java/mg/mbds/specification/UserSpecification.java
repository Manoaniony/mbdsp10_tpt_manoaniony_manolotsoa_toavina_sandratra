package mg.mbds.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import mg.mbds.model.Objet;
import mg.mbds.model.User;

public class UserSpecification {
	private static final Logger logger = LoggerFactory.getLogger(ObjetSpecification.class);

    public static Specification<User> filterByAttributes(String username, String adresse, String tel, String email, String nom) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            
            if (nom != null && !nom.isEmpty()) {
                predicates.add(criteriaBuilder.like(root.get("nom"), "%" + nom + "%"));
            }
            if (username != null && !username.isEmpty()) {
            	predicates.add(criteriaBuilder.like(root.get("username"), "%" + username + "%"));
            }
            if (adresse != null && !adresse.isEmpty()) {
            	predicates.add(criteriaBuilder.like(root.get("adresse"), "%" + adresse + "%"));
            }
            if (tel != null && !tel.isEmpty()) {
            	predicates.add(criteriaBuilder.like(root.get("tel"), "%" + tel + "%"));
            }
            if (email != null && !email.isEmpty()) {
            	predicates.add(criteriaBuilder.like(root.get("email"), "%" + email + "%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
