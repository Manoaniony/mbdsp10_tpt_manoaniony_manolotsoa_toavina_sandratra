package mg.mbds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import mg.mbds.model.Role;
import mg.mbds.model.User;
import mg.mbds.service.UserService;

import java.util.Arrays;

@SpringBootApplication
public class AuthApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        /*if(true){
            User admin = new User("admin","admin");
            admin.setAuthorities(Arrays.asList(new Role("ROLE_ADMIN")));
            userService.save(admin);

            User chef = new User("chef","chef");
            chef.setAuthorities(Arrays.asList(new Role("ROLE_CHEF")));
            userService.save(chef);
        }*/

    }
    @Autowired
    private UserService userService;
}
