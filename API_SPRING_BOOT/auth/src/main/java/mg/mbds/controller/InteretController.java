package mg.mbds.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import mg.mbds.model.Annonce;
import mg.mbds.model.Interet;
import mg.mbds.model.Objet;
import mg.mbds.service.InteretService;

@RestController
@RequestMapping("/api/interets")
public class InteretController {
	ObjectMapper om = new ObjectMapper();
	public static final Logger log = LoggerFactory.getLogger(InteretController.class);
	
	@Autowired
	InteretService service;
	
	
	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody Interet interet) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(om.writeValueAsString(interet));
			//Map<String,Object> parametres = om.readValue(param, new TypeReference<>(){});
			obj.put("ok", true);
			obj.put("objects", service.saveIdUserObject(interet));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			Interet interet = service.getById(id);
			if (interet != null) {
				service.delete(interet);
				obj.put("ok", true);
				obj.put("objects", new Annonce());
				obj.put("message", "success");
				obj.put("status", 200);
				return new ResponseEntity<>(obj, HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<Object> getAll(
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) Long annonceId,
            Pageable pageable) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			obj.put("ok", true);
			obj.put("objects", service.getAll(userId, annonceId, pageable));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/getById/{id}")
	public ResponseEntity<Object> getById(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(om.writeValueAsString(id));
			obj.put("ok", true);
			obj.put("objects", service.getById(id));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> edit(@PathVariable Long id, @RequestBody Interet interet) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			interet.setId(id);
			log.info(om.writeValueAsString(id));
			obj.put("ok", true);
			obj.put("objects", service.save(interet));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
}
