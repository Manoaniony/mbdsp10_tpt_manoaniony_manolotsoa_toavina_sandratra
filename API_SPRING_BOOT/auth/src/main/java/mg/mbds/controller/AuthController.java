package mg.mbds.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import mg.mbds.model.User;
import mg.mbds.service.UserService;
import mg.mbds.service.util.JwtUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	ObjectMapper om = new ObjectMapper();
	public static final Logger log = LoggerFactory.getLogger(AnnonceController.class);
	
	@Autowired
    private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService service;
	@Autowired private JwtUtil jwtUtil;

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody Map<String, String> credentials, HttpServletResponse response) throws IOException {
    	Map<String,Object> obj = new HashMap<String,Object>();
    	Map<String,Object> res = new HashMap<String,Object>();
        try {
        	log.info(om.writeValueAsString(credentials));
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(credentials.get("username"), credentials.get("password"))
            );
            User loadUserByUsername = (User) service.loadUserByUsername(credentials.get("username"));
            String token = jwtUtil.generateToken(loadUserByUsername);
            
    		obj.put("ok",true);
    		obj.put("objects", token);
    		obj.put("message","success");
    		obj.put("status", 200);
    		return new ResponseEntity<>(obj, HttpStatus.OK);
        } catch (AuthenticationException e) {
        		obj.put("ok",false);
        		obj.put("objects",null);
        		obj.put("message","Bad credentials");
        		obj.put("status", 401);
        		e.printStackTrace();
        		return new ResponseEntity<>(obj, HttpStatus.UNAUTHORIZED);
        }
    }
    
    @PostMapping("/loginUser")
    public ResponseEntity<Object> loginUser(@RequestBody Map<String, String> credentials, HttpServletResponse response) throws IOException {
    	Map<String,Object> obj = new HashMap<String,Object>();
    	Map<String,Object> res = new HashMap<String,Object>();
        try {
        	log.info(om.writeValueAsString(credentials));
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(credentials.get("username"), credentials.get("password"))
            );
            User loadUserByUsername = (User) service.loadUserByUsername(credentials.get("username"));
            String token = jwtUtil.generateToken(loadUserByUsername);
            res.put("token", token);
            res.put("user", loadUserByUsername);
    		obj.put("ok",true);
    		obj.put("objects", res);
    		obj.put("message","success");
    		obj.put("status", 200);
    		return new ResponseEntity<>(obj, HttpStatus.OK);
        } catch (AuthenticationException e) {
        		obj.put("ok",false);
        		obj.put("objects",null);
        		obj.put("message","Bad credentials");
        		obj.put("status", 401);
        		e.printStackTrace();
        		return new ResponseEntity<>(obj, HttpStatus.UNAUTHORIZED);
        }
    }
}
