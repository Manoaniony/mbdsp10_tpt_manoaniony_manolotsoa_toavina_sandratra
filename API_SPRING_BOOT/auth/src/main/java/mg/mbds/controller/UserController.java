package mg.mbds.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import mg.mbds.model.Annonce;
import mg.mbds.model.Role;
import mg.mbds.model.User;
import mg.mbds.service.UserService;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {
	ObjectMapper om = new ObjectMapper();
	public static final Logger log = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;

    @PostMapping("/save")
    public ResponseEntity<Object> save(@RequestBody User user) {
    	Map<String,Object> obj = new HashMap<String,Object>();
    	try {
    		log.info(om.writeValueAsString(user));
    		user.setAuthorities(Arrays.asList(new Role("ROLE_USER")));
    		User verif = userService.save(user);
    		if(verif == null) {
    			obj.put("ok",false);
        		obj.put("objects", null);
        		obj.put("message","Username ou Email existant");
        		obj.put("status", 200);
    		}else {
    			obj.put("ok",true);
        		obj.put("objects", userService.save(user));
        		obj.put("message","success");
        		obj.put("status", 200);
    		}
    		return new ResponseEntity<>(obj, HttpStatus.OK);
    	}catch(Exception e) {
    		obj.put("ok",false);
    		obj.put("objects",null);
    		obj.put("message","error");
    		obj.put("status", 404);
    		e.printStackTrace();
    		return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
    	}
    }

    @GetMapping("/getByUserName/{username}")
    public ResponseEntity<Object> loadUserByUsername(@PathVariable String username) throws UsernameNotFoundException {
        Map<String,Object> obj = new HashMap<String,Object>();
    	try {
    		obj.put("ok",true);
    		obj.put("objects", userService.loadUserByUsername(username));
    		obj.put("message","success");
    		obj.put("status", 200);
    		return new ResponseEntity<>(obj, HttpStatus.OK);
    	}catch(Exception e) {
    		obj.put("ok",false);
    		obj.put("objects",null);
    		obj.put("message","error");
    		obj.put("status", 404);
    		e.printStackTrace();
    		return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
    	}
    }
    
   /* @PostMapping("/addPointRecupToUser")
    public ResponseEntity<Object> addPointRecupToUser(@RequestBody String param){
        Map<String,Object> obj = new HashMap<String,Object>();
    	try {
    		Map<String,Object> parametres = om.readValue(param, new TypeReference<>(){});
    		obj.put("ok",true);
    		obj.put("objects", userService.addPointDeRecupToUser(Long.parseLong(parametres.get("idUser").toString()), Long.parseLong(parametres.get("idPointRecup").toString())));
    		obj.put("message","success");
    		obj.put("status", 200);
    		return new ResponseEntity<>(obj, HttpStatus.OK);
    	}catch(Exception e) {
    		obj.put("ok",false);
    		obj.put("objects",null);
    		obj.put("message","error");
    		obj.put("status", 404);
    		e.printStackTrace();
    		return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
    	}
    }*/
    
    @PutMapping("/update/{id}")
	public ResponseEntity<Object> edit(@PathVariable Long id, @RequestBody User user) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(om.writeValueAsString(id));
			user.setId(id);
			user.setUpdate(true);
			obj.put("ok", true);
			obj.put("objects", userService.save(user));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
    
    @DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			User user = userService.findById(id);
			if (user != null) {
				userService.delete(user);
				obj.put("ok", true);
				obj.put("objects", new Annonce());
				obj.put("message", "success");
				obj.put("status", 200);
				return new ResponseEntity<>(obj, HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}

	}
    
    @GetMapping("/getAll")
	public ResponseEntity<Object> getAll(@RequestParam(required = false) String username ,
            @RequestParam(required = false) String adresse ,
            @RequestParam(required = false) String tel ,
            @RequestParam(required = false) String email ,
            @RequestParam(required = false) String nom  ,
            Pageable pageable) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			obj.put("ok", true);
			obj.put("objects", userService.getAll(username,adresse,tel, email,nom ,pageable));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
    
    @GetMapping("/getById/{id}")
	public ResponseEntity<Object> getById(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			obj.put("ok", true);
			obj.put("objects", userService.findById(id));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
    
    
    @GetMapping("/getByToken")
	public ResponseEntity<Object> getByToken(@RequestParam String param) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(param);
			obj.put("ok", true);
			obj.put("objects", userService.getByToken(param));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
    
    /*public UserController(UserService userService) {
        this.userService = userService;
    }*/
}
