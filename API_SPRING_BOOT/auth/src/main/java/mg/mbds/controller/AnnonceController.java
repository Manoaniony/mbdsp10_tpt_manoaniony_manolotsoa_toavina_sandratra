package mg.mbds.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import mg.mbds.model.Annonce;
import mg.mbds.model.User;
import mg.mbds.service.AnnonceService;
import mg.mbds.service.UserService;

@RestController
@RequestMapping("/api/annonces")
public class AnnonceController {
	ObjectMapper om = new ObjectMapper();
	public static final Logger log = LoggerFactory.getLogger(AnnonceController.class);
	@Autowired
	AnnonceService service;

	@Autowired
	UserService userService;

	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody Annonce annonce) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(om.writeValueAsString(annonce));
			obj.put("ok", true);
			obj.put("objects", service.save(annonce));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/getById/{id}")
	public ResponseEntity<Object> getById(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			log.info(om.writeValueAsString(id));
			obj.put("ok", true);
			obj.put("objects", service.getById(id));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> edit(@PathVariable Long id, @RequestBody Annonce updatedAnnonce) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			updatedAnnonce.setId(id);
			log.info(om.writeValueAsString(id));
			obj.put("ok", true);
			obj.put("objects", service.updateAnnonces(updatedAnnonce));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteAnnonce(@PathVariable Long id) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			Annonce annonce = service.getById(id);
			if (annonce != null) {
				service.delete(annonce);
				obj.put("ok", true);
				obj.put("objects", new Annonce());
				obj.put("message", "success");
				obj.put("status", 200);
				return new ResponseEntity<>(obj, HttpStatus.OK);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}

	}
	
	@GetMapping("/getAll")
	public ResponseEntity<Object> getAll(@RequestParam(required = false) String descs,
            @RequestParam(required = false) String etat,
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) Date datePublication,
            @RequestParam(required = false) String libelle,
            Pageable pageable) {
		Map<String, Object> obj = new HashMap<String, Object>();
		try {
			obj.put("ok", true);
			obj.put("objects", service.getAll(descs, etat, userId, datePublication,libelle, pageable));
			obj.put("message", "success");
			obj.put("status", 200);
			return new ResponseEntity<>(obj, HttpStatus.OK);
		} catch (Exception e) {
			obj.put("ok", false);
			obj.put("objects", null);
			obj.put("message", "error");
			obj.put("status", 404);
			e.printStackTrace();
			return new ResponseEntity<>(obj, HttpStatus.NOT_FOUND);
		}
	}

}
