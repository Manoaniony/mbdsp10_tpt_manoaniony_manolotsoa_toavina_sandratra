package mg.mbds.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mg.mbds.model.Annonce;
import mg.mbds.model.Objet;
import mg.mbds.model.User;

@Repository
public interface ObjetRepository extends JpaRepository<Objet, Long> ,JpaSpecificationExecutor<Objet>{
	List<Objet> findByAnnonceAndEtat(Annonce annonce,String etat);
	List<Objet> findByUser(User user);
}
