package mg.mbds.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mg.mbds.model.PointDeRecuperation;
import mg.mbds.model.User;


@Repository
public interface PointDeRecuperationRepository extends JpaRepository<PointDeRecuperation, Long>,JpaSpecificationExecutor<PointDeRecuperation> {
}
