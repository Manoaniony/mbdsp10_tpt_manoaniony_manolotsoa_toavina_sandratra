package mg.mbds.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mg.mbds.model.Annonce;
import mg.mbds.model.User;


@Repository
public interface AnnonceRepository extends JpaRepository<Annonce, Long>,JpaSpecificationExecutor<Annonce> {
	Optional<Annonce> findById(Long ind);
	List<Annonce> findByUser(User user);
}
