package mg.mbds.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mg.mbds.model.Annonce;
import mg.mbds.model.Match;
import mg.mbds.model.Objet;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long>,JpaSpecificationExecutor<Match> {
	@Query("SELECT m FROM Match m WHERE m.objet1 = :objet OR m.objet2 = :objet")
    List<Match> findByObjet1OrObjet2(@Param("objet") Objet objet);
}
