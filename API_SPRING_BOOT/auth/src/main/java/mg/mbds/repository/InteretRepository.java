package mg.mbds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import mg.mbds.model.Annonce;
import mg.mbds.model.Interet;


@Repository
public interface InteretRepository extends JpaRepository<Interet, Long> ,JpaSpecificationExecutor<Interet>{

}
