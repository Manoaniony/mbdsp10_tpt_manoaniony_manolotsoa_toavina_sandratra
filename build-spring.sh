#!/bin/bash

# Navigate to the specific directory
cd "$(pwd)/API_SPRING_BOOT/auth" || { echo "Failed to change directory"; exit 1; }

# Print the current working directory
echo "Current working directory: $(pwd)"

# Build the Spring Boot by cleaning the target folder and installing the dependancies what needed
./mvnw clean install
