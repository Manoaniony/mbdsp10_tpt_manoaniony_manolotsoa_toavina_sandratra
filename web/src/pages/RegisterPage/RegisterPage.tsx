import { Button, Card, Form, Input } from "antd";
import Title from "antd/es/typography/Title";
import { FormProvider } from "../../provider/FormProvider";
import { SimpleInput } from "../../shared/ui/simpleInput";
import { ChangeEvent, useState } from "react";
import { CustomButtonFilled } from "../../shared/ui/button";
import { useNavigate } from "react-router-dom";

export const RegisterPage = () => {
  const [form] = Form.useForm();
  const { Password } = Input
  const [newPassword, setNewPassword] = useState<string>();
  const [, setConfirmPassword] = useState<string>();
  const navigate = useNavigate();

  const handleSignup: ((values: unknown) => void) = (values: unknown) => {
    console.log("click signup ", values);
  }

  const goToLogin: () => void = () => {
    navigate("/login")
  }

  return (
    <div className="login__container">
      <div className="flex justify-center items-center">
        <Card className="w-[600px]">
          <div className="login__content-card">
            <div className="login__content-card-item ">
              <div className="login__form">
                <div className="login__form-header">
                  <Title className="text-center">S'inscrire</Title>
                </div>
                <div>
                  <FormProvider
                    Form={Form}
                    form={form}
                    onFinish={(values: unknown) => {
                      handleSignup(values)
                    }}
                    onFinishFailed={(errorInfo) => {
                      console.log("ERROR INFO ", errorInfo);
                    }}
                    debug={import.meta.env.DEV}
                    initialValues={undefined}>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="email"
                        label="Email"
                        rules={[
                          {
                            required: true, message: 'Le champ Email est réquis.'
                          },
                          {
                            type: 'email',
                            message: 'Veuillez entrer un adresse Email valide',
                          },
                        ]}
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="adresse"
                        label="Adresse"
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="tel"
                        label="Téléphone"
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="password"
                        label="Mot de passe"
                        rules={[
                          {
                            required: true, message: 'Le champ Mot de passe est réquis.'
                          },
                        ]}
                        customInput={
                          <Password
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setNewPassword(e?.target?.value)}
                          />
                        }
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="confirmPassword"
                        label="Confirmation Mot de passe"
                        // dependencies={["newPassword"]}
                        rules={[
                          {
                            required: true, message: 'Le champ Confirmation Mot de passe est réquis.'
                          },
                          {
                            validator: (_rule, value) => {
                              return new Promise<void>((resolve, reject) => {
                                if (value && newPassword === value) {
                                  resolve();
                                  return;
                                }
                                reject("Error")
                              })
                            },
                            message: "Les 2 champs doivent etre identiques"
                          }
                        ]}
                        customInput={
                          <Password
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setConfirmPassword(e?.target?.value)}
                            placeholder="Confirmer votre mot de passe" />
                        }
                      />
                    </div>

                    <div className="flex flex-col justify-center">
                      <Button
                        type="primary"
                        htmlType="submit"
                      >
                        S'inscrire
                      </Button>
                    </div>
                    <div>
                      <CustomButtonFilled onClick={goToLogin} className="w-full">J'ai déjà un compte</CustomButtonFilled>
                    </div>
                  </FormProvider>
                </div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  )
}