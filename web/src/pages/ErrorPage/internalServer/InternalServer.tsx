import { CustomError } from "../../../shared/components/error";

export const InternalServer = () => {
  return (
    <CustomError status={500} message="Il y a une erreur dans le serveur" />
  )
}