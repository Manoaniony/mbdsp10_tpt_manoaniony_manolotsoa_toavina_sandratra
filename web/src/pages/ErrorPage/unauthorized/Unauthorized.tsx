import { CustomError } from "../../../shared/components/error";

export const Unauthorized = () => {
  return (
    <CustomError status={401} message="Vous n'êtes pas autoriser à accéder cette page." />
  )
}