import { Button, Card, Form, Input } from "antd"
import Title from "antd/es/typography/Title"
import { FormProvider } from "../../provider/FormProvider";
import { SimpleInput } from "../../shared/ui/simpleInput";
import { ChangeEvent, } from "react";
import { Link } from "../../shared/ui/link";
import { CustomButtonFilled } from "../../shared/ui/button";
import { useNavigate } from "react-router-dom"

export const LoginPage = () => {

  const [form] = Form.useForm();
  const { Password } = Input
  const navigate = useNavigate();

  const goToRegister: () => void = () => {
    navigate("/register")
  }

  const handleLogin: ((values: unknown) => void) = (values: unknown) => {
    console.log("click login ", values);
  }
  return (
    <div className="login__container">
      <div className="flex justify-center items-center">
        <Card className="w-[600px]">
          <div className="login__content-card">
            <div className="login__content-card-item ">
              <div className="login__form">
                <div className="login__form-header">
                  <Title className="text-center">Se connecter</Title>
                </div>
                <div>
                  <FormProvider
                    Form={Form}
                    form={form}
                    onFinish={(values: unknown) => {
                      handleLogin(values)
                    }}
                    onFinishFailed={(errorInfo) => {
                      console.log("ERROR INFO ", errorInfo);
                    }}
                    debug={import.meta.env.DEV}
                    initialValues={undefined}>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="email"
                        label="Email"
                        rules={[
                          {
                            required: true, message: 'Le champ Email est réquis.'
                          },
                          {
                            type: 'email',
                            message: 'Veuillez entrer un adresse Email valide',
                          },
                        ]}
                      />
                    </div>
                    <div className="w-full">
                      <SimpleInput
                        Form={Form}
                        name="password"
                        label="Mot de passe"
                        rules={[
                          {
                            required: true, message: 'Le champ Mot de passe est réquis.'
                          },
                        ]}
                        customInput={
                          <Password
                            onChange={(e: ChangeEvent<HTMLInputElement>) => console.log((e?.target?.value))}
                          />
                        }
                      />
                    </div>

                    <Link path="/">Mot de passe oublié?</Link>
                    <div className="flex flex-col justify-center">
                      <Button
                        type="primary"
                        htmlType="submit"
                      >
                        Se connecter
                      </Button>
                    </div>
                    <div>
                      <CustomButtonFilled onClick={goToRegister} color="success" className="w-full">Créer un compte</CustomButtonFilled>
                    </div>
                  </FormProvider>

                </div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  )

}