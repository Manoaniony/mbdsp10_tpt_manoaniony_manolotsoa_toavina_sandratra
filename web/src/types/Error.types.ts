export type ErrorType = {
  status: 500 | 404 | 403 | 400 | 401
  message?: string
}