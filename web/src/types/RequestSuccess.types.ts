import { AxiosHeaders } from "axios"
import { DataSuccessType } from "./DataSuccess.types"

export type RequestSuccessType<T> = {
  config?: unknown
  data?: DataSuccessType<T>
  headers?: AxiosHeaders
  request?: XMLHttpRequest
  status?: number
  statusText?: string
}