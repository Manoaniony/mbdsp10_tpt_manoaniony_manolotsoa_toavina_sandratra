export type DataSuccessType<T> = {
  dataResponse: {
    ok: boolean
    object: T | T[]
  },
  message?: string
  status?: number
}