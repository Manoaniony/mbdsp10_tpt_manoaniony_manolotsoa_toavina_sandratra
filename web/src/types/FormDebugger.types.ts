import { Form as FormAntd, FormInstance } from "antd";

export type FormDebuggerType = {
  Form?: typeof FormAntd;
  form?: FormInstance;
}