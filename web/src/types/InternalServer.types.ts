export type InternalServerType = {
  dataResponse: null,
  message?: string
  status: 500
}