import { Form as FormAntd, FormInstance } from "antd"

export type CustomFormProviderTypes = {
  Form: (typeof FormAntd),
  initialValues?: never,
  form: FormInstance | undefined,
  onFinish: (values: unknown) => void,
  onFinishFailed: (values: unknown) => void,
  debug?: boolean
}
