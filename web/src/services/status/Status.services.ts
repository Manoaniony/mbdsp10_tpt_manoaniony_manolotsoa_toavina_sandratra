import { AxiosError } from "axios";
import type { DataSuccessType } from "../../types/DataSuccess.types";
import { request } from "../../utils";
import { InternalServerType } from "../../types";

export const getOk: () => Promise<DataSuccessType<{ ok: boolean }> | AxiosError> = () => {
  return request({ url: "/api/ok" }) as Promise<DataSuccessType<{ ok: boolean }>>
}

export const getInternalServer: () => Promise<InternalServerType | AxiosError> = () => {
  return request({ url: "/api/internal-server" }) as Promise<InternalServerType>
}