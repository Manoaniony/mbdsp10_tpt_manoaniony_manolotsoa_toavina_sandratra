import { AxiosError } from "axios";
import type { User } from "../../types";
import type { DataSuccessType } from "../../types/DataSuccess.types";
import { request } from "../../utils";

export const getUserMock: () => Promise<DataSuccessType<User> | AxiosError> = () => {
  return request({ url: "/api/mock_users" }) as Promise<DataSuccessType<User>>
}