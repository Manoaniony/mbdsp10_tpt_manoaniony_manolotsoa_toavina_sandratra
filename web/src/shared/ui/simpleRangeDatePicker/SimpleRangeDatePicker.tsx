import { DatePicker } from "antd";
import localeFr from 'antd/es/date-picker/locale/fr_FR';
import 'dayjs/locale/fr';
import dayjs from "dayjs";
import { RangePickerProps } from "antd/es/date-picker";

export const SimpleRangePicker = (props: RangePickerProps) => {
  dayjs.locale('fr');
  const { RangePicker } = DatePicker;
  const { locale, ...rest } = props
  return <RangePicker {...rest} locale={localeFr || locale} />
}
