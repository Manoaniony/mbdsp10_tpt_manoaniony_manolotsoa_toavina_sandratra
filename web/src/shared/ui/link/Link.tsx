import React from "react";
import style from "./style.module.scss";
import classnames from "classnames";

type PropsType = {
  path: string;
  children?: string | React.ReactNode;
};

export const Link = ({ children, path }: PropsType) => {
  return (
    <a className={classnames(style.link__content, "text-right")} href={path}>
      {" "}
      {children}{" "}
    </a>
  );
}
