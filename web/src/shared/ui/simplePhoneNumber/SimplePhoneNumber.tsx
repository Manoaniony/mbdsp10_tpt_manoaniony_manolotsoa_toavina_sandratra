import { FC } from "react"
import { DefaultInputComponentProps } from "react-phone-number-input"
import PhoneInput from 'react-phone-number-input'
import 'react-phone-number-input/style.css'

interface ISimplePN {
  onChange: (e: never) => void
}

export const SimplePhoneNumber: FC<DefaultInputComponentProps & ISimplePN> = (props) => {
  const { onChange } = props;
  return (
    <PhoneInput
      {...props}
      onChange={onChange}
    />
  )
}
