import { Button, ButtonProps, ConfigProvider } from "antd"
import { FC } from "react"
import { getColor } from "../../../themes/color.custom"

type ButtonFilledType = {
  color?: "success" | "info" | "warning" | "error",
}

export const CustomButtonFilled: FC<ButtonFilledType & ButtonProps> = ({ color, ...props }) => {
  const { children, ...rest } = props
  return (
    color ? <ConfigProvider
      theme={{
        token: {
          // Seed Token
          colorPrimary: getColor(color),
          borderRadius: 6,

          // Alias Token
          // colorBgContainer: '#f6ffed',
        }
      }}
    >
      <Button {...rest} type="primary">{children}</Button>
    </ConfigProvider> :
      <Button {...rest}>{children}</Button>
  )
}