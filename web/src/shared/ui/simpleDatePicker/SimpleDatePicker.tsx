import { DatePicker, DatePickerProps } from "antd";
import localeFr from 'antd/es/date-picker/locale/fr_FR';
import 'dayjs/locale/fr';
import dayjs from "dayjs";

export const SimpleDatePicker = (props: DatePickerProps) => {
  dayjs.locale('fr');
  const { locale, ...rest } = props
  return <DatePicker {...rest} locale={localeFr || locale} />
}
