import { Input } from "antd";
import styles from "./styles.module.scss";
import { Rule } from "antd/es/form";
import { SimpleInputTypes } from "../../../types/SimpleInput.types";

export const SimpleInput = <T,>({
  Form,
  name,
  label,
  hidden,
  rules,
  customInput,
  disabled,
  hasFeedback,
  valuePropName,
  onChange,
  onPressEnter,
  maxLength,
  spaceNoWrap
}: SimpleInputTypes<T>) => {
  return (
    <>
      <span className={`${spaceNoWrap ? "whitespace-nowrap w-full max-w-[140px]" : ""}`}>
        {label && <label className={`${styles.input__label}`}>
          {label}
          {
            rules?.some((rule: Rule) => (((rule as (never | { required: boolean }))?.required === true))) && (
              <span className={styles.input__required}> *</span>
            )
          }
        </label>}
      </span>

      {name ? (
        <>
          <Form.Item<T>
            hidden={hidden}
            name={name as never}
            rules={rules}
            hasFeedback={hasFeedback}
            valuePropName={valuePropName}
            className={spaceNoWrap ? "w-full" : "m-0"}
          >
            {!customInput ?
              <Input maxLength={maxLength} onChange={onChange} onPressEnter={onPressEnter} disabled={disabled} className="w-full" /> :
              customInput
            }
          </Form.Item>
        </>
      ) :
        (
          <>
            {!customInput ?
              <Input max={maxLength} onChange={onChange} onPressEnter={onPressEnter} disabled={disabled} className="w-full" /> :
              customInput
            }
          </>
        )
      }
    </>
  )
}
