import { useContext, useEffect, useState } from 'react'
import './App.css'
import { useQuery } from '@tanstack/react-query';
import { getUserMock } from './services/users/Users.services';
import { Button } from 'antd';
import { ThemeContext } from './context/ThemeContext';
import { darkTheme } from './themes/dark.mode';
import { lightTheme } from './themes/light.mode';
import { getInternalServer, getOk } from './services/status/Status.services';

export const App = () => {
  const [count, setCount] = useState<number>(0);
  const { isDark, handleToggleClick } = useContext(ThemeContext)
  // Queries
  const { data: response, error, refetch, isFetching } = useQuery({
    queryKey: ['userMock'],
    queryFn: getUserMock,
    enabled: false
  })

  const { data: responseOk, error: errorOk, refetch: refetchOk, isFetching: isFetchingOk } = useQuery({
    queryKey: ['ok'],
    queryFn: getOk,
    enabled: false
  })

  const { data: responseIS, error: errorIS, refetch: refetchIS, isFetching: isFetchingIS } = useQuery({
    queryKey: ['internalServer'],
    queryFn: getInternalServer,
    enabled: false
  })

  useEffect(() => {
    document.body.style.backgroundColor = (isDark ? darkTheme.token.colorBgContainer : lightTheme.token.colorBgContainer) as string
    document.body.style.color = (isDark ? darkTheme.token.colorText : lightTheme.token.colorText) as string
  }, [isDark]);

  const handleClickMockUser: () => void = () => {
    refetch()
  }

  const handleClickOk: () => void = () => {
    refetchOk()
  }

  const handleClickIS: () => void = () => {
    refetchIS()
  }

  if (isFetching || isFetchingOk || isFetchingIS) {
    return <>Chargement...</>
  }

  if (response) {
    console.log(response);
  }
  if (responseOk) {
    console.log(responseOk);
  }
  if (responseIS) {
    console.log(responseIS);
  }

  if (error || errorOk || errorIS) {
    return <>Un erreur est survenue</>
  }


  return (
    <>
      <h1>Vite + React</h1>
      <div className="card">
        <Button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </Button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <div className="text-3xl font-bold underline">
        Hello world!!!
      </div>
      <div>
        <Button onClick={handleToggleClick}>Change theme to {isDark ? "Light" : "Dark"}</Button>
      </div>
      <Button onClick={handleClickMockUser}>fetch Mock User</Button>
      <Button onClick={handleClickOk}>fetch Ok</Button>
      <Button onClick={handleClickIS}>Internal server</Button>
      {/* {response && JSON.stringify(response)} */}
      {/* {responseOk && JSON.stringify(responseOk)} */}
      {responseIS && JSON.stringify(responseIS)}
    </>
  )
}
