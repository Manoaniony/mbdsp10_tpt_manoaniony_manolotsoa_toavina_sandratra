import { createBrowserRouter } from "react-router-dom"
import { App } from "./App";
import { LoginPage } from "./pages/LoginPage";
import { RegisterPage } from "./pages/RegisterPage";
import { InternalServer, NotFound } from "./pages/ErrorPage";
import { Unauthorized } from "./pages/ErrorPage/unauthorized";
import { BadRequest } from "./pages/ErrorPage/bad-request";
import { Forbidden } from "./pages/ErrorPage/forbidden";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <LoginPage />,
  },
  {
    path: "/home",
    element: <App />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  {
    path: "/register",
    element: <RegisterPage />,
  },
  // error-page
  {
    path: "/internal-server",
    element: <InternalServer />,
  },
  {
    path: "/forbidden",
    element: <Forbidden />,
  },
  {
    path: "/bad-request",
    element: <BadRequest />,
  },
  {
    path: "/unauthorized",
    element: <Unauthorized />,
  },
  {
    path: "/not-found",
    element: <NotFound />,
  },
]);
