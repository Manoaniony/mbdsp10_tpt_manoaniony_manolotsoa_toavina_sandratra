import type { ThemeType } from "../types/Themes.types";

export const darkTheme: ThemeType = {
  token: {
    colorPrimary: '#1890ff',  // Primary color
    colorBgContainer: '#000000',  // Background color
    colorText: '#ffffff',  // Text color
    // Add other customizations here
  },
};
