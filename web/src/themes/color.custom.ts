import { ColorSystemTypes } from "../types/ColorSystem.types";

type ColorType = "success" | "info" | "warning" | "error"

export const defaultColors: ColorSystemTypes = {
  success: '#52c41a',
  info: '#1890ff',
  warning: '#faad14',
  error: '#f5222d',
};

export const getColor: (color?: ColorType) => string = (color?: ColorType) => {
  return color ? defaultColors?.[color] : "#ffffff"
}