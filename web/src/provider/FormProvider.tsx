import { PropsWithChildren } from "react";
import { CustomFormProviderTypes } from "../types/CustomFormProvider.types";
import { FormContext } from "../context/FormContext";
import { FormDebugger } from "../shared/components/formDebugger/FormDebugger";

export const FormProvider = ({
  Form,
  form,
  initialValues,
  onFinish,
  onFinishFailed,
  children,
  debug
}: PropsWithChildren & CustomFormProviderTypes) => {
  return (
    <FormContext.Provider value={{
      Form,
      form,
      onFinish,
      onFinishFailed,
      initialValues,
      debug
    }}>
      <Form
        initialValues={initialValues}
        form={form}
        className="my-[50px] flex flex-col gap-[20px]"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        {children}
        {debug && <FormDebugger Form={Form} form={form} />}
      </Form>
    </FormContext.Provider>

  )
}