package org.mbds.tpt.View;

public interface ILoginView {
    void OnLoginSuccess(String message);
    void OnLoginError(String message);
    void showLoading();
    void hideLoading();
}

