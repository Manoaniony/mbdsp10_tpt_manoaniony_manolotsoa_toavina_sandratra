package org.mbds.tpt.Controller;

import android.content.Context;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.mbds.tpt.Model.Annonce;
import org.mbds.tpt.Model.Object;
import org.mbds.tpt.service.Api;
import org.mbds.tpt.service.AsyncResponse;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class AnnonceController {

    public interface AnnonceResponseListener {
        void onAnnoncesReceived(List<Object> obj);
        void onError(VolleyError error);
    }

    public void GetAllAnnonce(Context context, final AnnonceResponseListener listener) {
        Api.get(context, "/objets/getAll", true, new AsyncResponse() {
            @Override
            public void actionOnResponse(JSONObject output) throws JSONException {
                if (output.has("ok") && output.getBoolean("ok")) {
                    if (output.has("objects")) {
                        // Accède à l'objet "objects" et ensuite au tableau "content"
                        JSONObject objects = output.getJSONObject("objects");
                        if (objects.has("content")) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Object>>() {}.getType();
                            List<Object> obj = gson.fromJson(objects.getJSONArray("content").toString(), listType);
                            listener.onAnnoncesReceived(obj);
                        } else {
                            // Si "content" n'existe pas dans "objects", retourner une liste vide ou gérer l'erreur
                            listener.onAnnoncesReceived(Collections.emptyList());
                        }
                    } else {
                        // Si "objects" n'existe pas, retourner une liste vide ou gérer l'erreur
                        listener.onAnnoncesReceived(Collections.emptyList());
                    }
                }
            }

            @Override
            public void ERROR(VolleyError error) {
                listener.onError(error);
            }
        });
    }


}
