package org.mbds.tpt.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import org.mbds.tpt.Controller.LoginController;
import org.mbds.tpt.R;
import org.mbds.tpt.service.PrefsManager;

public class SignupActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PrefsManager prefsManager = new PrefsManager(this);
        String token = prefsManager.getToken();

        if (token != null && !token.isEmpty()) {
            // Si un token est trouvé, rediriger vers HomeActivity
            Intent intent = new Intent(SignupActivity.this, HomeActivity.class);
            startActivity(intent);
            finish(); // Fermer l'activité actuelle pour que l'utilisateur ne puisse pas revenir en arrière
            return;
        }

        setContentView(R.layout.activity_register);
    }
}
