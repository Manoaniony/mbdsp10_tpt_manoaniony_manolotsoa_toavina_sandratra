package org.mbds.tpt.service;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;

import org.mbds.tpt.Model.User;

public class PrefsManager {

    private static final String PREFS_NAME = "my_prefs";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_SERVER = "server";
    private static final String KEY_USER = "user";

    private SharedPreferences sharedPreferences;
    private Gson gson;
    public PrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public boolean clearPref() {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setToken(String token) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEY_TOKEN, token);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getToken() {
        try {
            return sharedPreferences.getString(KEY_TOKEN, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setServerStatus(String status) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEY_SERVER, status);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getServerStatus() {
        try {
            String value = sharedPreferences.getString(KEY_SERVER, null);
            return "1".equals(value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    // Setter for user
    public void setUser(String user) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEY_USER, user);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Getter for user
    public User getUser() {
        try {
            String userJson = sharedPreferences.getString(KEY_USER, null);
            if (userJson != null) {
                return gson.fromJson(userJson, User.class);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


