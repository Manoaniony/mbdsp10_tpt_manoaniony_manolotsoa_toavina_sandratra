package org.mbds.tpt.Model;

import android.text.TextUtils;
import android.util.Patterns;
public class Login implements ILogin{
    private final String userName;
    private final String password;
    public Login(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
    @Override
    public String getUsername() {
        return userName;
    }
    @Override
    public String getPassword() {
        return password;
    }
    @Override
    public int isValid() {
        // 0. Check for Email Empty
        // 1. Check for Email Match pattern
        // 2. Check for Password > 6
        if(TextUtils.isEmpty(getUsername()))
            return  0;
        else if(TextUtils.isEmpty(getPassword()))
            return 2;
        else if(getPassword().length()<=1)
            return 3;
        else
            return -1;
    }
}