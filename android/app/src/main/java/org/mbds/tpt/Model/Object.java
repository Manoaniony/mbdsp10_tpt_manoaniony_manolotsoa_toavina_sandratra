package org.mbds.tpt.Model;

public class Object {
    private Integer id;
    private String nom;
    private String description;
    private String categorie;
    private String etat;
    private String photo;
    private User user;
    private Annonce annonce;

    // Constructeur par défaut
    public Object() {
    }

    // Constructeur avec paramètres
    public Object(Integer id, String nom, String description, String categorie, String etat, String photo, Annonce annonce, User user) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.categorie = categorie;
        this.etat = etat;
        this.photo = photo;
        this.annonce = annonce;
        this.user = user;
    }


    // Getters et Setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Annonce getAnnonce() {
        return annonce;
    }

    public void setAnnonce(Annonce annonce) {
        this.annonce = annonce;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return nom;  // Affiche le nom de l'objet dans le Spinner
    }
}

