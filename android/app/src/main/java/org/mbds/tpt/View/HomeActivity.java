package org.mbds.tpt.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;

import org.mbds.tpt.R;
import org.mbds.tpt.databinding.ActivityBasicBinding;
import org.mbds.tpt.databinding.ActivityMainBinding;

import java.util.Hashtable;

public class HomeActivity extends AppCompatActivity {


    ActivityBasicBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBasicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        replaceFragment(new AnnonceFragment());

        Hashtable<Integer,Fragment> fragments = new Hashtable<Integer,Fragment>();
        fragments.put(R.id.annonce, new AnnonceFragment());
        fragments.put(R.id.publication, new PublicationFragment());
        fragments.put(R.id.match, new MatchFragment());
        fragments.put(R.id.profile, new ProfileFragment());

        binding.bottomNav.setOnItemSelectedListener(item -> {
            replaceFragment(fragments.get(item.getItemId()));
            return true;
        });

        //init();
        //initializeData();
    }

    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, fragment, null)
                .setReorderingAllowed(true)
                .commit();
    }


}