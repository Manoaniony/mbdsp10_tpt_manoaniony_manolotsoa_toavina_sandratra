package org.mbds.tpt.Controller;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;
import org.mbds.tpt.Model.Login;
import org.mbds.tpt.service.Api;
import org.mbds.tpt.service.AsyncResponse;
import org.mbds.tpt.service.PrefsManager;
import org.mbds.tpt.View.ILoginView;

public class LoginController implements ILoginController {
    ILoginView loginView;

    public LoginController(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void OnLogin(Context context, String userName, String password) {
        loginView.showLoading();
        Login user = new Login(userName, password);
        JSONObject req = new JSONObject();
        PrefsManager prefsManager = new PrefsManager(context);
        int loginCode = user.isValid();

        if (loginCode == 0) {
            loginView.hideLoading();
            loginView.OnLoginError("Please enter Email");
        } else if (loginCode == 2) {
            loginView.hideLoading();
            loginView.OnLoginError("Please enter Password");
        } else if (loginCode == 3) {
            loginView.hideLoading();
            loginView.OnLoginError("Please enter Password greater than 6 characters");
        } else {
            try {
                req.put("username", userName);
                req.put("password", password);

                Api.post(context, "/auth/login", req, false, new AsyncResponse() {
                    @Override
                    public void actionOnResponse(JSONObject response) throws JSONException {
                        loginView.hideLoading();
                        if (response.has("ok") && response.getBoolean("ok")) {
                            if (response.has("objects")) {
                                String token = response.getString("objects");
                                prefsManager.setToken(token);
                                Api.get(context, "/users/getByToken?param=" + response.getString("objects"), true, new AsyncResponse() {

                                    @Override
                                    public void actionOnResponse(JSONObject output) throws JSONException {
                                        if (output.has("ok") && output.getBoolean("ok")) {
                                            if (output.has("objects")) {
                                                prefsManager.setUser(output.getJSONObject("objects").toString());
                                                loginView.OnLoginSuccess("user info message : " + output.getString("message"));
                                            }
                                        }
                                    }

                                    @Override
                                    public void ERROR(VolleyError error) {
                                        loginView.OnLoginError("Get user Info failed: " + error.getMessage());
                                    }
                                });

                            } else {
                                loginView.OnLoginError("Login failed: No token received");
                            }
                        } else {
                            loginView.OnLoginError("Login failed: Invalid credentials or server error");
                        }
                    }

                    @Override
                    public void ERROR(VolleyError error) {
                        loginView.hideLoading();
                        loginView.OnLoginError("Login failed: Network error");
                    }
                });
            } catch (JSONException e) {
                loginView.hideLoading();
                e.printStackTrace();
                loginView.OnLoginError("Login failed: Error creating request");
            }
        }
    }
}
