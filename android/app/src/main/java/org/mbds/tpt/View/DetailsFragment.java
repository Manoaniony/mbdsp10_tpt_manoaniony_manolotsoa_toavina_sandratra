package org.mbds.tpt.View;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.mbds.tpt.Controller.AnnonceController;
import org.mbds.tpt.Controller.MatchController;
import org.mbds.tpt.Controller.ObjectController;
import org.mbds.tpt.Model.Match;
import org.mbds.tpt.Model.Object;
import org.mbds.tpt.R;

import java.util.ArrayList;
import java.util.List;

public class DetailsFragment extends Fragment {

    private ImageView detailsImage;
    private TextView detailsTitle, detailsCategoryBadge, detailsDescription, detailsState, detailsDate;
    private TextView latitudeValue, longitudeValue;
    private Spinner exchangeWithSpinner;
    private FusedLocationProviderClient fusedLocationClient;
    private List<Object> objectList = new ArrayList<>();
    private Button echangerButton;
    private Object currentObject; // Object that corresponds to the ID passed in arguments

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        // Initialize views
        detailsImage = view.findViewById(R.id.details_image);
        detailsTitle = view.findViewById(R.id.details_title);
        detailsCategoryBadge = view.findViewById(R.id.details_category_badge);
        detailsDescription = view.findViewById(R.id.details_description);
        detailsState = view.findViewById(R.id.details_state);
        detailsDate = view.findViewById(R.id.details_date);
        exchangeWithSpinner = view.findViewById(R.id.exchange_with_spinner);
        latitudeValue = view.findViewById(R.id.latitude_value);
        longitudeValue = view.findViewById(R.id.longitude_value);
        echangerButton = view.findViewById(R.id.echanger_button);
        // Initialize the location client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        // Get the object ID from the arguments
        int objectId = getArguments().getInt("object_id", -1);

        if (objectId != -1) {
            // Fetch the object details using the controller
            ObjectController objectController = new ObjectController();
            objectController.GetObjectByID(getContext(), objectId, new ObjectController.ObjectResponseListener() {
                @Override
                public void onObjectReceived(Object obj) {
                    if (obj != null) {
                        currentObject = obj; // Store the current object
                        // Populate the views with the object data
                        displayObjectDetails(currentObject);
                    } else {
                        Toast.makeText(getContext(), "Object details not found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(getContext(), "Error fetching details: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

            objectController.getObjectByToken(getContext(), new ObjectController.ObjectListResponseListener() {
                @Override
                public void onObjectListReceived(List<Object> objects) {
                    if (objects != null && !objects.isEmpty()) {
                        objectList.clear();
                        objectList.addAll(objects);
                        populateExchangeSpinner(); // Populate the spinner with user's objects
                    } else {
                        Toast.makeText(getContext(), "No objects found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(VolleyError error) {
                    Toast.makeText(getContext(), "Error fetching objects: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "Invalid object ID", Toast.LENGTH_SHORT).show();
        }

        // Get the last known location
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Request permissions if not granted
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            fusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        // Display the latitude and longitude
                        latitudeValue.setText(String.valueOf(location.getLatitude()));
                        longitudeValue.setText(String.valueOf(location.getLongitude()));
                    } else {
                        Toast.makeText(getContext(), "Unable to get location", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        echangerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the selected object to exchange with
                Object selectedObject = (Object) exchangeWithSpinner.getSelectedItem();

                // Create a Match object
                Match match = new Match();
                match.setObjet1(currentObject); // Current object
                match.setObjet2(selectedObject); // Selected object from spinner
                match.setLatitude(Double.parseDouble(latitudeValue.getText().toString()));
                match.setLongitude(Double.parseDouble(longitudeValue.getText().toString()));

                // Post the match using MatchController
                MatchController matchController = new MatchController();
                matchController.postMatch(getContext(), match, new MatchController.MatchResponseListener() {
                    @Override
                    public void onMatchPosted(Match mtch) {
                        Toast.makeText(getContext(), "Match posted successfully!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(VolleyError error) {
                        Toast.makeText(getContext(), "Failed to post match: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        return view;
    }





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted, try to get location again
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                // Display the latitude and longitude
                                latitudeValue.setText(String.valueOf(location.getLatitude()));
                                longitudeValue.setText(String.valueOf(location.getLongitude()));
                            } else {
                                Toast.makeText(getContext(), "Unable to get location", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                Toast.makeText(getContext(), "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void populateExchangeSpinner() {
        ArrayAdapter<Object> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, objectList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        exchangeWithSpinner.setAdapter(adapter);

        // Set an item selected listener to handle selection
        exchangeWithSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Retrieve the selected object
                Object selectedObject = (Object) exchangeWithSpinner.getSelectedItem();

                // Handle the selected object
                handleExchange(selectedObject);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle case when nothing is selected, if necessary
            }
        });
    }


    // Method to display details of the current object (from ID)
    private void displayObjectDetails(Object obj) {
        detailsTitle.setText(obj.getNom());
        detailsCategoryBadge.setText(obj.getCategorie());
        detailsDescription.setText(obj.getDescription());
        detailsState.setText(obj.getEtat());
        detailsDate.setText(obj.getAnnonce().getDatePublication());

        // Load image with Glide
        Glide.with(DetailsFragment.this).load(obj.getPhoto()).into(detailsImage);
    }

    // Handle the selected object for exchange
    private void handleExchange(Object selectedObject) {
        // You can store this object, send it to another fragment, etc.
        Toast.makeText(getContext(), "Selected for exchange: " + selectedObject.getNom(), Toast.LENGTH_SHORT).show();
        // Further logic for the exchange can be implemented here
    }
}
