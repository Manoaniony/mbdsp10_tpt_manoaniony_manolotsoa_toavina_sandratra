package org.mbds.tpt.Controller;

import android.content.Context;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mbds.tpt.Model.Match;
import org.mbds.tpt.Model.Object;
import org.mbds.tpt.Model.User;
import org.mbds.tpt.service.Api;
import org.mbds.tpt.service.AsyncResponse;
import org.mbds.tpt.service.PrefsManager;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ObjectController {

    public interface ObjectResponseListener {
        void onObjectReceived(Object obj);
        void onError(VolleyError error);
    }

    public interface ObjectListResponseListener {
        void onObjectListReceived(List<Object> objectList);
        void onError(VolleyError error);
    }

    public interface SaveObjectResponseListener {
        void onObjectReceived(Object obj);
        void onError(VolleyError error);
    }
    // Function to get a single object by ID
    public void GetObjectByID(Context context, int objectId, final ObjectResponseListener listener) {
        String url = "/objets/getById/" + objectId;  // Assuming the ID is passed as a query parameter

        Api.get(context, url, true, new AsyncResponse() {
            @Override
            public void actionOnResponse(JSONObject output) throws JSONException {
                if (output.has("ok") && output.getBoolean("ok")) {
                    if (output.has("objects")) {
                        JSONObject objectJson = output.getJSONObject("objects");

                        Gson gson = new Gson();
                        Object obj = gson.fromJson(objectJson.toString(), Object.class);

                        listener.onObjectReceived(obj);
                    } else {
                        listener.onObjectReceived(null);  // Or handle this case differently if necessary
                    }
                }
            }

            @Override
            public void ERROR(VolleyError error) {
                listener.onError(error);
            }
        });
    }

    // Function to get a list of objects by user token
    public void getObjectByToken(Context context, final ObjectListResponseListener listener) {
        String url = "/objets/getByUser/";
        PrefsManager prefsManager = new PrefsManager(context);
        User user = prefsManager.getUser();

        Api.get(context, url + user.getId(), true, new AsyncResponse() {
            @Override
            public void actionOnResponse(JSONObject output) throws JSONException {
                if (output.has("ok") && output.getBoolean("ok")) {
                    if (output.has("objects")) {
                        // Ici, nous supposons que "objects" est un JSONArray
                        JSONArray objectsJsonArray = output.getJSONArray("objects");

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<Object>>() {}.getType();

                        // Convertir le JSONArray en List<Object>
                        List<Object> objectList = gson.fromJson(objectsJsonArray.toString(), listType);

                        listener.onObjectListReceived(objectList);

                    } else {
                        listener.onObjectListReceived(Collections.emptyList());  // Si aucun objet n'est trouvé
                    }
                }
            }

            @Override
            public void ERROR(VolleyError error) {
                listener.onError(error);
            }
        });
    }

    public void postObject(Context context, Match match, final SaveObjectResponseListener listener) {

        // Create the JSON object from the Match model
        JSONObject matchJson = new JSONObject();

        try {

            matchJson.put("dateMatch", match.getDateMatch());
            matchJson.put("objet1", new JSONObject().put("id", match.getObjet1().getId())); // Assuming Object class has getId()
            matchJson.put("objet2", new JSONObject().put("id", match.getObjet2().getId())); // Assuming Object class has getId()
            matchJson.put("latitude", match.getLatitude());
            matchJson.put("longitude", match.getLongitude());
            Api.post(context, "/annonce/save", matchJson, true, new AsyncResponse() {
                @Override
                public void actionOnResponse(JSONObject response) throws JSONException {

                    if (response.has("ok") && response.getBoolean("ok")) {
                        if (response.has("objects")) {
                            Api.post(context, "/objet/save", matchJson, true, new AsyncResponse() {
                                @Override
                                public void actionOnResponse(JSONObject response) throws JSONException {

                                    if (response.has("ok") && response.getBoolean("ok")) {
                                        if (response.has("objects")) {

                                            JSONArray objectsJsonArray = response.getJSONArray("objects");

                                            Gson gson = new Gson();
                                            Type listType = new TypeToken<Object>() {}.getType();

                                            Object objectList = gson.fromJson(objectsJsonArray.toString(), listType);

                                            listener.onObjectReceived(objectList);

                                        } else {
                                            listener.onObjectReceived(null);
                                        }
                                    }
                                }

                                @Override
                                public void ERROR(VolleyError error) {
                                    listener.onError(error);
                                }
                            });


                        } else {
                            listener.onObjectReceived(null);
                        }
                    }
                }

                @Override
                public void ERROR(VolleyError error) {
                    listener.onError(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
