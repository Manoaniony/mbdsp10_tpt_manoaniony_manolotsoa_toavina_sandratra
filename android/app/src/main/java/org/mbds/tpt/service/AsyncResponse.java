package org.mbds.tpt.service;

import com.android.volley.VolleyError;
import org.json.JSONException;
import org.json.JSONObject;

public interface AsyncResponse {
    void actionOnResponse(JSONObject output) throws JSONException;
    void ERROR(VolleyError error);
}
