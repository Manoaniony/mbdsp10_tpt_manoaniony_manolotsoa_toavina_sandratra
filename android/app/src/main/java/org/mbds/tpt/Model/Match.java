package org.mbds.tpt.Model;

public class Match {

    private Integer id;
    private String dateMatch;
    private Object objet1;
    private Object objet2;
    private Double latitude;
    private Double longitude;

    public Match(Integer id, String dateMatch, Object objet1, Object objet2, Double latitude, Double longitude) {
        this.id = id;
        this.dateMatch = dateMatch;
        this.objet1 = objet1;
        this.objet2 = objet2;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Match() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(String dateMatch) {
        this.dateMatch = dateMatch;
    }

    public Object getObjet1() {
        return objet1;
    }

    public void setObjet1(Object objet1) {
        this.objet1 = objet1;
    }

    public Object getObjet2() {
        return objet2;
    }

    public void setObjet2(Object objet2) {
        this.objet2 = objet2;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
