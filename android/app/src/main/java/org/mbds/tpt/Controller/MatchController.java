package org.mbds.tpt.Controller;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.mbds.tpt.Model.Match;
import org.mbds.tpt.service.Api;
import org.mbds.tpt.service.AsyncResponse;

public class MatchController {

    public interface MatchResponseListener {
        void onMatchPosted(Match mtch);

        void onError(VolleyError error);
    }


    public void postMatch(Context context, Match match, final MatchResponseListener listener) {

        // Create the JSON object from the Match model
        JSONObject matchJson = new JSONObject();

        try {

            matchJson.put("dateMatch", match.getDateMatch());
            matchJson.put("objet1", new JSONObject().put("id", match.getObjet1().getId())); // Assuming Object class has getId()
            matchJson.put("objet2", new JSONObject().put("id", match.getObjet2().getId())); // Assuming Object class has getId()
            matchJson.put("latitude", match.getLatitude());
            matchJson.put("longitude", match.getLongitude());
            Api.post(context, "/match/save", matchJson, true, new AsyncResponse() {
                @Override
                public void actionOnResponse(JSONObject response) throws JSONException {

                    if (response.has("ok") && response.getBoolean("ok")) {
                        if (response.has("objects")) {
                            JSONObject objectJson = response.getJSONObject("objects");

                            Gson gson = new Gson();
                            Match mtch = gson.fromJson(objectJson.toString(), Match.class);

                            listener.onMatchPosted(mtch);
                        } else {
                            listener.onMatchPosted(null);
                        }
                    }
                }

                @Override
                public void ERROR(VolleyError error) {
                    listener.onError(error);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
