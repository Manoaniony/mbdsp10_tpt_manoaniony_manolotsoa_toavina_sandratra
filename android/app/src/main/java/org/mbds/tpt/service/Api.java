package org.mbds.tpt.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public final class Api {
    private static RequestQueue request_queue;
    private static final String BASE_URL = "https://tpt-backend-java.onrender.com/api";

    private Api() {
        super();
    }

    public static void get(Context context, String url, boolean tokenRequired, final AsyncResponse asyncResponse) {
        url = BASE_URL + url;
        request_queue = Volley.newRequestQueue(context);
        JsonObjectRequest request;

        if (tokenRequired) {
            PrefsManager prefsManager = new PrefsManager(context);
            String token = prefsManager.getToken();
            request = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    if (token != null) {
                        headers.put("Authorization", "Bearer " + token);
                    }
                    return headers;
                }
            };
        } else {
            request = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            );
        }

        request_queue.add(request);
    }

    public static void post(Context context, String url, JSONObject object, boolean tokenRequired, final AsyncResponse asyncResponse) {
        url = BASE_URL + url;
        request_queue = Volley.newRequestQueue(context);
        JsonObjectRequest request;

        if (tokenRequired) {
            PrefsManager prefsManager = new PrefsManager(context);
            String token = prefsManager.getToken();
            request = new JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    object,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    if (token != null) {
                        headers.put("Authorization", "Bearer " + token);
                    }
                    return headers;
                }
            };
        } else {
            request = new JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    object,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            );
        }

        request_queue.add(request);
    }

    public static void upload(Context context, String url, JSONObject object, boolean tokenRequired, final AsyncResponse asyncResponse) {
        url = BASE_URL + url;
        request_queue = Volley.newRequestQueue(context);
        JsonObjectRequest request;

        if (tokenRequired) {
            PrefsManager prefsManager = new PrefsManager(context);
            String token = prefsManager.getToken();
            request = new JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    object,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    if (token != null) {
                        headers.put("Authorization", "Bearer " + token);
                        headers.put("Content-Type", "multipart/form-data");
                    }
                    return headers;
                }
            };
        } else {
            request = new JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    object,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                asyncResponse.actionOnResponse(response);
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            asyncResponse.ERROR(error);
                        }
                    }
            );
        }

        request_queue.add(request);
    }
}
