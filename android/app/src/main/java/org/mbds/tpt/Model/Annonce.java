package org.mbds.tpt.Model;

import java.util.List;

public class Annonce {
    private Integer id;
    private String descs;
    private String photo;
    private String etat;

    private String libelle;
    private String datePublication;
    private User user;
//    private List<Object> objets;

    // Constructeur par défaut
    public Annonce(String libelle) {
        this.libelle = libelle;
    }

    // Constructeur avec paramètres
    public Annonce(Integer id, String descs, String photos, String etat, String libelle, String datePublication, User user) {
        this.id = id;
        this.descs = descs;
        this.photo = photos;
        this.etat = etat;
        this.libelle = libelle;
        this.datePublication = datePublication;
        this.user = user;
//        this.objets = objets;
    }

    // Getters et Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescs() {
        return descs;
    }

    public void setDescs(String descs) {
        this.descs = descs;
    }

    public String getPhotos() {
        return photo;
    }

    public void setPhotos(String photos) {
        this.photo = photo;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    //    public List<Object> getObjets() {
//        return objets;
//    }
//
//    public void setObjets(List<Object> objets) {
//        this.objets = objets;
//    }

    @Override
    public String toString() {
        return "AnnoncementType{" +
                "id=" + id +
                ", descs='" + descs + '\'' +
                ", photos='" + photo + '\'' +
                ", etat='" + etat + '\'' +
                ", datePublication='" + datePublication + '\'' +
                ", user=" + user +
                '}';
    }
}
