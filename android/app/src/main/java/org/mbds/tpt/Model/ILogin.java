package org.mbds.tpt.Model;

public interface ILogin {

    String getUsername();
    String getPassword();
    int isValid();
}