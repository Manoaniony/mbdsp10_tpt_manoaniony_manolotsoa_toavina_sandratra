package org.mbds.tpt.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.mbds.tpt.Controller.ILoginController;
import org.mbds.tpt.Controller.LoginController;
import org.mbds.tpt.R;
import org.mbds.tpt.service.PrefsManager;

public class MainActivity extends AppCompatActivity implements ILoginView {
    EditText email, password;
    Button loginb, signup_redirection_button;
    ProgressBar progressBar;
    ILoginController loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PrefsManager prefsManager = new PrefsManager(this);
        String token = prefsManager.getToken();

        if (token != null && !token.isEmpty()) {
            // Redirect to HomeActivity if token exists
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_main);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        loginb = findViewById(R.id.loginb);
        signup_redirection_button = findViewById(R.id.signupb);
        progressBar = findViewById(R.id.progressBar);
        loginPresenter = new LoginController(this);

        loginb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.OnLogin(MainActivity.this, email.getText().toString().trim(), password.getText().toString().trim());
            }
        });

        signup_redirection_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void OnLoginSuccess(String message) {
        hideLoading();
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        startActivity(intent);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void OnLoginError(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }
}
