package org.mbds.tpt.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.mbds.tpt.Model.Object;
import org.mbds.tpt.R;

import java.util.List;

public class AnnonceAdapter extends RecyclerView.Adapter<AnnonceAdapter.AnnonceViewHolder> {

    private Context context;
    private List<Object> obj;

    public AnnonceAdapter(Context context, List<Object> obj) {
        this.context = context;
        this.obj = obj;
    }

    // Method to update the list of annonces
    public void updateObject(List<Object> newobject) {
        this.obj = newobject;
        notifyDataSetChanged();  // Notify the adapter that the data set has changed
    }

    @NonNull
    @Override
    public AnnonceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_annonce, parent, false);
        return new AnnonceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnonceViewHolder holder, int position) {
        Object objs = obj.get(position);
        holder.title.setText(objs.getNom() != null ? objs.getNom() : "");
        holder.description.setText(objs.getDescription() != null ? objs.getDescription() : "");
        holder.state.setText(objs.getEtat() != null ? objs.getEtat() : "");
        holder.category.setText(objs.getCategorie() != null ? objs.getCategorie() : "");
        holder.date.setText(objs.getAnnonce().getDatePublication() != null ? objs.getAnnonce().getDatePublication() : "");

        Glide.with(context)
                .load(objs.getPhoto())
                .placeholder(R.drawable.lamp)
                .error(R.drawable.ic_launcher_background)
                .into(holder.image);

        // Set the onClickListener for the arrow ImageView
        // Inside AnnonceAdapter
        holder.arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailsFragment detailFragment = new DetailsFragment();

                // Pass the object ID as an argument to the DetailFragment
                Bundle args = new Bundle();
                args.putInt("object_id", objs.getId());  // Assuming objs has a method getId()
                detailFragment.setArguments(args);

                // Replace the current fragment with the DetailFragment
                FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_container, detailFragment)
                        .addToBackStack(null)  // Add the transaction to the back stack so the user can navigate back
                        .commit();

            }
        });

    }


    @Override
    public int getItemCount() {
        return obj != null ? obj.size() : 0;
    }

    public static class AnnonceViewHolder extends RecyclerView.ViewHolder {

        TextView title, description, state, category, date;
        ImageView image, arrow;

        public AnnonceViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            description = itemView.findViewById(R.id.item_description);
            state = itemView.findViewById(R.id.item_state);
            category = itemView.findViewById(R.id.item_category_badge);
            date = itemView.findViewById(R.id.item_date);

            image = itemView.findViewById(R.id.item_image);
            arrow = itemView.findViewById(R.id.item_navigation_arrow); // Reference to the arrow ImageView
        }
    }

}
