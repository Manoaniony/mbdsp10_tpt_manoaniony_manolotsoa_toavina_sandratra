package org.mbds.tpt.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import org.mbds.tpt.Controller.AnnonceController;
import org.mbds.tpt.Controller.ObjectController;
import org.mbds.tpt.Model.Object;
import org.mbds.tpt.R;

import java.util.List;

public class AnnonceFragment extends Fragment {

    private AnnonceAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_annonce, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new AnnonceAdapter(getContext(), null);
        recyclerView.setAdapter(adapter);

        loadAnnonces();

        return view;
    }

    private void loadAnnonces() {
        AnnonceController annonceController = new AnnonceController();
        annonceController.GetAllAnnonce(getContext(), new AnnonceController.AnnonceResponseListener() {
            @Override
            public void onAnnoncesReceived(List<Object> obj) {
                adapter.updateObject(obj);
            }

            @Override
            public void onError(VolleyError error) {
                Toast.makeText(getContext(), "Failed to load annonces", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
