
let baseURL = "https://tpt-backend-java.onrender.com";
export const urls = {
  baseURL: baseURL,
  login: `${baseURL}/api/auth/login`,
  signup: `${baseURL}/api/users/save`,
  getInfo: `${baseURL}/api/users/getByToken?param=`,
  saveAnnoncement: `${baseURL}/api/annonces/save`,
  saveObject: `${baseURL}/api/objets/save`,
  getObjectByUser: `${baseURL}/api/objets/getByUser`,
  getObjectById: `${baseURL}/api/objets/getById`,
  getAllObjects: `${baseURL}/api/objets/getAll`,
  getAllAnnoncement : `${baseURL}/api/annonces/getAll?page=0&size=10&sort=datePublication,desc`,
  getAnnoncementById : `${baseURL}/api/annonces/getById`,
  saveMatch: `${baseURL}/api/match/save`,
  getAllMatch: `${baseURL}/api/match/getAll`,
  getMatchById: `${baseURL}/api/match/getById`,
  uploadImg: `https://mbdsp10-tpt-manoaniony-manolotsoa.onrender.com/api/upload_files`
};
