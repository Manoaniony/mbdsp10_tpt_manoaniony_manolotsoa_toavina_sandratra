export enum announcementStatus {
    WAITING  = "En Attente",
    RESERVE = "Reserver",
    CONFIRMED = "Confirmé",
}