export enum announcementCategories {
    deco = "Decoration",
    livre = "Livre",
    technologie = "Technologie",
    mode = "Mode",
    alimentation = "Alimentation",
    sport = "Sport",
    voyage = "Voyage",
    immobilier = "Immobilier",
    art = "Art",
    musique = "Musique",
}
