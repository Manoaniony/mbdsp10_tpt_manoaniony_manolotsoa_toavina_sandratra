import { annoncementType } from "./annonce.type"


export type annoncementsType = {
	totalPages : number
    content : annoncementType[]
    totalElements: number
}