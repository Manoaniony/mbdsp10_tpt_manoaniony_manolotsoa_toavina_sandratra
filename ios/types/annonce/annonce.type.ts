import { objectType } from "../object/object.type";
import { userType } from "../user/user.types";

export type annoncementType = {
  id?: number;
  libelle?: string;
  descs?: string;
  photo?: string;
  etat?: string;
  datePublication?: string;
  user?: userType;
  // objets? : objectType[]
};
