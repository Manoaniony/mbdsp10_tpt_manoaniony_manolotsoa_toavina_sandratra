import { userType } from "../user/user.types"

export type saveAnnonceType = {
	libelle: string,
	descs : string,
	photo : string,
	etat : string,
	datePublication : string,
	user : userIdType
}

export type userIdType = {
    id: number
}

