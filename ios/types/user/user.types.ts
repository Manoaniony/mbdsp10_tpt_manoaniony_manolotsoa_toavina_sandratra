export type userType = {
  id?: number,
  authorities?: authoritie[],
  password?: string,
  username?: string,
  accountNonExpired?: boolean,
  accountNonLocked?: boolean,
  credentialsNonExpired?: boolean,
  enabled?: boolean,
  adresse?: string,
  tel?: string,
  email?: string,
  nom?: string,
  pointsDeRecuperation?: string
}


export type authoritie = {
  id?: number;
  authority?: string;
}