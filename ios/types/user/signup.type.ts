export type signUpType = {
	password: string,
	username: string,
	accountNonExpired: boolean,
	accountNonLocked: boolean,
	credentialsNonExpired: boolean,
	enabled: boolean,
	adresse?: string,
	tel?: string,
	email: string,
	nom: string
}