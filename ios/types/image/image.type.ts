export type imageType = {
    files: Blob
}

export type imageResponse = {
    url: string,
    id: string
}