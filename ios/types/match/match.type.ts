import { objectType } from "../object/object.type";

export type matchType = {
    id?: string;
    dateMatch : string
    objet1 : objectType
    objet2 : objectType
    latitude: number,
    longitude: number
}