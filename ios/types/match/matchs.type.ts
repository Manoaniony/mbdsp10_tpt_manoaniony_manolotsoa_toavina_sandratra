import { matchType } from "./match.type"

export type matchsType = {
	totalPages : number
    content : matchType[]
    totalElements: number
}