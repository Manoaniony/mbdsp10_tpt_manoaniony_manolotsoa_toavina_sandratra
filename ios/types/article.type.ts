export type ArticleType = {
    id: number,
    name: string,
    description?: string,
    image?: string,
    location?: string,
    preferences?: string[],
    price?: number,
}
