

export type BaseResponseType<T> = {
    ok: boolean,
    status?: number,
    message?: string;
    objects?: T
}

export enum StatusCode {
    SUCCESS = 200,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
    PARTIAL_SUCCESS = 207,
}

export enum StatusMessage {
    SUCCESS = 'SUCCESS',
    BAD_REQUEST = 'BAD REQUEST',
    UNAUTHORIZED = 'UNAUTHORIZED',
    NOT_FOUND = 'NOT FOUND',
    INTERNAL_SERVER_ERROR = 'INTERNAL SERVER ERROR',
    VALIDATION_ERROR = 'VALIDATION ERROR',
    PARTIAL_SUCCESS = 'PARTIAL SUCCESS',
}

export const BaseResponse = <T>(ok: boolean, status?: number, data?: T, message?: string): BaseResponseType<T> => {
    
    const res: BaseResponseType<T> = {
        ok: ok,
        status: status,
        message: message,
        objects: data,
    }
    return res
}