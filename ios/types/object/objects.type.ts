import { objectType } from "./object.type"

export type objectsType = {
	totalPages : number
    content : objectType[]
    totalElements: number
}