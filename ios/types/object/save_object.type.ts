import { userIdType } from "../annonce/save_annonce.type"

export type saveObjectType = {
	nom: String,
    description: String,
    categorie: String
    etat: String
    photo: String
    annonce: annonceIdType
    user: userIdType
}

export type annonceIdType = {
    id: number
}