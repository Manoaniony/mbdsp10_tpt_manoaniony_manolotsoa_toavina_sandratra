import { annoncementType } from "../annonce/annonce.type"
import { userType } from "../user/user.types"

export type objectType = {
    id?: number
	nom?: string,
    description?: string,
    categorie?: string
    etat?: string
    photo?: string
    annonce? : annoncementType
    user? : userType
}