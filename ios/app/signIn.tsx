import { useSession } from '@/services/context/authContext';
import { View, Text, Alert } from 'react-native';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import { router } from 'expo-router';
import CustomButton from '@/components/CustomButton';
import { styled } from 'nativewind';
import FormInput from '@/components/form/FormInput';
import { AuthenticationService } from '@/services/api/authentication/authentication.service';
import { LoginType } from '@/types/user/login.type';
import { useContext } from 'react';
import { GlobalContext } from '@/services/context/globalContext';
import { Snackbar } from 'react-native-paper';
import BaseScreen from '@/components/base/base.screen';

const formSchema = z.object({
    username: z.string().min(1, 'This field is required'),
    password: z.string().min(2, 'Password must be at least 8 characters'),
});

type FormSchemaType = z.infer<typeof formSchema>;

const StyledView = styled(View);
const StyledText = styled(Text);

export default function SignIn() {
    const { signIn } = useSession();

    const { setLoading, setShowSnackbar } = useContext(GlobalContext);

    const { control, handleSubmit } = useForm<FormSchemaType>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            username: "",
            password: "",
        },
    });

    const onSubmit = async (values: FormSchemaType) => {
        const { username, password } = values;
        const req: LoginType = {
            username: username,
            password: password
        }

        setLoading(true)
        try {
            const res = await signIn(req)

            if (res.ok) {
                setShowSnackbar({ text: "Sign in successful", visible: true });
                setLoading(false)
                router.replace("/(app)")
            } else {
                setLoading(false)
                setShowSnackbar({ text: res.message ?? "Something went wrong", visible: true });
            }
        } catch (error) {
            console.log("Error occured : " , error)
            setLoading(false)
            setShowSnackbar({ text: "Something went wrong", visible: true });
        }
    };

    return (
        <BaseScreen>
            <StyledView className="flex-1 items-center justify-center bg-white p-4">
                <View className="border border-gray-300 rounded-lg p-6 bg-white shadow-md w-full max-w-sm">
                    <StyledText className="text-2xl font-bold text-center">Se connecter</StyledText>
                    <StyledView className='mb-5'>
                        <FormInput<FormSchemaType>
                            control={control}
                            name="username"
                            placeholder="User name"
                            label="User name"
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="password"
                            placeholder="Password"
                            label="Mot de passe"
                            secureTextEntry
                        />
                    </StyledView>
                    <CustomButton
                        onPress={handleSubmit(onSubmit)}
                        title="Se Connecter"
                        titleClassName='font-bold'
                    />
                    <CustomButton
                        onPress={() => {
                            router.navigate("/signUp")
                        }}
                        title="Créer un compte"
                        buttonClassName='bg-green-500'
                        titleClassName='font-bold'
                    />
                </View>
            </StyledView>
        </BaseScreen>
    );
}
