import { SessionProvider, useSession } from '@/services/context/authContext';
import { GlobalContext, GlobalProvider, useGlobal } from '@/services/context/globalContext';
import { Redirect, Slot, Stack } from 'expo-router';
import { useContext, useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import { Snackbar } from 'react-native-paper';

export default function Root() {
  const { session, isLoading } = useSession();
  const { showSnackbar, setShowSnackbar } = useGlobal();

  if (session) {
    return <Redirect href="/(app)" />;
  }

  return (
    <SessionProvider>
      <GlobalProvider>
        <SafeAreaView className="flex-1">
          <Stack>
            <Stack.Screen name="signIn" options={{ headerShown: false }} />
            <Stack.Screen name="signUp" options={{ headerShown: false }} />
            <Stack.Screen name="(app)" options={{ headerShown: false }} />
          </Stack>
        </SafeAreaView>
      </GlobalProvider>
    </SessionProvider>
  );
}
