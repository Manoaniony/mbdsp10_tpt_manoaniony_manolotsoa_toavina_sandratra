import React, { useContext } from 'react';
import { useSession } from '@/services/context/authContext';
import { View, Text, Alert, ScrollView } from 'react-native';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';
import CustomButton from '@/components/CustomButton';
import { styled } from 'nativewind';
import FormInput from '@/components/form/FormInput';
import { signUpType } from '@/types/user/signup.type';
import { AuthenticationService } from '@/services/api/authentication/authentication.service';
import { GlobalContext } from '@/services/context/globalContext';
import { router } from 'expo-router';
import BaseScreen from '@/components/base/base.screen';

const formSchema = z.object({
    fullName: z.string().min(1, "This field is required"),
    userName: z.string().min(1, "This field is required"),
    adress: z.string().nullable(),
    telephone: z.string().nullable(),
    email: z.string().email('Please enter a valid email'),
    password: z
        .string()
        .min(8, { message: "Password must contain more than 8 characters" }),
    confirmPassword: z.string().min(8, {
        message: "Password must contain more than 8 characters",
    }),
}).superRefine(({ confirmPassword, password }, ctx) => {
    if (confirmPassword !== password) {
        ctx.addIssue({
            code: "custom",
            message: "The passwords did not match",
        });
    }
});

type FormSchemaType = z.infer<typeof formSchema>;

const StyledView = styled(View);
const StyledText = styled(Text);

export default function SignUp() {
    const {setShowSnackbar, setLoading } = useContext(GlobalContext);

    const { control, handleSubmit } = useForm<FormSchemaType>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            fullName: '',
            userName: '',
            adress: '',
            telephone: "",
            email: "",
            password: "",
        },
    });

    const { signup } = AuthenticationService()
    const onSubmit = async (values: FormSchemaType) => {
        const { email, password, userName, fullName, adress, telephone} = values;

        const req: signUpType = {
            ...values,
            password: password,
            username: userName,
            accountNonExpired: true,
            accountNonLocked: true,
            credentialsNonExpired: true,
            enabled: true,
            tel: telephone ?? "",
            nom: fullName
        }

        setLoading(true)
        try {
            const res = await signup(req)

            if (res.ok) {
                setShowSnackbar({ text: "Registration successful", visible: true });
                setLoading(false)
                router.navigate("/signIn")
            } else {
                setLoading(false)
                setShowSnackbar({ text: res.message ?? "Something went wrong", visible: true });
            }
        } catch (error) {
            console.log("Error occured : " , error)
            setLoading(false)
            setShowSnackbar({ text: "Something went wrong", visible: true });
        }
    };

    return (
        <BaseScreen>
            <StyledView className="flex-1 items-center justify-center bg-white p-4 pt-20">
            <ScrollView 
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={{ width: '100%' }}
            >
                <View className="border border-gray-300 rounded-lg p-6 bg-white shadow-md w-full max-w-sm mx-auto">
                    <StyledText className="text-2xl font-bold text-center mb-4">S'inscrire</StyledText>
                    <StyledView className='mb-5'>
                        <FormInput<FormSchemaType>
                            control={control}
                            name="fullName"
                            placeholder="Nom et prénom"
                            label="Nom et prénom"
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="userName"
                            placeholder="Nom d'utilisateur"
                            label="Nom d'utilisateur"
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="adress"
                            placeholder="Adresse"
                            label="Adresse"
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="telephone"
                            placeholder="Téléphone"
                            label="Téléphone"
                            keyboardType='phone-pad'
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="email"
                            placeholder="Email"
                            label="Email"
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="password"
                            placeholder="Mot de passe"
                            label="Mot de passe"
                            secureTextEntry
                        />
                        <FormInput<FormSchemaType>
                            control={control}
                            name="confirmPassword"
                            placeholder="Confirmer le mot de passe"
                            label="Confirmer le mot de passe"
                            secureTextEntry
                        />
                    </StyledView>
                    <CustomButton
                        onPress={handleSubmit(onSubmit)}
                        title="S'inscrire"
                        titleClassName='font-bold'
                    />
                    <CustomButton
                        onPress={() => {
                            router.navigate("/signIn") 
                        }}
                        title="J'ai deja un compte"
                        titleClassName='font-bold'
                        outline={true}
                    />
                </View>
            </ScrollView>
        </StyledView>
        </BaseScreen>
    );
}
