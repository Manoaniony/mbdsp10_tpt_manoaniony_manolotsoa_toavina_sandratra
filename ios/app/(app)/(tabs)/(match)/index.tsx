import MatchCard from "@/components/card/match.card"
import { MatchService } from "@/services/api/match/match.service"
import { matchType } from "@/types/match/match.type"
import { matchsType } from "@/types/match/matchs.type"
import { useEffect, useState } from "react"
import { View , Text, ScrollView, RefreshControl} from "react-native"



const MatchScreen = () => {

    const [matchs, setMatchs] = useState<matchsType>()
    const [refreshing, setRefreshing] = useState(false);

    const {getAllMatch} = MatchService()

    const getMatchList = async () => {
        try {
            const res = await getAllMatch()
            if(res.ok && res.objects) {
                setMatchs(res.objects)
            }
        } catch (error) {
            console.log("Error occured : " , error)
        }
    }

    const refresh = async () => {
        setRefreshing(true)
        try {
            const res = await getAllMatch()
            if(res.ok && res.objects) {
                setMatchs(res.objects)
            }
        } catch (error) {
            console.log("Error occured : " , error)
        }finally{
            setRefreshing(false)
        }
    }

    useEffect(() => {
        getMatchList()
    }, [])

    return (
        <View className="flex-1">
            <View className="mx-5 my-10 mb-50">
                <Text className="font-bold text-[30px] pb-3">Vos Match</Text>            
                <ScrollView 
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={refresh}
                        />
                    }
                >
                    <View className="mb-20">
                        {
                            matchs && matchs.content.map((match, index) => {
                                return(
                                    <MatchCard key={index} match={match} />
                                )
                            })
                        }
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default MatchScreen