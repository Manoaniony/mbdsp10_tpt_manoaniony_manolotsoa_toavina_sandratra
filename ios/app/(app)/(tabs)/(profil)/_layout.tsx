import { Ionicons } from '@expo/vector-icons';
import { router, Stack } from 'expo-router';
import { TouchableOpacity } from 'react-native';

export default function ProfileLayout() {
  return (
    <Stack 
    screenOptions={{
      headerShown: true,
      headerRight: () => (
        <TouchableOpacity onPress={()=> {
          router.push("/scan")
        }}>
          <Ionicons name="scan" size={30} color="black" style={{ marginRight: 10 }} />
        </TouchableOpacity>
      ),
    }}
    >
      <Stack.Screen name="index" options={{ headerShown: true, title: "Profil" }} />
      {/* <Stack.Screen name="objet/[id]" options={{ title: "Details" }}/> */}
    </Stack>
  );
}
