import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';
import { ObjectService } from "@/services/api/object/object.service";
import { getUser, clearPref } from "@/services/storage/storage";
import { objectType } from "@/types/object/object.type";
import { useGlobal } from "@/services/context/globalContext";
import { useRouter } from "expo-router";
import { userType } from "@/types/user/user.types";
import AnnoucementCard from "@/components/card/announcement.card";
import QRCode from 'react-native-qrcode-svg';

export const ProfileScreen = () => {
    const [objects, setObjects] = useState<objectType[]>([]);
    const [user, setUser] = useState<userType | null>(null);

    const { getObjectByUser } = ObjectService();
    const { setShowSnackbar } = useGlobal();
    const router = useRouter();

    const getObjects = async () => {
        const user = await getUser();
        if (user) {
            setUser(user);
            let response = await getObjectByUser(user.id!);
            if (response.ok && response.objects) {
                setObjects(response.objects);
            }
        }
    };

    const handleLogout = async () => {
        await clearPref();
        setShowSnackbar({ text: "Déconnexion réussie", visible: true });
        router.push("/signIn");
    };

    useEffect(() => {
        getObjects();
    }, []);

    if(!user) {
        <View></View>
    }

    return (
        <View className="flex-1 bg-gray-100 p-4">
            <View className="flex-row items-center mb-8">
                <Ionicons name="person-circle-outline" size={80} color="black" />
                <View className="ml-4 mr-20">
                    <Text className="text-2xl font-semibold w-50">{user?.nom}</Text>
                    <Text className="text-xs font-semibold text-gray-500 w-50">{user?.username}</Text>
                    <Text className="text-xs font-semibold text-gray-500 w-50">{user?.email}</Text>
                </View>
                {
                    user?.email && 
                    <QRCode
                        value={user.email}
                        size={60}
                    />
                }
            </View>
            <View className="border border-gray-300 p-2 rounded-lg">
                <Text className="ml-4 text-m text-center font-semibold">Vos publications</Text>
            </View>
            <ScrollView className="mb-20">
                {objects.map((object, index) => (
                    <View key={index}>
                        <AnnoucementCard object={object} />
                    </View>
                ))}
            </ScrollView>
            <View className="justify-end ">
                <TouchableOpacity 
                    onPress={handleLogout} 
                    className="bg-red-500 rounded-lg p-4"
                >
                    <Text className="text-white text-center text-lg font-bold">Se déconnecter</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default ProfileScreen;
