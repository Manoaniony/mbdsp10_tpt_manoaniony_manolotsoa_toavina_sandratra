import React, { useEffect, useState } from "react";
import { View, Text, Image, ScrollView, ActivityIndicator, Alert } from "react-native";
import Ionicons from '@expo/vector-icons/Ionicons';
import CustomButton from "@/components/CustomButton";
import FormDropdown from "@/components/form/FormDropdown";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ObjectService } from "@/services/api/object/object.service";
import { objectType } from "@/types/object/object.type";
import { getUser } from "@/services/storage/storage";
import { matchType } from "@/types/match/match.type";
import { MatchService } from "@/services/api/match/match.service";
import { useGlobal } from "@/services/context/globalContext";
import MapView, { Marker, MapPressEvent, Region } from 'react-native-maps';
import { useLocalSearchParams } from "expo-router";
import * as Location from 'expo-location';

const formSchema = z.object({
  objet: z.number().min(1, "Ce champ est obligatoire"),
  latitude: z.number().min(-90, "Latitude invalide").max(90, "Latitude invalide"),
  longitude: z.number().min(-180, "Longitude invalide").max(180, "Longitude invalide"),
});

type FormSchemaType = z.infer<typeof formSchema>;

const ArticleDetailsScreen = () => {
  const [object, setObject] = useState<objectType>();
  const [objectsUser, setObjectsUser] = useState<objectType[]>();
  const [markerCoords, setMarkerCoords] = useState<{ lat: number; long: number } | null>(null);
  const [mapRegion, setMapRegion] = useState<Region>({
    latitude: -18.8792,        // Latitude pour Antananarivo
    longitude: 47.5079,        // Longitude pour Antananarivo
    latitudeDelta: 0.05,
    longitudeDelta: 0.05,
  });
  const [loadingLocation, setLoadingLocation] = useState<boolean>(false);

  const { id } = useLocalSearchParams();

  const { setLoading, setShowSnackbar } = useGlobal();

  const { getObjectById , getObjectByUser} = ObjectService();
  const { saveMatch } = MatchService();

  const { control, handleSubmit, setValue, watch, formState: { errors } } = useForm<FormSchemaType>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      objet: 0,
      latitude: -18.8792,   // Valeurs par défaut pour Antananarivo
      longitude: 47.5079,
    },
  });

  const getObject = async () => {
    try {
      setLoading(true);
      const user = await getUser();

      if (typeof id === "string") {
        const [res, resUser] = await Promise.all([
          getObjectById(parseInt(id)),
          getObjectByUser(user?.id!),
        ]);

        if (res.ok && res.objects) {
          setObject(res.objects);
        }

        if (resUser.ok && resUser.objects) {
          setObjectsUser(resUser.objects);
        }
      }
    } catch (error) {
      console.log("Error occurred on get details: ", error);
      setShowSnackbar({ text: "Erreur lors du chargement des données.", visible: true });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getObject();
  }, []);

  const onSubmit = async (value: FormSchemaType) => {
    try {
      setLoading(true);
      if (!markerCoords) {
        setShowSnackbar({ text: "Veuillez sélectionner un point de récupération sur la carte.", visible: true });
        return;
      }

      const resObj = await getObjectById(value.objet);
      if (resObj.ok && resObj.objects) {
        const matchBody: matchType = {
          dateMatch: new Date().toISOString().split('T')[0],
          objet1: object!,
          objet2: resObj.objects,
          latitude: value.latitude,
          longitude: value.longitude,
        };

        const res = await saveMatch(matchBody);

        if(res.ok) {
          setShowSnackbar({ text: "Vous avez un nouveau match !", visible: true });
        } else {
          setShowSnackbar({ text: res.message ?? "Une erreur est survenue", visible: true });
        }
      }
    } catch (error) {
      setShowSnackbar({ text: "Une erreur est survenue", visible: true });
      console.log("Error occurred: ", error);
    } finally{
      setLoading(false);
    }
  };

  const handleMapPress = (e: MapPressEvent) => {
    const { latitude, longitude } = e.nativeEvent.coordinate;
    setMarkerCoords({ lat: latitude, long: longitude });
    setValue("latitude", latitude);
    setValue("longitude", longitude);
  };

  const getCurrentLocation = async () => {
    try {
      setLoadingLocation(true);
      const { status } = await Location.requestForegroundPermissionsAsync();

      if (status !== 'granted') {
        Alert.alert('Permission refusée', 'La permission de localisation est nécessaire pour obtenir votre position actuelle.');
        return;
      }

      const location = await Location.getCurrentPositionAsync({});
      const { latitude, longitude } = location.coords;

      setMapRegion({
        ...mapRegion,
        latitude,
        longitude,
      });

      setMarkerCoords({ lat: latitude, long: longitude });
      setValue("latitude", latitude);
      setValue("longitude", longitude);
    } catch (error) {
      console.log("Error getting current location: ", error);
      Alert.alert('Erreur', 'Impossible d\'obtenir la position actuelle.');
    } finally {
      setLoadingLocation(false);
    }
  };

  if (!object || !objectsUser) {
    return (
      <View className="flex-1 items-center justify-center">
        <Text className="text-lg">Aucune donnée disponible.</Text>
      </View>
    );
  }

  return (
    <View className="flex-1 p-5 bg-gray-100">
      <ScrollView>
        <View className="bg-white rounded-lg shadow-lg overflow-hidden">
          <Image
            source={object.photo ? { uri: object.photo } : require("../../../../../assets/images/book.jpg")}
            className="w-full h-60"
            resizeMode="cover"
          />
          <View className="p-4">
            <View className='flex flex-row items-center'>
              <Text className="text-2xl font-bold pr-3">{object.nom ?? ""}</Text>
              <View className="bg-blue-100 text-blue-800 px-2 py-1 rounded-full">
                <Text className="text-xs font-semibold">
                  {object.categorie ?? ""}
                </Text>
              </View>
            </View>
            <Text className="text-gray-700 mt-2">{object.description ?? ""}</Text>
            <View className="flex flex-row items-center mt-1">
              <Ionicons size={15} name="information-circle-outline" color="gray" />
              <Text className="text-gray-500 ml-2">{object.etat}</Text>
            </View>
            <View className="flex flex-row items-center mt-1">
              <Ionicons size={15} name="calendar-outline" color="gray" />
              <Text className="text-gray-500 ml-2">
                {object.annonce?.datePublication ? object.annonce.datePublication.split('T')[0] : ""}
              </Text>
            </View>
          </View>
        </View>

        <View className="mt-5">
          <Text className="text-lg font-semibold">Échanger contre :</Text>
          <FormDropdown<FormSchemaType>
            control={control}
            name="objet"
            hint="Sélectionner un objet"
            options={objectsUser.map((a) => a.id ?? 0)}
            optionToDisplay={objectsUser.map((a) => a.nom ?? "")}
          />
          {errors.objet && <Text className="text-red-500 mt-1">{errors.objet.message}</Text>}
        </View>

        <View className="mt-5">
          <Text className="text-lg font-semibold">Ajouter un point de récupération :</Text>
          <MapView
            style={{ width: '100%', height: 300, marginTop: 10 }}
            region={mapRegion}
            onPress={handleMapPress}
            showsUserLocation={true}
            loadingEnabled={true}
          >
            {markerCoords && (
              <Marker
                coordinate={{ latitude: markerCoords.lat, longitude: markerCoords.long }}
                title="Point de récupération"
              />
            )}
          </MapView>
          {errors.latitude && <Text className="text-red-500 mt-1">{errors.latitude.message}</Text>}
          {errors.longitude && <Text className="text-red-500 mt-1">{errors.longitude.message}</Text>}

          <CustomButton
            onPress={getCurrentLocation}
            title={loadingLocation ? "Recherche en cours..." : "Utiliser ma position actuelle"}
            buttonClassName="mt-5"
            titleClassName="font-bold"
            disabled={loadingLocation}
          />
        </View>

        <CustomButton
          onPress={handleSubmit(onSubmit)}
          title="Échanger"
          buttonClassName="bg-black mt-3 mb-10"
          titleClassName="font-bold"
        />
      </ScrollView>
    </View>
  );
};

export default ArticleDetailsScreen;
