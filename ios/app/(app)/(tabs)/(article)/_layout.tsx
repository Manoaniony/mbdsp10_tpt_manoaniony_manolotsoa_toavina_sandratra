import { useSession } from '@/services/context/authContext';
import { router, Stack } from 'expo-router';
import { TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default function ArticleLayout() {
  const { signOut } = useSession();

  return (
    <Stack
      screenOptions={{
        headerShown: true,
        headerRight: () => (
          <TouchableOpacity onPress={()=> {
            router.push("/scan")
          }}>
            <Ionicons name="scan" size={30} color="black" style={{ marginRight: 10 }} />
          </TouchableOpacity>
        ),
      }}
    >
      <Stack.Screen name="index" options={{ title: "TPT" }}/>
      <Stack.Screen name="details/[id]" options={{ title: "Details" }}/>
      <Stack.Screen name="map/index" options={{ title: "Point de récupération" }}/>
    </Stack>
  );
}
