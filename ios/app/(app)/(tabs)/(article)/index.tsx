import { mockArticles } from "@/mocks/article.mocks";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { View, Text, ScrollView, TextInput, RefreshControl } from "react-native";
import { z } from "zod";
import { router } from 'expo-router';
import { useEffect, useState } from "react";
import { ArticleType } from "@/types/article.type";
import { AnnouncementService } from "@/services/api/announcement/announcement.service";
import { useGlobal } from "@/services/context/globalContext";
import { annoncementType } from "@/types/annonce/annonce.type";
import AnnoucementCard from "@/components/card/announcement.card";
import { objectType } from "@/types/object/object.type";
import { ObjectService } from "@/services/api/object/object.service";




const ArticleScreen = () => {

    const { setLoading, setShowSnackbar } = useGlobal();

    const [Allannoucements, setAllAnnoucements] = useState<objectType[]>([])
    const [annoucements, setAnnoucements] = useState<objectType[]>([])
    const [refreshing, setRefreshing] = useState(false);

    const { getAllObject } = ObjectService()

    const getAnnoucement = async () => {
        setLoading(true);
        try {
            const res = await getAllObject();
            if (res.ok && res.objects) {
                setAnnoucements(res.objects.content);
                setAllAnnoucements(res.objects.content);
            } else {
                setShowSnackbar({ text: res.message ?? "Une erreur est survenue", visible: true });
            }
        } catch (error) {
            console.log("error occurred: ", error);
        } finally {
            setLoading(false);
        }
    };

    const refesh = async () => {
        setRefreshing(true);
        try {
            const res = await getAllObject();
            if (res.ok && res.objects) {
                setAnnoucements(res.objects.content);
                setAllAnnoucements(res.objects.content);
            } else {
                setShowSnackbar({ text: res.message ?? "Une erreur est survenue", visible: true });
            }
        } catch (error) {
            console.log("error occurred: ", error);
        } finally {
            setRefreshing(false);
        }
    };

    useEffect(() => {
        getAnnoucement()
    }, [])

    const searchByName = (name: string) => {
        if (name != "") {
            const filteredArticles = []


            // for(let i = 0 ; i < Allannoucements.length ; i++) {
            //     if(Allannoucements[i].objets) {
            //         for(let p = 0 ; p < Allannoucements[i].objets!.length; i++) {
            //             if(Allannoucements[i].objets![p].nom.toLowerCase().includes(name.toLowerCase())){
            //                 filteredArticles.push(Allannoucements[i])
            //             }
            //         }
            //     }
            // }

            // setAnnoucements(filteredArticles)
        } else {
            setAnnoucements(Allannoucements)
        }
    }
    return (
        <View className="flex-1">
            <View className="mx-5 my-10 mb-50">
                <Text className="font-bold text-[30px] pb-3">Article</Text>
                <TextInput
                    onChangeText={searchByName}
                    placeholder={"Recherche"}
                    className="border rounded-lg p-2 mb-2 w-full"
                />
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={refesh}
                        />
                    }
                >
                    <View className="mb-20">
                        {annoucements?.map((ac) =>
                            <AnnoucementCard
                                key={ac.id}
                                object={ac}
                                onPress={() => {
                                    router.push(`/details/${ac.id}`);
                                }}
                            />
                        )}
                    </View>
                </ScrollView>

            </View>
        </View>
    )
}

export default ArticleScreen