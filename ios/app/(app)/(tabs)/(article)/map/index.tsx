import { Text, View } from "react-native";
import MapView, { Marker } from 'react-native-maps';

const MapScreen = () => {
    return (
        <View className="flex-1">
            <MapView
                className="w-full h-full"
                initialRegion={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
            >
                <Marker
                    coordinate={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                    }}
                    title="My Location"
                    description="This is a description of the marker"
                />
            </MapView>
        </View>
    );
}

export default MapScreen;
