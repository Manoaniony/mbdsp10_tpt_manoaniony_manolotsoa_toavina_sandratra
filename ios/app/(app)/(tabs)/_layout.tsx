import { useSession } from '@/services/context/authContext';
import { Redirect, Stack } from 'expo-router';
import { Text } from 'react-native';
import { Tabs } from 'expo-router';
import { TabBarIcon } from '@/components/navigation/TabBarIcon';
import { Colors } from '@/constants/Colors';
import { useColorScheme } from '@/hooks/useColorScheme';
import BaseScreen from '@/components/base/base.screen';

export default function TabsLayout() {

  const colorScheme = useColorScheme();

  // This layout can be deferred because it's not the root layout.
  return (
      <Tabs
        screenOptions={{
          tabBarActiveTintColor: "black",
          headerShown: false,
        }}>
        <Tabs.Screen
          name="(article)"
          options={{
            title: 'article',
            tabBarIcon: ({ color, focused }) => (
              <TabBarIcon name={focused ? "basket" : "basket-outline"} color={color} />
            ),
          }}
        />
        <Tabs.Screen
          name="(pub)"
          options={{
            title: 'Publier',
            tabBarIcon: ({ color, focused }) => (
              <TabBarIcon name={focused ? "add-circle" : "add-circle-outline"} color={color} />
            ),
          }}
        />
        <Tabs.Screen
          name="(match)"
          options={{
            title: 'Match',
            tabBarIcon: ({ color, focused }) => (
              <TabBarIcon name={focused ? "star" : "star-outline"} color={color} />
            ),
          }}
        />
        <Tabs.Screen
          name="(profil)"
          options={{
            title: 'Profile',
            tabBarIcon: ({ color, focused }) => (
              <TabBarIcon name={focused ? "person-circle" : "person-circle-outline"} color={color} />
            ),
          }}
        />
      </Tabs>
  );
}
