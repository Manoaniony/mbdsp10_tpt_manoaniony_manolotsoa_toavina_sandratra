import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as ImagePicker from "expo-image-picker";
import { z } from "zod";
import FormInput from "@/components/form/FormInput";
import CustomButton from "@/components/CustomButton";
import { annoncementType } from "@/types/annonce/annonce.type";
import { saveAnnonceType } from "@/types/annonce/save_annonce.type";
import { getUser } from "@/services/storage/storage";
import { useGlobal } from "@/services/context/globalContext";
import { AnnouncementService } from "@/services/api/announcement/announcement.service";
import { saveObjectType } from "@/types/object/save_object.type";
import { announcementStatus } from "@/constants/enum/annoucement/status.enum";
import FormDropdown from "@/components/form/FormDropdown";
import { announcementCategories } from "@/constants/enum/annoucement/category.enum";
import BaseScreen from "@/components/base/base.screen";
import { createTable, deleteBrouillon, getBrouillon, insertBrouillon, updateBrouillon } from "@/services/local/db";
import { UploadService } from "@/services/api/upload/upload.service";
import { imageType } from "@/types/image/image.type";

const formSchema = z.object({
    image: z.string(),
    name: z.string().min(1, "Ce champ est obligatoire"),
    description: z.string(),
    category: z.string(),
});

export type FormSchemaType = z.infer<typeof formSchema>;

const PubScreen = () => {
    const [imageUri, setImageUri] = useState<string | null>(null);
    const { setLoading, setShowSnackbar } = useGlobal();

    const { control, handleSubmit, setValue , watch} = useForm<FormSchemaType>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            image: "",
            name: "",
            description: "",
            category: ""
        },
    });

    const fields = watch();

    const { saveAnnouncement, saveObject } = AnnouncementService()
    const {uploadImg} = UploadService()

    const loadBrouillon = async () => {
        const brouillon: FormSchemaType = await getBrouillon() as FormSchemaType;
        if (brouillon) {
            setValue("image", brouillon.image);
            setValue("name", brouillon.name);
            setValue("description", brouillon.description);
            setValue("category", brouillon.category);
            setImageUri(brouillon.image);
        }
    };

    
    useEffect(() => {
        createTable();
        loadBrouillon();
    }, []);

    const insertToDb = async () => {
        const { image, name, description, category } = fields;
        if(image != null && name != "" && description != "" && category != ""){
            const brouillon  = await getBrouillon() as any;
            if(brouillon) {
                await updateBrouillon(brouillon?.id, fields).then(() => {
                    setShowSnackbar({ text: "Brouillon enregistrer", visible: true })
                });
            }else{
                await insertBrouillon(fields).then(() => {
                    setShowSnackbar({ text: "Brouillon enregistrer", visible: true })
                })
            }  
        }
    }

    // useEffect(() => {
    //     insertToDb()
    // }, [fields]);

    const onSubmit = async (values: FormSchemaType) => {
        // Handle form submission
        console.log("start submit pub")
        const user = await getUser()
        
        

        setLoading(true)

        try {
            const formData = new FormData();
            
            const uriParts = values.image.split('.');
            const fileType = uriParts[uriParts.length - 1];
            
            formData.append('files', {
                uri: values.image,
                name: `photo.${fileType}`,
                type: `image/${fileType}`,
            } as any);

        
            const imgUpload = await uploadImg(formData)
            console.log("imgupload === ", imgUpload)
            if(imgUpload.ok && imgUpload.objects) {
                const req: saveAnnonceType = {
                    libelle: values.name,
                    descs: values.description,
                    photo: imgUpload.objects[0].url,
                    etat: announcementStatus.WAITING,
                    datePublication: new Date().toISOString().split('T')[0], 
                    user: {
                        id: user?.id!,
                    },
                }
                const res = await saveAnnouncement(req)
                if (res.ok && res.objects) {
                    const obj: saveObjectType = {
                        nom: values.name,
                        description: values.description,
                        categorie: values.category,
                        etat: announcementStatus.WAITING,
                        photo: imgUpload.objects[0].url,
                        annonce: {
                            id: res.objects.id!
                        },
                        user: {
                            id: user?.id!,
                        },
                    }

                    const resObject = await saveObject(obj)
                    if (resObject.ok) {
                        setLoading(false)
                        await deleteBrouillon();
                        setShowSnackbar({ text: "Votre annonce a ete publier avec succces ", visible: true })
                    } else {
                        setShowSnackbar({ text: resObject.message ?? "Une erreur est suvenue", visible: true })
                    }
                } else {
                    setShowSnackbar({ text: res.message ?? "Une erreur est suvenue", visible: true })
                }
            }else{
                setShowSnackbar({ text: imgUpload.message ?? "Une erreur est suvenue upload", visible: true })
            }
            setLoading(false)

        } catch (error) {
            setLoading(false)
            console.log("Error occured : ", error)
        }
        setLoading(false)
    };

    const pickImage = async () => {
        const permissionResult =
            await ImagePicker.requestMediaLibraryPermissionsAsync();

        if (permissionResult.granted === false) {
            alert("Permission to access camera is required!");
            return;
        }

        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            quality: 1,
        });

        if (!result.canceled) {
            const selectedImage = result.assets[0].uri;
            setImageUri(selectedImage);
            setValue("image", selectedImage);
        }
    };

    return (
            <View className="flex-1">
                <View className="mx-5 my-10">
                    <Text className="font-bold text-[30px] pb-3">Publier une annonce</Text>
                    <ScrollView>
                        {/* Image Picker */}
                        <TouchableOpacity
                            onPress={pickImage}
                            className="bg-gray-200 p-4 mb-4 rounded-lg items-center"
                        >
                            <Text className="text-black font-semibold">Selectioner une image</Text>
                        </TouchableOpacity>

                        {/* Display Image Preview */}
                        {imageUri && (
                            <Image
                                source={{ uri: imageUri }}
                                className="w-full h-40 mb-4 rounded-lg"
                            />
                        )}

                        <View className="p-3 mb-5">
                            <FormInput<FormSchemaType>
                                control={control}
                                name="name"
                                placeholder="Ecrivez le nom de l'article"
                            />
                            <FormInput<FormSchemaType>
                                control={control}
                                name="description"
                                placeholder="Ecrivez une description"
                                multiline={true}
                                numberOfLines={4}
                            />
                            <FormDropdown<FormSchemaType>
                                control={control}
                                name="category"
                                hint="Sélectionner une categorie"
                                options={Object.values(announcementCategories)}
                            />

                            
                        </View>
                        <CustomButton
                            onPress={insertToDb}
                            title="Brouillon"
                            outline = {true}
                        />
                        <CustomButton
                            onPress={handleSubmit(onSubmit)}
                            title="Publier"
                            buttonClassName="bg-black"
                        />
                    </ScrollView>
                </View>
            </View>
    );
};

export default PubScreen;
