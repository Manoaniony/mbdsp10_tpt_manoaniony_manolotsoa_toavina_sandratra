import { useSession } from '@/services/context/authContext';
import { Redirect, Stack } from 'expo-router';
import { Text } from 'react-native';
import { Tabs } from 'expo-router';
import { TabBarIcon } from '@/components/navigation/TabBarIcon';
import { Colors } from '@/constants/Colors';
import { useColorScheme } from '@/hooks/useColorScheme';
import BaseScreen from '@/components/base/base.screen';

export default function AppLayout() {
  const { session, isLoading } = useSession();

  const colorScheme = useColorScheme();

  // You can keep the splash screen open, or render a loading screen like we do here.
  if (isLoading) {
    return <Text className='text-center font-bold'>...Loading</Text>
  }

  // Only require authentication within the (app) group's layout as users
  // need to be able to access the (auth) group and sign in again.
  if (!session) {
    // On web, static rendering will stop here as the user is not authenticated
    // in the headless Node process that the pages are rendered in.
    return <Redirect href="/signIn" />;
  }

  // This layout can be deferred because it's not the root layout.
  return (
    <BaseScreen>
          <Stack 
          initialRouteName="(tabs)">
            <Stack.Screen name="(tabs)" options={{ headerShown: false }}/>
            <Stack.Screen name="scan" options={{ headerShown: true, title: "Scan" }} />
          </Stack>
    </BaseScreen>
  );
}
