import React, { useState } from 'react';
import { Text, TextInput, View, TouchableOpacity } from 'react-native';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';
import { Ionicons } from '@expo/vector-icons'; 
import { styled } from 'nativewind';

type Props<T extends FieldValues> = {
    control: Control<T>,
    name: Path<T>,
    placeholder?: string,
    label?: string,
    required? : boolean
} & React.ComponentProps<typeof TextInput>;

const StyledTextInput = styled(TextInput);
const StyledText = styled(Text);
const StyledView = styled(View);

const FormInput = <T extends FieldValues>({ control, name, placeholder, label, secureTextEntry, required = false, ...textInputProps }: Props<T>) => {
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);

    return (
        <Controller
            control={control}
            name={name}
            render={({ field: { value, onChange, onBlur }, fieldState: { error } }) => (
                <StyledView className="w-full">
                    {label && <StyledText className="mb-2 text-lg">{label} :</StyledText>}
                    <View className="relative w-full">
                        <StyledTextInput
                            value={value}
                            onChangeText={onChange}
                            onBlur={onBlur}
                            placeholder={placeholder}
                            secureTextEntry={secureTextEntry && !isPasswordVisible}
                            className="bg-gray-200 rounded-lg p-2 mb-2 w-full"
                            {...textInputProps}
                        />
                        {secureTextEntry && (
                            <TouchableOpacity
                                onPress={() => setIsPasswordVisible(!isPasswordVisible)}
                                className="absolute right-3 top-3"
                            >
                                <Ionicons
                                    name={isPasswordVisible ? "eye-off" : "eye"}
                                    size={20}
                                    color="gray"
                                />
                            </TouchableOpacity>
                        )}
                    </View>
                    {error && <StyledText className="text-red-500">{error.message}</StyledText>}
                </StyledView>
            )}
        />
    );
};
export default FormInput;
