import React from 'react';
import { View, Text } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { Control, Controller, FieldValues, Path } from 'react-hook-form';

type Props<T extends FieldValues> = {
    control: Control<T>,
    name: Path<T>,
    options: any[],
    label?: string,
    hint?: string,
    isRequired?: boolean,
    optionToDisplay?: string[],
    editable?: boolean
} & React.ComponentProps<typeof Picker>;


const FormDropdown = <T extends FieldValues>({
  control,
  name,
  options,
  label,
  hint,
  isRequired,
  optionToDisplay,
  editable = true,
  ...pickerProps
}: Props<T>) => {
  return (
      <Controller
          control={control}
          name={name}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
              <View className="w-full">
                  {label && (
                      <Text className="text-base font-semibold mb-1">
                          {label}
                          {isRequired && (
                              <Text className="text-red-600 font-bold text-lg">
                                  {" (*)"}
                              </Text>
                          )}
                      </Text>
                  )}
                  <View className="border border-gray-300 rounded-lg overflow-hidden w-full">
                      <Picker
                          selectedValue={value}
                          onValueChange={editable ? onChange : undefined}
                          className="bg-white w-full"  // Ajout de w-full ici aussi
                          {...pickerProps}
                      >
                          {/* Option par défaut */}
                          <Picker.Item
                              label={hint ?? "Sélectionner une option"}
                              value=""
                              color="gray"
                          />
                          {options?.map((option, index) => (
                              <Picker.Item
                                  key={index}
                                  label={optionToDisplay && optionToDisplay[index] ? optionToDisplay[index] : option}
                                  value={option}
                              />
                          ))}
                      </Picker>
                  </View>
                  {error && (
                      <Text className="text-red-600 mt-2">
                          {error.message ?? "Ce champ est obligatoire"}
                      </Text>
                  )}
              </View>
          )}
      />
  );
};

export default FormDropdown;
