import React from "react";
import { View, Text } from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import { matchType } from "@/types/match/match.type";

type Props = {
  match : matchType
};

const MatchCard = ({ match }: Props) => {
  return (
    <View className="p-5 bg-white rounded-lg my-3 shadow-md">
      <View className="flex flex-row justify-between items-center mb-4">
        <Text className="text-lg font-bold text-gray-800">{match.objet1.nom}</Text>
        <Ionicons size={28} name="swap-horizontal-outline" color="#4F46E5" />
        <Text className="text-lg font-bold text-gray-800">
          {match.objet2.nom}
        </Text>
      </View>
      <View className="flex flex-row justify-between items-center">
        <View className="flex-1">
          <Text className="text-gray-600 text-sm">Date: {match.dateMatch}</Text>
          {/* <Text className="text-gray-600 text-sm">Heure: {time}</Text> */}
        </View>
        <Ionicons size={20} name="calendar-outline" color="#6B7280" />
      </View>
    </View>
  );
};

export default MatchCard;
