import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { ArticleType } from "@/types/article.type";
import Ionicons from '@expo/vector-icons/Ionicons';
import { annoncementType } from '@/types/annonce/annonce.type';
import { objectType } from '@/types/object/object.type';

type props = {
  object: objectType,
  onPress?: () => void
}

const AnnoucementCard = ({ object, onPress }: props) => {
  return (
    <View className="p-4 bg-white rounded-lg my-3 shadow-md ">
      <Image
        source={object.photo != null ? { uri: object.photo } : require("../../assets/images/book.jpg")}
        className="w-full h-60 rounded-t-lg"
        resizeMode="cover"
      />
      <View className="pt-4">
        <View className='flex flex-row justify-between items-center'>
          <View className='flex flex-row items-center'>
            <Text className="text-xl font-semibold pr-3">{object?.nom ? object?.nom : ""}</Text>
            
            {object.categorie !== null && (
                <View className="bg-blue-100 text-blue-800 px-2 py-1 rounded-full inline-flex items-center">
                  <Text className="text-xs font-semibold">
                    {object?.categorie ? object.categorie : ""}
                  </Text>
                </View>
            )}
          </View>

          {
            onPress && 
            <TouchableOpacity
              onPress={onPress}
            >
              <Ionicons size={40} name="arrow-forward-circle-outline" />
            </TouchableOpacity>
          }
        </View>
        {object.description && (
          <Text className="text-gray-600 mt-2">{object.description}</Text>
        )}
        {object.etat && (
          <Text className="text-gray-500 mt-1 ">
            <Ionicons size={18} name="information-circle-outline"/>
            {" "}{object.etat}
          </Text>
        )}
        {object.annonce && (
          <Text className="text-gray-500 mt-1">
            <Ionicons size={15} name="calendar-outline" />
            {" "}
            {object.annonce.datePublication?.split('T')[0]}
          </Text>
        )}

      </View>
    </View>
  );
};

export default AnnoucementCard;
