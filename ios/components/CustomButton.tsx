import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { styled } from 'nativewind';

type CustomButtonProps = {
    onPress: () => void;
    title: string;
    buttonClassName?: string;
    titleClassName?: string;
    outline?: boolean;
    disabled?: boolean; // Changed `disable` to `disabled`
};

const StyledButton = styled(TouchableOpacity);
const StyledText = styled(Text);

const CustomButton = ({ onPress, title, buttonClassName, titleClassName, outline, disabled }: CustomButtonProps) => {
    const buttonStyle = outline 
        ? `border ${disabled ? 'border-gray-400 bg-gray-200' : 'border-blue-500 bg-transparent'} ${buttonClassName}`
        : `${disabled ? 'bg-gray-400' : 'bg-blue-500'} ${buttonClassName}`;
    const textStyle = outline 
        ? `${disabled ? 'text-gray-400' : 'text-blue-500'} ${titleClassName}`
        : `text-white ${titleClassName}`;

    return (
        <StyledButton 
            onPress={!disabled ? onPress : undefined} 
            className={`${buttonStyle} rounded-lg p-4 mb-2 w-full`}
            disabled={disabled} // Set the disabled prop here
        >
            <StyledText className={`text-center text-lg ${textStyle}`}>
                {title}
            </StyledText>
        </StyledButton>
    );
};

export default CustomButton;
