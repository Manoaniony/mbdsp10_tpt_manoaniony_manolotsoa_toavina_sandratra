import { GlobalContext } from "@/services/context/globalContext";
import { ReactNode, useContext } from "react";
import { ActivityIndicator, Modal, View, Text } from "react-native";
import { Snackbar } from "react-native-paper";


type props = {
    children: ReactNode;
};


const BaseScreen = (props: props) => {

    const { showSnackbar, setShowSnackbar, loading } = useContext(GlobalContext);

    return (
        <>
            {props.children}
            <Modal
                transparent={true}
                animationType={"none"}
                visible={loading}
                onRequestClose={() => {
                    console.log("Modal has been closed.");
                }}>
                <View className="flex-1 items-center flex-col justify-around bg-black/40">
                    <View className="bg-white/70 h-25 w-25 p-2 rounded-lg flex items-center justify-center">
                        <ActivityIndicator animating={loading} size="large" color="#0000ff" />
                        <Text className="font-bold pt-1.5">Chargement</Text>
                    </View>
                </View>
            </Modal>
            <Snackbar
                visible={showSnackbar.visible}
                onDismiss={() => setShowSnackbar({ ...showSnackbar, visible: false })}
                action={{
                    label: 'Fermer',
                    onPress: () => { },
                }}
            >
                {showSnackbar.text}
            </Snackbar>
        </>
    )
}

export default BaseScreen