import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabaseSync('brouillon.db');

type FormSchemaType = {
    image: string | null;
    name: string;
    description: string;
    category: string;
}

export const createTable = async () => {
        await db.execAsync(
            `CREATE TABLE IF NOT EXISTS brouillon (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                image TEXT,
                name TEXT,
                description TEXT,
                category TEXT
            );`
        );
    
};

export const insertBrouillon = async (data: FormSchemaType) => {
    await db.runAsync(
        'INSERT INTO brouillon (image, name, description, category) VALUES (?, ?, ?, ?)', 
        data.image, data.name, data.description, data.category
    );
};

export const updateBrouillon = async (id : number, data: FormSchemaType) => {
    await db.runAsync(
        'UPDATE brouillon SET image = ? , name = ? , description = ? , category = ? WHERE id = ?', 
         data.image, data.name, data.description, data.category, id
    );
};


export const getBrouillon = async () => {
    
    return await db.getFirstAsync('SELECT * FROM brouillon');
};

export const deleteBrouillon = async () => {
    await db.runAsync('DELETE FROM brouillon');
};
