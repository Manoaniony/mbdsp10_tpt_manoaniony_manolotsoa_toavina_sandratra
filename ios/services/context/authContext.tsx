import { useStorageState } from '@/hooks/useStorageState';
import React from 'react';
import { clearPref, getToken } from '../storage/storage';
import { AuthenticationService } from '../api/authentication/authentication.service';
import { LoginType } from '@/types/user/login.type';
import { BaseResponseType } from '@/types/base_response.type';

const AuthContext = React.createContext<{
  signIn: (req: LoginType) => Promise<BaseResponseType<null> | BaseResponseType<string>>;
  signOut: () => void;
  session?: string | null;
  setServerStatus: (status: string) => void;
  serverStatus?: string | null;
  isLoading: boolean;
}>({
  signIn: async () => {
    return {
      ok: false,
      status: 0,
      message: "",
      object: null,
    };
  },
  signOut: () => null,
  session: null,
  setServerStatus: () => null,
  serverStatus: '0',
  isLoading: false,
});


// This hook can be used to access the user info.
export function useSession() {
  const value = React.useContext(AuthContext);
  if (process.env.NODE_ENV !== 'production') {
    if (!value) {
      throw new Error('useSession must be wrapped in a <SessionProvider />');
    }
  }

  return value;
}

export function SessionProvider(props: React.PropsWithChildren) {
  const [[isLoading, session], setSession] = useStorageState('session');
  const [[serverLoading, serverStatus], setServerStatus] = useStorageState('server');

  const { login , getUserInfo} = AuthenticationService()
  return (
    <AuthContext.Provider
      value={{
        signIn: async (req: LoginType) => {
          
          const res = await login(req)
          if(res.ok && res.objects){
            setSession(res.objects)
            await getUserInfo()
          }
          return res; 
        },
        signOut: async () => {
          await clearPref().then(() => 
            setSession(null)
          )
        },
        setServerStatus: (status: string) => {
          setServerStatus(status);
        },
        serverStatus,
        session,
        isLoading,
      }}>
      {props.children}
    </AuthContext.Provider>
  );
}
