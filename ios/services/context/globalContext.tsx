import React, { ReactNode, createContext, useEffect, useState } from "react";
import { Snackbar } from "react-native-paper";

type GlobalContextState = {
  loading: boolean;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
  showSnackbar: { text: string; visible: boolean };
  setShowSnackbar: React.Dispatch<React.SetStateAction<{ text: string; visible: boolean }>>;
};

export const GlobalContext = createContext<GlobalContextState>({
  loading: false,
  setLoading: () => { },
  showSnackbar: { text: "", visible: false },
  setShowSnackbar: () => { }
});

export function useGlobal() {
  const value = React.useContext(GlobalContext);
  if (process.env.NODE_ENV !== 'production') {
    if (!value) {
      throw new Error('useGlobal must be wrapped in a <GlobalProvider />');
    }
  }

  return value;
}

export const GlobalProvider = ({ children }: { children: ReactNode }) => {
  const [showSnackbar, setShowSnackbar] = useState({ text: "", visible: false });
  const [loading, setLoading] = useState(false);


  return (
    <GlobalContext.Provider
      value={{
        loading,
        setLoading,
        showSnackbar,
        setShowSnackbar,
      }}>
      {children}
    </GlobalContext.Provider>
  );
};
