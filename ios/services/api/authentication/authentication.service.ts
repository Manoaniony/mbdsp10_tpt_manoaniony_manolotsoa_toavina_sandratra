import { urls } from "@/constants/urls";
import { get, post } from "../api";
import { BaseResponseType } from "@/types/base_response.type";
import { getToken, setToken, setUser } from "@/services/storage/storage";
import { LoginType } from "@/types/user/login.type";
import { signUpType } from "@/types/user/signup.type";
import { userType } from "@/types/user/user.types";


export const AuthenticationService = () => {
    return {
        login: async (login: LoginType) => {
            const response = await post<string>(urls.login, login, false, false);
            if (response.ok && response.objects) {
                setToken(response.objects)
            }
            return response
        },

        signup: async (req: signUpType) => {
            const response = await post<userType>(urls.signup, req, false, false);

            return response
        },

        getUserInfo: async () => {
            const token = await getToken()
            const response = await get<userType>(urls.getInfo + token, {}, true);
            if (response.ok && response.objects) {
                setUser(response.objects)
            }


            return response
        }

    }
}