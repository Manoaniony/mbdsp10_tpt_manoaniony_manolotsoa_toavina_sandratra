import { urls } from "@/constants/urls";
import { get, post } from "../api";
import { matchType } from "@/types/match/match.type";
import { matchsType } from "@/types/match/matchs.type";


export const MatchService = () => {
    return {
        saveMatch: async (req: matchType) => {
            const response = await post<matchType>(urls.saveMatch, req, true, false);
            return response
        },

        getAllMatch: async () => {
            const response = await get<matchsType>(`${urls.getAllMatch}`, {}, true);

            return response
        },

        getObjectById: async (id: number) => {
            const response = await get<matchType>(`${urls.getMatchById}/${id}`, {}, true);

            return response
        },
    }
}