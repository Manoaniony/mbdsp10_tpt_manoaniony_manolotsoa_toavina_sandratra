import { BaseResponse, BaseResponseType } from "@/types/base_response.type";
import axios, { AxiosError } from "axios";
import { useSession } from "../context/authContext";
import { getToken, setServerStatus } from "../storage/storage";
import { urls } from "@/constants/urls";


enum serveError {
  INTERNAL_SERVER = 500,
  BAD_GATEWAY = 502,
  SERVICE_UNVALAIBLE = 503,
}

export const HttpStatus = {
  OK: 200,
  ErrorClient: 400,
  Unauthorized: 401,
  BadRequest: 402,
  Notfound: 404,
  ServerError: 500,
};

const client = axios.create()

export const headers = async (tokenRequired: boolean = false, isFile: boolean = false) => {
  let token = await getToken();
  let headerConfig: Record<string, string> = {
      "Content-Type": isFile ? "multipart/form-data" : "application/json",
      "Access-Control-Allow-Origin": "*",
  };

  if (token !== null && tokenRequired) {
      headerConfig["Authorization"] = `Bearer ${token}`;
  }

  return headerConfig;
};


export const catchError = (error: AxiosError) => {
  let data
  let status

  if (error?.status) {
    status = error.status;
  } else {
    data = error?.response?.data as any;
    status = error?.response?.status;
  }
  serveurStatus(status);

  console.log(data)
  const response = BaseResponse<null>(false, status, null, data?.message);

  return response;
};

const serveurStatus = (status?: number) => {
  if (status === serveError.BAD_GATEWAY || status === serveError.SERVICE_UNVALAIBLE) {
    setServerStatus("0");
  } else {
    setServerStatus("1");
  }
};


export const get = async <T>(url: string, data?: any, tokenRequired: boolean = false) => {
  const headerConfig = await headers(tokenRequired);
  try {
      let res;
      if (!data) {
          res = await client.get(url, { headers: headerConfig });
      } else {
          res = await client.get(url, {
              params: data,
              headers: headerConfig,
          });
      }
      serveurStatus(res.status);
      return res.data as BaseResponseType<T>;
  } catch (error) {
      const err = error as AxiosError;
      return catchError(err);
  }
};

export const post = async <T>(
  url: string,
  data: any,
  tokenRequired: boolean = false,
) => {
  const headerConfig = await headers(tokenRequired);
  try {
      const res = await client.post(url, data, { headers: headerConfig });
      serveurStatus(res.status);
      return res.data as BaseResponseType<T>;
  } catch (error) {
      const err = error as AxiosError;
      return catchError(err);
  }
};

// La même chose pour `put`, `remove` et `upload`.


export const put = async <T>(url: string, data = {}, params = {}, tokenRequired: boolean = false) => {
  const headerConfig = await headers(tokenRequired);
  try {
    let res;

    if (!Object.keys(params || {}).length) {
      res = await client.put(url, data,);
    } else {
      res = await client.put(`${url}/${params}`, data, { headers: headerConfig });
    }
    serveurStatus(res.status);
    return res.data as BaseResponseType<T>;;
  } catch (error) {
    const err = error as AxiosError;
    return catchError(err);
  }
};

export const remove = async <T>(url: string, tokenRequired: boolean = false) => {
  const headerConfig = await headers(tokenRequired);
  try {
    const res = await client.delete(url, { headers: headerConfig });
    serveurStatus(res.status);
    return res.data as BaseResponseType<T>;;
  } catch (error) {
    const err = error as AxiosError;
    return catchError(err);
  }
};

export const upload = async <T>(
  url: string,
  data: any,
  tokenRequired: boolean = false,
) => {
  const headerConfig = await headers(tokenRequired, true);
  try {
    const res = await client.post(url, data, { headers: headerConfig });
    serveurStatus(res.status);
    let result: BaseResponseType<T> = {
      ok: true,
      objects: res.data.data,
      message: res.data.message,
      status: res.status
    }
    return result
  } catch (error) {
    console.log("error on api : ", error)
    const err = error as AxiosError;
    return catchError(err);
  }
};
