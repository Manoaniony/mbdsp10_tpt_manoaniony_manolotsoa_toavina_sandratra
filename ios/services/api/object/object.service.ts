import { urls } from "@/constants/urls";
import { get, post } from "../api";
import { userType } from "@/types/user/user.types";
import { saveAnnonceType } from "@/types/annonce/save_annonce.type";
import { saveObjectType } from "@/types/object/save_object.type";
import { annoncementType } from "@/types/annonce/annonce.type";
import { objectType } from "@/types/object/object.type";
import { annoncementsType } from "@/types/annonce/announcements.type";
import { objectsType } from "@/types/object/objects.type";


export const ObjectService = () => {
    return {
        saveObject: async (req: saveObjectType) => {
            const response = await post<objectType>(urls.saveObject, req, true, false);

            return response
        },

        getObjectByUser: async (id: number) => {
            const response = await get<objectType[]>(`${urls.getObjectByUser}/${id}`, {}, true);

            return response
        },

        getAllObject: async () => {
            const response = await get<objectsType>(`${urls.getAllObjects}`, {}, true);

            return response
        },

        getObjectById: async (id: number) => {
            const response = await get<objectType>(`${urls.getObjectById}/${id}`, {}, true);

            return response
        },
    }
}