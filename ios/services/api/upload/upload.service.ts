import { urls } from "@/constants/urls";
import { get, post, upload } from "../api";
import { userType } from "@/types/user/user.types";
import { saveAnnonceType } from "@/types/annonce/save_annonce.type";
import { saveObjectType } from "@/types/object/save_object.type";
import { annoncementType } from "@/types/annonce/annonce.type";
import { objectType } from "@/types/object/object.type";
import { annoncementsType } from "@/types/annonce/announcements.type";
import { objectsType } from "@/types/object/objects.type";
import { imageResponse, imageType } from "@/types/image/image.type";


export const UploadService = () => {
    return {
        uploadImg: async (req: FormData) => {

            console.log("req ======== ", req)
            const response = await upload<imageResponse[]>(urls.uploadImg, req, false);

            return response
        },

    }
}