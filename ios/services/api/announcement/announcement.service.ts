import { urls } from "@/constants/urls";
import { get, post } from "../api";
import { userType } from "@/types/user/user.types";
import { saveAnnonceType } from "@/types/annonce/save_annonce.type";
import { saveObjectType } from "@/types/object/save_object.type";
import { annoncementType } from "@/types/annonce/annonce.type";
import { objectType } from "@/types/object/object.type";
import { annoncementsType } from "@/types/annonce/announcements.type";


export const AnnouncementService = () => {
    return {

        getAllAnnouncement: async () => {
            const response = await get<annoncementsType>(urls.getAllAnnoncement, null, true);

            return response
        },

        getAnnouncementById: async (id: number) => {
            const response = await get<annoncementType>(`${urls.getAnnoncementById}/${id}`, null, true);

            return response
        },

        saveAnnouncement: async (req: saveAnnonceType) => {
            const response = await post<annoncementType>(urls.saveAnnoncement, req, true, false);
            
            return response
        },


        saveObject: async (req: saveObjectType) => {
            const response = await post<objectType>(urls.saveObject, req, true, false);

            return response
        },

    }
}