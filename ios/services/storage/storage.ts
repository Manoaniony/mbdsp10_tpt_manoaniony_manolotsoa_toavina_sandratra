
import { userType } from '@/types/user/user.types';
import AsyncStorage from '@react-native-async-storage/async-storage';

const key_token = "token"
const key_server = "server"
const key_user = "user"

export const clearPref = async () => {
    try {
        await AsyncStorage.clear();
        return true
    } catch (e) {
        console.log(e)
        return false
    }
}


export const setToken = async (token: string) => {
    try {
        await AsyncStorage.setItem(key_token, token);
    } catch (e) {
        console.log(e)
    }
};

export const getToken = async () => {
    try {
        const value = await AsyncStorage.getItem(key_token);
        return value
    } catch (e) {
        console.log(e)
        return null
    }
};

export const setServerStatus = async (status: string) => {
    try {
        await AsyncStorage.setItem(key_server, status);
    } catch (e) {
        console.log(e)
    }
};

export const getServerStatus = async () => {
    try {
        const value = await AsyncStorage.getItem(key_server);
        return value == '1'
    } catch (e) {
        console.log(e)
        return null
    }
};

export const setUser = async (userObj: userType) => {
    try {
        const userStr = JSON.stringify(userObj); 
        await AsyncStorage.setItem(key_user, userStr);
    } catch (e) {
        console.log(e);
    }
};


export const getUser = async (): Promise<userType | null> => {
    try {
        const userStr = await AsyncStorage.getItem(key_user);
        if (userStr) {
            return JSON.parse(userStr);
        }
        return null;
    } catch (e) {
        console.log(e);
        return null;
    }
};
